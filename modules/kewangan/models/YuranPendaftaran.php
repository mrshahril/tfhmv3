<?php

namespace app\modules\kewangan\models;

use Yii;

use app\models\LookupPusatPengajian;
use app\models\LookupYurandaftar;

use app\modules\hr\pelajar\models\MaklumatPelajarPenjaga;
use app\modules\kewangan\models\YuranPendaftaranDetail;
use app\modules\oldyuran\models\OldYuranDaftar;

/**
 * This is the model class for table "yuran_pendaftaran".
 *
 * @property int $id
 * @property int $idstd
 * @property string $invoice_no
 * @property string $tarikh_bayaran
 * @property int $pusat_pengajian
 * @property double $jumlah_yuran
 * @property double $jumlah_bayaran
 * @property double $yuran_tertunggak_daftar
 * @property int $tahun
 * @property int $tahap_pelajar
 * @property int $guru_kelas
 * @property int $guru_kanan
 * @property int $status_yuran
 * @property string $nota
 * @property int $jenis_pelajar
 * @property string $nama_penerima
 * @property string $jenis_bayaran
 * @property string $no_rujukan_pindahan_wang
 * @property double $pelarasan_tambahan
 * @property double $pelarasan_tolakan
 * @property string $created_at
 * @property string $updated_at
 * @property int $enter_by
 * @property int $update_by
 */
class YuranPendaftaran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yuran_pendaftaran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //validation based on scenario
            [['jenis_pelajar','tahap_pelajar'],'required','on'=>'addbill','message'=>'Ruangan {attribute} wajib diisi'],
            [['jenis_pelajar'],'required','on'=>'yuranregisterday','message'=>'Ruangan {attribute} wajib diisi'],
            [['jenis_pelajar','tahap_pelajar'],'required','on'=>'edityuran','message'=>'Ruangan {attribute} wajib diisi'],

            [['idstd', 'pusat_pengajian', 'tahun', 'tahap_pelajar', 'guru_kelas', 'guru_kanan', 'status_yuran', 'jenis_pelajar', 'enter_by', 'update_by'], 'integer'],
            [['jumlah_yuran', 'jumlah_bayaran', 'yuran_tertunggak_daftar', 'pelarasan'], 'number'],
            [['nota','ulasan_pelarasan'], 'string'],
            [['invoice_no', 'nama_penerima', 'jenis_bayaran'], 'string', 'max' => 255],
            [['tarikh_bayaran', 'created_at', 'updated_at'], 'string', 'max' => 50],
            [['no_rujukan_pindahan_wang'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idstd' => 'Idstd',
            'invoice_no' => 'No. Invois',
            'tarikh_bayaran' => 'Tarikh Bayaran',
            'pusat_pengajian' => 'Pusat Pengajian',
            'jumlah_yuran' => 'Jumlah Yuran (RM)',
            'jumlah_bayaran' => 'Jumlah Bayaran (RM)',
            'yuran_tertunggak_daftar' => 'Yuran Tertunggak Daftar',
            'tahun' => 'Tahun',
            'tahap_pelajar' => 'Tahap Pelajar',
            'guru_kelas' => 'Guru Kelas',
            'guru_kanan' => 'Guru Kanan',
            'status_yuran' => 'Status Yuran',
            'nota' => 'Nota',
            'jenis_pelajar' => 'Jenis Pelajar',
            'nama_penerima' => 'Nama Penerima',
            'jenis_bayaran' => 'Jenis Bayaran',
            'no_rujukan_pindahan_wang' => 'No Rujukan Pindahan Wang',
            'pelarasan' => 'Pelarasan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'enter_by' => 'Enter By',
            'update_by' => 'Update By',
        ];
    }

    public static function savebill($id,$jenis_pelajar)
    {
        $model = MaklumatPelajarPenjaga::findOne($id);
        $model2 = new YuranPendaftaran();

        if (isset($model)) {
            $model2->idstd = $model->id;
            $model2->created_at = date('Y-m-d h:i:s A');
            $model2->enter_by = Yii::$app->user->identity->id;
            $model2->tahun = date('Y');
            $model2->pusat_pengajian = $model->pusat_pengajian_id;
            $model2->status_yuran = 0;
            $model2->tahap_pelajar = $model->tahap_semasa;
            $model2->jenis_pelajar = $jenis_pelajar;

            $getInv = YuranPendaftaran::find()->where(['not', ['invoice_no' => null]])->orderBy(['id' => SORT_DESC])->limit(1)->one();

            if (empty($getInv['invoice_no'])) {
                $runninNo = 10000;
                $noinv = 'INV#'.$model2->namatahfiz->code_resit."-".$runninNo;
                //strlen
            } else {
                $len = strlen('INV#'.$model2->namatahfiz->code_resit) + 1; // count string length and plus 1 
                $qt = substr($getInv['invoice_no'], $len); //remove code tahfiz n dash (-)
                $new = $qt + 1; // plus 1 for new no of receipt
                $runninNo = $new;

                $noinv = 'INV#'.$model2->namatahfiz->code_resit."-".$runninNo;
            }

            $model2->invoice_no = $noinv;

            if ($model2->save()) {
                
                $yuran = LookupYurandaftar::find()
                        ->where(['tahap_pelajar'=>$model->tahap_semasa])
                        ->andWhere(['jenis_pelajar'=>$jenis_pelajar])
                        ->all();

                foreach ($yuran as $key => $value) {
                    $model3 = new YuranPendaftaranDetail;

                    $model3->item_daftar = $value->id;
                    $model3->price = $value->jumlah;
                    $model3->daftarid = $model2->id;
                    $model3->save();
                }
                //check yuran tertunggak tahun sebelum
                $oldyuran = OldYuranDaftar::find()
                            ->where(['id_pelajar'=>$id])
                            ->andWhere(['!=','yuran_tertunggak',0.00])
                            ->orderBy(['id'=>SORT_DESC])
                            ->one();

                if (isset($oldyuran)) {
                    $yuran_tertunggak = new YuranPendaftaranDetail();
                    $yuran_tertunggak->item_daftar = 27;
                    $yuran_tertunggak->price = $oldyuran->yuran_tertunggak;
                    $yuran_tertunggak->daftarid = $model2->id;
                    $yuran_tertunggak->save();
                }
                return $model2->id;
            }
        }
        else{
            return false;
        }
    }

    public function getNamatahfiz()
    {
        return $this->hasOne(LookupPusatPengajian::className(),['id' =>'pusat_pengajian']);
    }

    public function getNamastd()
    {
        return $this->hasOne(MaklumatPelajarPenjaga::className(),['id' =>'idstd']);
    }

    public function getDetails()
    {
        return $this->hasMany(YuranPendaftaranDetail::className(),['daftarid'=>'id'])->orderBy(['id'=>SORT_ASC]);
    }

    public function getPayments()
    {
        return $this->hasMany(YuranPaymentDetails::className(),['daftarid'=>'id']);
    }

    public function getBakitunggak()
    {
        $baki = ($this->jumlah_yuran - $this->jumlah_bayaran) + $this->pelarasan;

        return $baki;
    }

    public function getStatus()
    {
        if ($this->status_yuran == 1) {
           $status = 'Telah Dijana';
        }
        else if ($this->status_yuran == 2) {
           $status = 'Separa';
        }
        else if ($this->status_yuran == 3) {
            $status = 'Telah Bayar';
        }
        else if ($this->status_yuran == 0) {
           $status = 'Draf';
        }

        return $status;
    }

    public function getJenispelajar()
    {
        if ($this->jenis_pelajar == 1) {
            $jenis = 'Pelajar Baru';
        }
        elseif ($this->jenis_pelajar == 2) {
            $jenis = 'Pelajar Lama';
        }
        elseif ($this->jenis_pelajar == 3) {
            $jenis = 'Pelajar Lewat';
        }

        return $jenis;
    }

    public static function jenispelajar($id)
    {
        if ($id == 1) {
            $jenis = 'Pelajar Baru';
        }
        elseif ($id == 2) {
            $jenis = 'Pelajar Lama';
        }
        elseif ($id == 3) {
            $jenis = 'Pelajar Lewat';
        }

        return $jenis;
    }
}
