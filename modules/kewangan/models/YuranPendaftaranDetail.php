<?php

namespace app\modules\kewangan\models;

use Yii;
use app\models\LookupYurandaftar;

/**
 * This is the model class for table "yuran_pendaftaran_detail".
 *
 * @property int $id
 * @property int $item_daftar
 * @property double $price
 * @property double $jumlah
 * @property int $daftarid
 */
class YuranPendaftaranDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yuran_pendaftaran_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_daftar', 'daftarid','status_pay','payment_id'], 'integer'],
            [['price', 'jumlah'], 'number'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_daftar' => 'Item Daftar',
            'price' => 'Price',
            'jumlah' => 'Jumlah',
            'daftarid' => 'Daftarid',
        ];
    }

    public function getItemdaftar()
    {
        return $this->hasOne(LookupYurandaftar::className(),['id'=>'item_daftar']);
    }
}
