<?php

namespace app\modules\kewangan\models;

use Yii;
use app\models\LookupYurandaftar;

/**
 * This is the model class for table "yuran_payment_details".
 *
 * @property int $id
 * @property string $no_resit
 * @property string $jenis_yuran
 * @property string $jenis_pembayaran
 * @property string $pic_name
 * @property string $created_at
 * @property int $enter_by
 * @property string $update_at
 * @property int $update_by
 * @property double $jumlah_bayaran
 * @property double $jumlah_tunggakan
 * @property string $pelarasan
 * @property string $ulasan
 * @property double $pelarasan_tambahan
 * @property double $pelarasan_tolakan
 */
class YuranPaymentDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yuran_payment_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tarikh_bayaran','jumlah_bayaran','pic_name'],'required','message'=>'Ruangan {attribute} wajib diisi','on'=>'paymentprocess'],
            [['enter_by', 'update_by','daftarid'], 'integer'],
            [['jumlah_bayaran', 'jumlah_tunggakan', 'pelarasan_tambahan', 'pelarasan_tolakan'], 'number'],
            [['ulasan'], 'string'],
            [['no_resit', 'jenis_yuran', 'jenis_pembayaran', 'pic_name','no_rujukan_pindahan_wang'], 'string', 'max' => 255],
            [['created_at', 'update_at','tarikh_bayaran'], 'string', 'max' => 50],
            [['pelarasan'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_resit' => 'No Resit',
            'jenis_yuran' => 'Jenis Yuran',
            'jenis_pembayaran' => 'Jenis Pembayaran',
            'pic_name' => 'Nama Penerima',
            'created_at' => 'Created At',
            'enter_by' => 'Enter By',
            'update_at' => 'Update At',
            'update_by' => 'Update By',
            'jumlah_bayaran' => 'Jumlah Bayaran',
            'jumlah_tunggakan' => 'Jumlah Tunggakan',
            'pelarasan' => 'Pelarasan',
            'ulasan' => 'Ulasan',
            'pelarasan_tambahan' => 'Pelarasan Tambahan',
            'pelarasan_tolakan' => 'Pelarasan Tolakan',
        ];
    }

    public function getInv()
    {
        return $this->hasOne(YuranPendaftaran::className(),['id' =>'daftarid']);
    }

    public function getItempays()
    {
        return $this->hasMany(YuranPendaftaranDetail::className(),['payment_id'=>'id']);
    }
}
