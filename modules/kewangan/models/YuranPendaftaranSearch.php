<?php

namespace app\modules\kewangan\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\kewangan\models\YuranPendaftaran;

/**
 * YuranPendaftaranSearch represents the model behind the search form of `app\modules\kewangan\models\YuranPendaftaran`.
 */
class YuranPendaftaranSearch extends YuranPendaftaran
{
    /**
     * {@inheritdoc}
     */
    public $globalyuran;

    public function rules()
    {
        return [
            [['id', 'idstd', 'pusat_pengajian', 'tahun', 'tahap_pelajar', 'guru_kelas', 'guru_kanan', 'status_yuran', 'jenis_pelajar', 'enter_by', 'update_by'], 'integer'],
            [['invoice_no', 'tarikh_bayaran', 'nota', 'nama_penerima', 'jenis_bayaran', 'no_rujukan_pindahan_wang', 'created_at', 'updated_at','globalyuran'], 'safe'],
            [['jumlah_yuran', 'jumlah_bayaran', 'yuran_tertunggak_daftar', 'pelarasan'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = YuranPendaftaran::find();
        $query->joinWith('namastd');
        $query->joinWith('payments');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'idstd' => $this->idstd,
            'pusat_pengajian' => $this->pusat_pengajian,
            'jumlah_yuran' => $this->jumlah_yuran,
            'jumlah_bayaran' => $this->jumlah_bayaran,
            'yuran_tertunggak_daftar' => $this->yuran_tertunggak_daftar,
            'tahun' => $this->tahun,
            'tahap_pelajar' => $this->tahap_pelajar,
            'guru_kelas' => $this->guru_kelas,
            'guru_kanan' => $this->guru_kanan,
            'status_yuran' => $this->status_yuran,
            'jenis_pelajar' => $this->jenis_pelajar,
            'pelarasan' => $this->pelarasan,
            'enter_by' => $this->enter_by,
            'update_by' => $this->update_by,
        ]);

        $query->orFilterWhere(['like', 'invoice_no', $this->globalyuran])
            ->orFilterWhere(['like', 'yuran_pendaftaran.created_at', $this->globalyuran])
            ->orFilterWhere(['like', 'maklumat_pelajar_penjaga.nama_pelajar', $this->globalyuran])
            ->orFilterWhere(['like', 'yuran_payment_details.no_resit', $this->globalyuran])
            ->orFilterWhere(['like', 'updated_at', $this->globalyuran]);

        return $dataProvider;
    }
}
