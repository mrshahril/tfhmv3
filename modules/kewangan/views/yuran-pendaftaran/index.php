<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ButtonDropdown;

use app\modules\kewangan\models\YuranPendaftaran;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\kewangan\models\YuranPendaftaranSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Yuran Pendaftaran';
$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('yuran-pendaftaran/index')){
        $('.main-menu > .scroll > ul > li#kewangan').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }
});
JS;
$this->registerJs($script);
?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Tambah Baru',['create'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <?= $this->render('daftaryuran/_search', ['model' => $searchModel]);  ?>
       
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body table-responsive">
                <?php Pjax::begin(['id'=>'some_pjax_id']); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{summary}\n{items}\n<nav class='mt-4 mb-3'>{pager}</nav>",
                        'pager' => [
                            'firstPageLabel' => 'Mula',
                            'lastPageLabel' => 'Akhir',
                            'prevPageLabel' => 'Sebelumnya',
                            'nextPageLabel' => 'Seterusnya',

                            'maxButtonCount' => 5,

                             'options' => [
                                'tag' => 'ul',
                                'class' => 'pagination justify-content-center mb-0',
                            ],
                            'linkContainerOptions'=>['class'=>'page-item'],
                            'linkOptions' => ['class' => 'page-link'],
                            'activePageCssClass' => 'active',
                        ],
                        'tableOptions'=>['class'=>'table table-striped'],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                                'namastd.nama_pelajar',
                                'invoice_no',
                                [
                                    'label'=>'No Resit',
                                    'format'=>'raw',
                                    'value'=>function($data){
                                        $resit = '';
                                        foreach ($data->payments as $key => $value) {
                                            $resit .= $value->no_resit;
                                            $resit .= '<br>';
                                        }
                                        return $resit;
                                    }
                                ],
                                'namatahfiz.pusat_pengajian',
                                'tahap_pelajar',
                                [
                                    'label'=>'Tarikh Bayaran',
                                    'format'=>'raw',
                                    'value'=>function($data){
                                        $date = '';
                                        foreach ($data->payments as $key => $value) {
                                            $date .= date('d-m-Y',strtotime($value->tarikh_bayaran));
                                            $date .= '<br>';
                                        }
                                        return $date;
                                    }
                                ],
                                [
                                    'attribute'=>'jumlah_yuran',
                                    'format'=>'raw',
                                    'value'=>function($data){
                                        return number_format($data->jumlah_yuran,2);
                                    }
                                ],
                                [
                                    'attribute'=>'jumlah_bayaran',
                                    'format'=>'raw',
                                    'value'=>function($data){
                                        return number_format($data->jumlah_bayaran,2);
                                    }
                                ],
                                'status',
                                [
                                    'header' => 'Tindakan',
                                    'class' => 'yii\grid\ActionColumn',
                                    'template'=>'{all}',
                                    'buttons' => [
                                        'all' => function ($url, $model, $key) {
                                            return ButtonDropdown::widget([
                                                'encodeLabel' => false, // if you're going to use html on the button label
                                                'label' => 'Pilih Tindakan',
                                                'dropdown' => [
                                                    'encodeLabels' => false, // if you're going to use html on the items' labels
                                                    'items' => [
                                                        [
                                                            'label' => \Yii::t('yii', 'Lihat Maklumat'),
                                                            'url' => ['yuran-pendaftaran/payyuran', 'id' =>base64_encode($key)],
                                                            'linkOptions' => ['class'=>'dropdown-item'],
                                                            'visible' => true,  // if you want to hide an item based on a condition, use this
                                                        ],
                                                        
                                                    ],
                                                    
                                                ],
                                                'options' => [
                                                    'class' => 'btn btn-outline-primary btn-sm',   // btn-success, btn-info, et cetera
                                                ],
                                                'split' => false,    // if you want a split button
                                            ]);
                                        },
                                    ],
                                ],
                        ],
                    ]); ?>
                <?php Pjax::end(); ?>

            </div>
        </div>
    </div>
</div>
