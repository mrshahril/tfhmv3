<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\kewangan\models\YuranPendaftaranSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="yuran-pendaftaran-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'idstd') ?>

    <?= $form->field($model, 'no_resit') ?>

    <?= $form->field($model, 'tarikh_bayaran') ?>

    <?= $form->field($model, 'pusat_pengajian') ?>

    <?php // echo $form->field($model, 'jumlah_yuran') ?>

    <?php // echo $form->field($model, 'jumlah_bayaran') ?>

    <?php // echo $form->field($model, 'yuran_tertunggak_daftar') ?>

    <?php // echo $form->field($model, 'tahun') ?>

    <?php // echo $form->field($model, 'tahap_pelajar') ?>

    <?php // echo $form->field($model, 'guru_kelas') ?>

    <?php // echo $form->field($model, 'guru_kanan') ?>

    <?php // echo $form->field($model, 'status_yuran') ?>

    <?php // echo $form->field($model, 'nota') ?>

    <?php // echo $form->field($model, 'jenis_pelajar') ?>

    <?php // echo $form->field($model, 'nama_penerima') ?>

    <?php // echo $form->field($model, 'jenis_bayaran') ?>

    <?php // echo $form->field($model, 'no_rujukan_pindahan_wang') ?>

    <?php // echo $form->field($model, 'pelarasan_tambahan') ?>

    <?php // echo $form->field($model, 'pelarasan_tolakan') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'enter_by') ?>

    <?php // echo $form->field($model, 'update_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
