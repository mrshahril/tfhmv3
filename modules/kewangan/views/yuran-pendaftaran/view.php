<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\kewangan\models\YuranPendaftaran */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Yuran Pendaftarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="yuran-pendaftaran-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'idstd',
            'no_resit',
            'tarikh_bayaran',
            'pusat_pengajian',
            'jumlah_yuran',
            'jumlah_bayaran',
            'yuran_tertunggak_daftar',
            'tahun',
            'tahap_pelajar',
            'guru_kelas',
            'guru_kanan',
            'status_yuran',
            'nota:ntext',
            'jenis_pelajar',
            'nama_penerima',
            'jenis_bayaran',
            'no_rujukan_pindahan_wang',
            'pelarasan_tambahan',
            'pelarasan_tolakan',
            'created_at',
            'updated_at',
            'enter_by',
            'update_by',
        ],
    ]) ?>

</div>
