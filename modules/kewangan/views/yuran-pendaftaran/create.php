<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\kewangan\models\YuranPendaftaran */

$this->title = 'Create Yuran Pendaftaran';
$this->params['breadcrumbs'][] = ['label' => 'Yuran Pendaftarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="yuran-pendaftaran-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
