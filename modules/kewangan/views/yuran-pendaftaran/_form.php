<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\kewangan\models\YuranPendaftaran */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Tambah Yuran Daftar';

$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('yuran-pendaftaran/create')){
        $('.main-menu > .scroll > ul > li#kewangan').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }
    
     $(".custom-control-input").click(function () {
         $('input:checkbox').not(this).prop('checked', this.checked);
     });
    $('#yuranpendaftaran-tahap_pelajar').change(function(){
        var id = $(this).val();
        var idstd = $('#yuranpendaftaran-idstd').val();
        var jenispelajar = $('#yuranpendaftaran-jenis_pelajar').val();
        $.ajax({
                type: 'POST',
                url: 'listyuran',
                data: {id: id, idstd: idstd,jenispelajar: jenispelajar},
                success: function(data) {
                    $('#infoyuran').show();
                    $('#yurantable > tbody').html(data);
               }
            });
    });

    $('#yuranpendaftaran-idstd').change(function(){
        var v = $(this).val();
        $.get('stdinfo',{id : v},function(data){
            var data = $.parseJSON(data);
            $('#yuranpendaftaran-tahap_pelajar').val(data.tahap_semasa);
        });
    });

    $('#yuranpendaftaran-jenis_pelajar').change(function(){
        var id = $(this).val();
        $.ajax({
            type: 'GET',
            url: 'filteryuran',
            data : {id: id},
            success: function(data) {
                $("select#yuranpendaftaran-tahap_pelajar").html(data);
            }
        });
    });
    
    $(".calc").click(function(event) {
        $('.proceed').show();
        var total = 0;
        $(".test:checked").each(function() {
            total += parseInt($(this).data("value")); //$(this).data("id")
        });
        
        if (total == 0) {
            $('#yuranpendaftaran-jumlah_yuran').val('');
        } else {                
            $('#yuranpendaftaran-jumlah_yuran').val(total.toFixed(2));
        }
    });

    $(".addyuran").submit(function() {

        if($('#yuranpendaftaran-idstd').val() != null){
            return true;
        }
        else{
            var error = 'Ruangan Nama Pelajar wajib diisi';
            $('.idstd').text(error);
            return false;
        }
    });
    $('.select2-single').select2({
        placeholder:'Sila Pilih',
        ajax: {
            url: 'stdnt',
            dataType: 'json',
            delay:250
        }
    });
});
JS;
$this->registerJs($script);
?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div id="test">
    
</div>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('saveerror')) { ?>
            <div class="alert alert-danger" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('saveerror'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php $form = ActiveForm::begin([
                'options'=>['class'=>'addyuran'],
                
            ]); ?>
                <div class="box-body">
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Nama Pelajar <span class="font-red">**</span></label>
                                <select id="yuranpendaftaran-idstd" class="select2-single" name="YuranPendaftaran[idstd]"></select>
                                <div class="help-block idstd"></div>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Jenis Pelajar <span class="font-red">**</span></label>
                                <?= $form->field($model,'jenis_pelajar')->dropDownList([
                                    '1'=>'Pelajar Baru',
                                    '2'=>'Pelajar Lama',
                                    '3'=>'Pelajar Lewat',
                                ],
                                [
                                    'prompt'=>'--Sila Pilih Jenis Pelajar--',
                                ]
                                )->label(false) ?>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Tahap Pelajar <span class="font-red">**</span></label>
                                <?= $form->field($model, 'tahap_pelajar')->dropDownList([
                                    '1'=>'1',
                                    '2'=>'2',
                                    '3'=>'3',
                                    '4'=>'4',
                                    '5'=>'5',
                                    '6'=>'6',
                                ], ['prompt' => '--Sila Pilih--'])->label(false) ?>
                            </div>
                        </div>
                        <div class="form-row" style="display: none" id="infoyuran">
                            <div class="form-group col-md-12">
                                <table class="table table-stripe" id="yurantable">
                                    <thead>
                                        <tr>
                                            <th>Jenis Bayaran</th>
                                            <th>Jumlah (RM)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                    <div class="box-footer">
                        <div class="col-md-12">
                            
                             <?= Html::submitButton('Simpan', ['class' => 'btn btn-success btn-block mb-1','id'=>'btnsbmit']) ?>
                            <?= Html::a('Batal',['index'],['class'=>'btn btn-secondary btn-block mb-1']) ?>
                        </div>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
