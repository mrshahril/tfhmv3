<?php 
use yii\helpers\Html;


?>

<div class="row text" style="margin:0px;">
	<div class="col-xs-12">
		<div style="border-bottom: 1px solid #dcdcdc;padding: 10px;">
			<div class="col-xs-6">
				<?= Html::img(Yii::getAlias('@web').'/image/logotafhim_1.png',['class'=>'invoice-logo']);  ?>
			</div>
			<div class="col-xs-pull-8">
				<address>
                    <strong>TAFHIM SDN BHD (560472-M) </strong><br>
                    No. 9, Jalan  1/4, Seksyen 1,<br>
                    43650 Bandar Baru Bangi, Selangor.<br>
                    <br>
                    Tel : 03-89258486
                    <br>
                    Fax : 03-89127784
                </address>
                
			</div>
		</div>
		<div style='margin:0 auto;width: 400px;text-align: center;'>
			<h3 style="font-size: 18px"><b>Invois</b></h3>
			
		</div>
	</div>
</div>
<div class="row text">
	<div style="padding: 10px;">
		<div class="col-xs-6">
			Bayar Kepada
			<address>
				<strong><?= $model->namastd->nama_pelajar ?></strong>
				<br>
				<?= $model->namastd->alamat_rumah ?>
			</address>
		</div>	
		<div class="col-xs-pull-6">
			<div class="table-responsive">
    			<table class="table no-border text" >
    				<tbody>
    					<tr>
    						<th style="border-top:none;">No. Invois:</th>
    						<td style="border-top:none;"><?= $model->invoice_no ?></td>
    					</tr>
    					<tr>
    						<th style="border-top:none;">Tarikh Invois:</th>
    						<td style="border-top:none;"><?= date('d M Y',strtotime($model->created_at)) ?></td>
    					</tr>
    				</tbody>
    			</table>
    		</div>
		</div>
	</div>
</div>
<div class="row text">
	<div class="col-xs-12">
		<table class="table table-striped txttable" style="border-color: 1px solid #908d8d !important;font-family: Courier !important;"  >
			<thead>
				<tr style="background-color: #f9f9f9 !important;"> 
					<th width="5">#</th>
					<th width="550">Item</th>
					<th>Jumlah (RM)</th>
				</tr>
			</thead>
			<tbody>
	           	<?php $gross = 0; ?>

				<?php foreach ($model->details as $key => $value): ?>
					<tr>
						<td><?= $key+1 ?></td>
						<td>
							<?= $value->itemdaftar->jenis_bayaran ?>
							<br>
							<?= $value->item_daftar == 25 ? $model->ulasan_pelarasan : '' ?>
						</td>
	                    <td><?= number_format($value->price,2) ?></td>
					</tr>
					<?php $gross += $value->price; ?>
				<?php endforeach ?>
				<tr>
        			<td align="right" colspan="2"><b>Jumlah Yuran </b></td>
        			<td><?= number_format($gross,2) ?></td>
        		</tr>
        		<?php if ($model->status_yuran != 0 && $model->status_yuran != 1): ?>
	        		<tr>
	        			<td align="right" colspan="2"><b>Amaun Dibayar</b></td>
	        			<td>(<?= $model->jumlah_bayaran != 0.00 ? number_format($model->jumlah_bayaran,2) : '0.00' ?>)</td>
	        		</tr>
	        		<tr>
	        			<td align="right" colspan="2"><b>Jumlah/Baki Keseluruhan</b></td>
	        			<td>
	        				<?= $model->yuran_tertunggak_daftar != 0.00 ? number_format($model->yuran_tertunggak_daftar,2) : '0.00' ?>
	        			</td>
	        		</tr>
                <?php endif ?>
			</tbody>
		</table>
	</div>
</div>