<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use app\modules\kewangan\models\YuranPendaftaran;

$this->title = 'Pelarasan Yuran Pendaftaran - '.$model->invoice_no;

$script = <<< JS
$(document).ready(function(){
	if($('#pelajar').hasClass('yuran-pendaftaran/pelarasan')){
        $('.main-menu > .scroll > ul > li#kewangan').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }
});
JS;
$this->registerJs($script);

?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
        	<?php $form = ActiveForm::begin(); ?>
        	<div class="box-body">
        		<form>
        			<div class="form-row">
        				<div class="form-group col-md-12">
                            <label>Pelarasan <i><b>(Nyatakan nilai negatif jika berlaku penolakan pelarasan atau sebaliknya)</b></i> </label>
                            <?= $form->field($model, 'pelarasan')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                        </div>
        			</div>
        			<div class="form-row">
        				<div class="form-group col-md-12">
                            <label>Ulasan</label>
                            <?= $form->field($model, 'ulasan_pelarasan')->textArea(['row' => 6,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                        </div>
        			</div>
        		</form>
        		<div class="box-footer">
                    <div class="col-md-12">
                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success btn-block mb-1','id'=>'btnsbmit']) ?>
                        <?= Html::a('Batal',['payyuran','id'=>base64_encode($model->id)],['class'=>'btn btn-secondary btn-block mb-1']) ?>
                    </div>
                </div>
        	</div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>