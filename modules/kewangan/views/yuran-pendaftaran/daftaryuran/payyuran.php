<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use app\modules\kewangan\models\YuranPendaftaran;

$this->title = 'Yuran Pendaftaran Invois - '.$model->invoice_no;

$script = <<< JS
$(document).ready(function(){
	if($('#yurandaftar').hasClass('yuran-pendaftaran/payyuran')){
        $('.main-menu > .scroll > ul > li#kewangan').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    $('.modaltest').click(function(){
        $('#exampleModal').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });

    deleteswal();
    janaswal();

    function deleteswal(){
        $('a.delete').on('click', function(e){
             e.preventDefault();
            var link = $(this).attr('value');
            console.log(link);
          swal({   
            title: "Anda pasti mahu hapus item ini dari sistem ?",
            // text: "Perhatian ! Proses ini tidak boleh diulang semua !",   
            type: "warning",   
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: 'Ya, saya pasti.',
            confirmButtonClass: 'btn-info',
            cancelButtonText: 'Batal',
            cancelButtonClass: 'btn-danger',

          }, 
           function(){   
                // $("#myform").submit();
                $.ajax({
                    type: 'POST',
                    url: link,
                    success: function(data) {
                    
                   }
                });
            });
        })
    }

    //jana yuran
    function janaswal(){
        $('a.jana').on('click', function(e){
             e.preventDefault();
            var link = $(this).attr('value');
            console.log(link);
          swal({   
            title: "Klik YA jika mahu meneruskan transaksi/proses ini.",
            type: "warning",   
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: 'Ya',
            confirmButtonClass: 'btn-success',
            cancelButtonText: 'Batal',
            cancelButtonClass: 'btn-danger',

          }, 
           function(){   
                $.ajax({
                    type: 'POST',
                    url: link,
                    success: function(data) {
                    
                   }
                });
            });
        })
    }
});
JS;
$this->registerJs($script);
?>

<span id="yurandaftar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>

        <div class="separator mb-5"></div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box box-success invoice">
			<div id="badge">
                <div class="arrow-up"></div>
                <div class="label bg-aqua"><center><b><?= $model->status ?></b></center></div>
                <div class="arrow-right"></div>
            </div>
			<div class="row invoice-header">
				<div class="col-xs-7">
					<?= Html::img(Yii::getAlias('@web').'/image/logotafhim_1.png',['class'=>'invoice-logo']);  ?>
				</div>
				<div class="col-xs-5 invoice-company"">
					<address>
                        <strong>TAFHIM SDN BHD (560472-M) </strong><br>
                        No. 9, Jalan  1/4, Seksyen 1,<br>
                        43650 Bandar Baru Bangi, Selangor.<br>
                        <br>
                        Tel : 03-89258486 <span>&nbsp;&nbsp;&nbsp;&nbsp;</span> Fax : 03-89127784
                    </address>
				</div>
            </div>
			<div class="box-body">
				<div class="row">
					<div class="col-xs-7">
						Kepada :
						<address>
							<strong><?= $model->namastd->nama_pelajar ?></strong>
							<br>
							<?= $model->namastd->alamat_rumah ?>
						</address>
						<br>
						<span><b>Jenis Pelajar: </b><?= $jenis = YuranPendaftaran::jenispelajar($model->jenis_pelajar) ?></span>
						<br>
						<span><b>Tahap Semasa: </b><?= $model->tahap_pelajar; ?></span>
						<br>
						<span><b>Pusat Pengajian: </b><?= $model->namatahfiz->pusat_pengajian ?></span>
						<div class="mb-5"></div>
					</div>
                	<div class="col-xs-5">
                		<div class="table-responsive">
                			<table class="table no-border">
                				<tbody>
                					<tr>
                						<th class="">No. Invois:</th>
                						<td><?= $model->invoice_no ?></td>
                					</tr>
                					<tr>
                						<th>Tarikh Invois:</th>
                						<td><?= date('d M Y',strtotime($model->created_at)) ?></td>
                					</tr>
                				</tbody>
                			</table>
                		</div>
                	</div>
				</div>
				<div class="row">
	                <div class="col-xs-12 table-responsive">
	                    <table class="table table-striped">
	                    	<thead>
	                    		<?php $model->status_yuran == 1 || $model->status_yuran == 2 || $model->status_yuran == 3 ? $hide = 'style=display:none' : $hide = '' ?>
	                    		<tr>
	                    			<th <?php echo $hide ?>>Tindakan</th>
	                    			<th>Item</th>
	                    			<th>Jumlah (RM)</th>
	                    		</tr>
	                    	</thead>
	                    	<tbody>
	                    		<?php $gross = 0; ?>
	                    		
	                    		<?php foreach ($model->details as $key => $value): ?>
	                    			<tr>
	                    				<td <?php echo $hide ?>>
	                    					<?= Html::a('<i class="iconsmind-Trash-withMen" style="font-size:18px"></i>',FALSE,['value'=>Url::to(['padamitemyuran','id'=>base64_encode($value->id)]),'class' => 'btn btn-xs btn-danger delete','title' => 'Padam Item','data-toggle'=>'tooltip','data-placement'=>'bottom','style'=>'color:#fff']) ?>
	                    					</td>
	                    				<td>
                                            <?= $value->itemdaftar->jenis_bayaran ?>
                                            <br>
                                            <?= $value->item_daftar == 25 ? $model->ulasan_pelarasan : '' ?> 
                                        </td>
	                    				<td><?= number_format($value->price,2) ?></td>
	                    			</tr>
	                    			<?php $gross += $value->price; ?>
	                    		<?php endforeach ?>
	                    		<tr>
	                    			<td <?php echo $hide ?>></td>
	                    			<td align="right"><b>Jumlah Yuran </b></td>
	                    			<td><?= number_format($gross,2) ?></td>
	                    		</tr>
	                    		 <?php if ($model->status_yuran != 0 && $model->status_yuran != 1): ?>
                                     <tr>
                                        <td <?php echo $hide ?>></td>
                                        <td align="right"><b>Amaun Dibayar</b></td>
                                        <td>(<?= $model->jumlah_bayaran != 0.00 ? number_format($model->jumlah_bayaran,2) : '0.00' ?>)</td>
                                    </tr>
                                    <tr>
                                        <td <?php echo $hide ?>></td>
                                        <td align="right"><b>Jumlah Bersih</b></td>
                                        <td>
                                            <?= number_format($model->bakitunggak,2) ?>
                                        </td>
                                    </tr>
                                 <?php endif ?>
	                    	</tbody>
	                    </table>
	                </div>
	            </div>
			</div>
            <div class="box-footer row no-print">
            	<div class="col-md-12">
            		<?php if ($model->status_yuran != 1 && $model->status_yuran != 2 && $model->status_yuran != 3) { ?>
            		<?= Html::a('Kemaskini Yuran',FALSE,['value'=>Url::to(['edityuran','id'=>base64_encode($model->id)]),'class' => 'btn btn-info mb-1 modaltest','id'=>'modaltest']) ?>
            		<?= Html::a('Jana Invois Yuran',FALSE,['value'=>Url::to(['janadaftar','id'=>base64_encode($model->id),'refer'=>'jana']),'class' => 'btn btn-success jana','style'=>'color:#fff']) ?>
                    <?= Html::a('Jana Dan Bayar Yuran',FALSE,['value'=>Url::to(['janadaftar','id'=>base64_encode($model->id)]),'class' => 'btn btn-primary jana','style'=>'color:#fff']) ?>
            	<?php }else{ ?>
            		<button id="btnGroupDrop1" type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tindakan</button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                        <?= Html::a('Print',['printinv','id'=>base64_encode($model->id)],['class'=>'dropdown-item','target'=>'_BLANK']) ?>
                        <?php if($model->status_yuran != 3){ ?>
                        <?= Html::a('Tambah Pembayaran',['paymentprocess','id'=>base64_encode($model->id)],['class'=>'dropdown-item']) ?>
                        <?php } ?>
                        <?= Html::a('Pelarasan',['pelarasan','id'=>base64_encode($model->id)],['class'=>'dropdown-item']) ?>
                    </div>
            	<?php } ?>
            	</div>
            </div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box box-danger invoice">
			<div class="box-title">
				<h4>Rekod Pembayaran</h4>
			</div>
			<div class="box-body table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Tarikh</th>
							<th>No Resit</th>
							<th>Ulasan</th>
							<th>Jumlah (RM)</th>
							<th>Tindakan</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($model->payments as $key => $value): ?>
							<tr>
								<td><?= date('d-M-Y',strtotime($value->tarikh_bayaran)) ?></td>
								<td><?= $value->no_resit ?></td>
								<td><?= $value->ulasan ?></td>
								<td><?= number_format($value->jumlah_bayaran,2) ?></td>
								<td><?= Html::a('Print Resit',['printresit','id'=>base64_encode($value->id)],['class'=>'btn btn-outline-primary','target'=>'_BLANK']) ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>