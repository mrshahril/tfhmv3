<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use app\modules\kewangan\models\YuranPendaftaran;

$this->title = 'Yuran Pendaftaran Invois - '.$model->invoice_no;

$script = <<< JS
$(document).ready(function(){
	$('#yuranpendaftaran-jenis_pelajar').change(function(){
        var id = $(this).val();
        $.ajax({
            type: 'GET',
            url: 'filteryuran',
            data : {id: id},
            success: function(data) {
                $("select#yuranpendaftaran-tahap_pelajar").html(data);
            }
        });
    });

});
JS;
$this->registerJs($script);
?>
<div class="row">
	<div class="col-md-12">
		<?php $form = ActiveForm::begin([
            'options'=>['class'=>'addyuran'],
            
        ]); ?>
    	<form>
        	<div class="form-row">
        		<div class="form-group col-md-6">
                    <label>Jenis Pelajar <span class="font-red">**</span></label>
                    <?= $form->field($model,'jenis_pelajar')->dropDownList([
                        '1'=>'Pelajar Baru',
                        '2'=>'Pelajar Lama',
                        '3'=>'Pelajar Lewat',
                    ],
                    [
                        'prompt'=>'--Sila Pilih Jenis Pelajar--',
                    ]
                    )->label(false) ?>
                </div>
                <div class="form-group col-md-6">
                    <label>Tahap Pelajar/Tahap Semasa <span class="font-red">**</span></label>
                    <?= $form->field($model, 'tahap_pelajar')->dropDownList([
                        '1'=>'1',
                        '2'=>'2',
                        '3'=>'3',
                        '4'=>'4',
                        '5'=>'5',
                        '6'=>'6',
                    ], ['prompt' => '--Sila Pilih--'])->label(false) ?>
                </div>
        	</div>
        	<div class="modal-footer">
	            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
	            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success mb-1']) ?>
	        </div>
    	</form>
    	
		<?php ActiveForm::end(); ?>
	</div>
</div>
