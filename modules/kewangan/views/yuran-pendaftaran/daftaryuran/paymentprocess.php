<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use app\modules\kewangan\models\YuranPendaftaran;

$this->title = 'Bayaran Yuran Pendaftaran - '.$model->invoice_no;

$script = <<< JS
$(document).ready(function(){
	if($('#yurandaftar').hasClass('maklumat-pelajar-penjaga/payyuran')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    $('.modaltest').click(function(){
        $('#exampleModal').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });

    deleteswal();
    janaswal();

    function deleteswal(){
        $('a.delete').on('click', function(e){
             e.preventDefault();
            var link = $(this).attr('value');
            console.log(link);
          swal({   
            title: "Anda pasti mahu hapus item ini dari sistem ?",
            // text: "Perhatian ! Proses ini tidak boleh diulang semua !",   
            type: "warning",   
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: 'Ya, saya pasti.',
            confirmButtonClass: 'btn-info',
            cancelButtonText: 'Batal',
            cancelButtonClass: 'btn-danger',

          }, 
           function(){   
                // $("#myform").submit();
                $.ajax({
                    type: 'POST',
                    url: link,
                    success: function(data) {
                    
                   }
                });
            });
        })
    }

    //jana yuran
    function janaswal(){
        $('a.jana').on('click', function(e){
             e.preventDefault();
            var link = $(this).attr('value');
            console.log(link);
          swal({   
            title: "Invois yuran pendaftaran akan dijana. Klik YA jika mahukan meneruskan pembayaran.",
            type: "warning",   
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: 'Ya',
            confirmButtonClass: 'btn-success',
            cancelButtonText: 'Batal',
            cancelButtonClass: 'btn-danger',

          }, 
           function(){   
                $.ajax({
                    type: 'POST',
                    url: link,
                    success: function(data) {
                    
                   }
                });
            });
        })
    }

    // $(".custom-control-input").click(function () {
    //      $('input:checkbox').not(this).prop('checked', this.checked);
    // });
    $('.selectall').click(function() {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

    $('.custom-control-input').change(function (){
        var total = 0;
        $('input:checkbox:checked').each(function(){
            total += isNaN(parseInt($(this).data('value'))) ? 0 : parseInt($(this).data('value'));
        });
        
        var tyuran = $('#yuranpendaftaran-jumlah_yuran').val() - total;
        $('#yuranpaymentdetails-jumlah_bayaran').val(total);
        $('#yuranpendaftaran-yuran_tertunggak_daftar').val(tyuran);
        $('#total').val(total);
        $('#btnsbmit').prop('disabled', false);

    });

    $('#yuranpaymentdetails-jumlah_bayaran').keyup(function(){
        var v = $('#yuranpendaftaran-jumlah_yuran').val();
        var byr = v - $(this).val();
        var b = $('#yuranpendaftaran-yuran_tertunggak_daftar').val(byr.toFixed(2));

        if($(this).val() == 0){
            $('#btnsbmit').prop('disabled', true);
        }
        else{
            $('#btnsbmit').prop('disabled', false);
        }

        if($(this).val() < $('#total').val()){
            $('#btnsbmit').prop('disabled', true);
        }
        else{
            $('#btnsbmit').prop('disabled', false);
        }
        
    });

    $("#yuranpaymentdetails-jumlah_bayaran").bind("mousewheel", function() {
        return false;
    });
    $("#yuranpaymentdetails-pelarasan_tambahan").bind("mousewheel", function() {
        return false;
    });
});
JS;
$this->registerJs($script);
?>

<span id="yurandaftar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-body">
                <?php $form = ActiveForm::begin([
                'options'=>['class'=>'addyuran'],
                
            ]); ?>
                <div class="box-body">
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Nama Pelajar <span class="font-red">**</span></label>
                                <input type="text" disabled="" value="<?= $model->namastd->nama_pelajar ?>" class="form-control" name="">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Tarikh Bayaran <span class="font-red">**</span></label>
                                <?= $form->field($model2, 'tarikh_bayaran')->textInput(['maxlength' => true,'class'=>'form-control datepicker','data-date-format'=>'yyyy-mm-dd','autocomplete'=>'off'])->label(false) ?>
                            </div>
                        </div>
                        <div class="form-row" id="infoyuran">
                            <div class="form-group col-md-12">
                                <label><i><b>Sila tandakan (&#10004; ) jenis bayaran berdasarkan pembayaran oleh pelajar/penjaga.</b></i></label>
                                <table class="table table-stripe" id="yurantable">
                                    <thead>
                                        <tr>
                                            <th width="10">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input selectall" id="customCheckThis" name="selectitemyuran">
                                                    <label class="custom-control-label" for="customCheckThis"></label>
                                                </div>
                                            </th>
                                            <th>Jenis Bayaran</th>
                                            <th>Jumlah (RM)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($model3 as $key => $value): ?>
                                            <tr>
                                                <td width="10">
                                                    <div class="custom-control custom-checkbox mb-4">
                                                        <input type="checkbox" name="selectitemyuran[]" class="custom-control-input" id="customCheckThis<?= $key ?>" value="<?= $value->id ?>" data-value="<?= $value->price ?>">
                                                        <label class="custom-control-label" for="customCheckThis<?= $key ?>"></label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?= $value->itemdaftar->jenis_bayaran ?>
                                                    <br>
                                                    <?= $value->item_daftar == 25 ? $model->ulasan_pelarasan : '' ?>
                                                </td>
                                                <td><?= number_format($value->price,2) ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- <span class="calc btn btn-primary mb-4">Lakukan Pengiraan</span> -->
                        </div>
                        <div class="proceed">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>Jumlah/Baki Yuran</label>
                                    <?php $model->jumlah_yuran = ($model->jumlah_yuran - $model->jumlah_bayaran) + $model->pelarasan; ?>
                                    <?= $form->field($model, 'jumlah_yuran',['template' => '{input}{error}{hint}<div style="color:#e73d4a"></div>'])->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off','disabled'=>''])->label(false)?>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Jumlah Perlu Dibayar <span class="font-red">**</span></label>
                                    <?= $form->field($model2, 'jumlah_bayaran')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off','type'=>'number','readonly'])->label(false)?>
                                    <input type="hidden" name="total" id="total">
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Baki Tertunggak</label>
                                    <?= $form->field($model, 'yuran_tertunggak_daftar')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off','readonly'=>'','type'=>'number'])->label(false)?>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>Nama Penerima <span class="font-red">**</span></label>
                                    <?= $form->field($model2, 'pic_name')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false)?>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Jenis Bayaran</label>
                                    <?= $form->field($model2,'jenis_pembayaran')->dropDownList([
                                        'Tunai'=>'Tunai',
                                        'Cek'=>'Cek',
                                        'Pindahan Bank'=>'Pindahan Bank',
                                    ],
                                    [
                                        'prompt'=>'--Sila Pilih Jenis Bayaran--',
                                    ]
                                    )->label(false) ?>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>No. Rujukan Pindahan Wang</label>
                                    <?= $form->field($model2, 'no_rujukan_pindahan_wang')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false)?>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Ulasan</label>
                                <?= $form->field($model2, 'ulasan')->textArea(['rows'=>6])->label(false)?>

                            </div>
                        </div>
                    </form>
                    <div class="box-footer">
                        <div class="col-md-12">
                            <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>
                            
                            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success btn-block mb-1','id'=>'btnsbmit','disabled'=>'']) ?>
                            <?= Html::a('Batal',['payyuran','id'=>base64_encode($model->id)],['class'=>'btn btn-secondary btn-block mb-1']) ?>
                        </div>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>