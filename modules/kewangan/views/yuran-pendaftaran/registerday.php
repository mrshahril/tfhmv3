<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\GuruKanan;

use app\modules\hr\staff\models\MaklumatKakitangan;

/* @var $this yii\web\View */
/* @var $model app\modules\kewangan\models\YuranPendaftaran */
/* @var $form yii\widgets\ActiveForm */
$gurukanan = ArrayHelper::map(GuruKanan::find()->where(['id_pusat_pengajian'=>Yii::$app->user->identity->tahfiz])->all(), 'id_staff', function($data) {
        return $data->gurukanan->nama;
    });
$namastaf = ArrayHelper::map(MaklumatKakitangan::find()->where(['status_pekerjaan'=>1])->andWhere(['tahfiz'=>Yii::$app->user->identity->tahfiz])->asArray()->all(), 'id_staf', 'nama');


$this->title = 'Yuran Daftar';

$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('yuran-pendaftaran/create')){
        $('.main-menu > .scroll > ul > li#kewangan').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    yuran();

    function yuran()
    {
        var v = $('#yuranpendaftaran-idstd').val();
        $.get('stdinfo',{id : v},function(data){
            var data = $.parseJSON(data);
            $('#yuranpendaftaran-tahap_pelajar').val(data.tahap_semasa);
            $('#yuranpendaftaran-guru_kelas').val(data.guru_kelas);
            $('#infoyuran').show();
            $.ajax({
                type: 'POST',
                url: 'listyuran',
                data: {id: data.tahap_semasa},
                success: function(data) {
                    $('#yurantable > tbody').html(data);
               }
            });
        });
    }
    
     $(".custom-control-input").click(function () {
         $('input:checkbox').not(this).prop('checked', this.checked);
     });
    $('#yuranpendaftaran-tahap_pelajar').change(function(){
        var id = $(this).val();
        $('#customCheckThis').prop('checked',false);
        $.ajax({
                type: 'POST',
                url: 'listyuran',
                data: {id: id},
                success: function(data) {
                    $('#yurantable > tbody').html(data);
               }
            });
    });
    
    $(".calc").click(function(event) {
        $('.proceed').show();
        $('#btnsbmit').prop('disabled',false);
        $('#yuranpendaftaran-jumlah_bayaran').prop('disabled',false);
        $('#yuranpendaftaran-nama_penerima').prop('disabled',false);
        $('#yuranpendaftaran-yuran_tertunggak_daftar').val(0);
        $('#yuranpendaftaran-jumlah_bayaran').val('');

        var total = 0;
        $(".test:checked").each(function() {
            total += parseInt($(this).data("value")); //$(this).data("id")
        });
        
        if (total == 0) {
            $('#yuranpendaftaran-jumlah_yuran').val(0);
        } else {                
            $('#yuranpendaftaran-jumlah_yuran').val(total.toFixed(2));
        }
    });

    $(".addyuran").submit(function() {
        total = 0;
        $(".test:checked").each(function() {
            total += parseInt($(this).data("value")); //$(this).data("id")
        });

        if($('#yuranpendaftaran-jumlah_yuran').val() == 0){
            $('#ralatyuran').show();
            return false;
        }

        if($('#yuranpendaftaran-jumlah_bayaran').val() != 0 || $('#yuranpendaftaran-jumlah_bayaran').val() != ''){
            return true;
        }
        else{
            $('#btnsbmit').prop('disabled', true);
            $('#ralatyuran').show();
            return false;
        }


    });
    $('#yuranpendaftaran-jumlah_bayaran').keyup(function(){
        var v = $('#yuranpendaftaran-jumlah_yuran').val();
        var byr = v - $(this).val();
        $('#yuranpendaftaran-yuran_tertunggak_daftar').val(byr.toFixed(2));
        $('#btnsbmit').prop('disabled', false);
    });

    $('.select2-single').select2({
        placeholder:'Sila Pilih',
        ajax: {
            url: 'stdnt',
            dataType: 'json',
            delay:250
        }
    });

    $("#yuranpendaftaran-jumlah_bayaran").bind("mousewheel", function() {
        return false;
    });
});
JS;
$this->registerJs($script);
?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div id="test">
    
</div>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali ke Pendaftaran Pelajar',['index'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('saveerror')) { ?>
            <div class="alert alert-danger" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('saveerror'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php $form = ActiveForm::begin([
                'options'=>['class'=>'addyuran'],
                
            ]); ?>
                <div class="box-body">
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Nama Pelajar <span class="font-red">**</span></label>
                                <input type="text" disabled="" value="<?= $model4->nama_pelajar ?>" class="form-control" name="">
                                <input type="hidden" id="yuranpendaftaran-idstd" value="<?= $model4->id ?>" class="form-control" name="">
                                <!-- <select id="yuranpendaftaran-idstd" class="select2-single" name="YuranPendaftaran[idstd]"></select> -->
                            </div>
                            <div class="form-group col-md-6">
                                <label>Tarikh Bayaran <span class="font-red">**</span></label>
                                <?= $form->field($model, 'tarikh_bayaran')->textInput(['maxlength' => true,'class'=>'form-control datepicker','data-date-format'=>'yyyy-mm-dd','autocomplete'=>'off'])->label(false) ?>
                            </div>
                        </div>
                        <div class="form-row" style="display: none">
                            <div class="form-group col-md-6">
                                <label>Guru Kelas <span class="font-red">**</span></label>
                                <?= $form->field($model, 'guru_kelas')->dropDownList($namastaf,
                                    [
                                        'prompt'=>'--Sila Pilih Guru Kelas--',
                                    ]
                                )->label(false); ?>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Guru Kanan <span class="font-red">**</span></label>
                                <?= $form->field($model, 'guru_kanan')->dropDownList($gurukanan,
                                    [
                                        'prompt'=>'--Sila Pilih Guru Kanan--',
                                    ]
                                )->label(false); ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Jenis Pelajar <span class="font-red">**</span></label>
                                <?= $form->field($model,'jenis_pelajar')->dropDownList([
                                    '1'=>'Pelajar Baru',
                                    '2'=>'Pelajar Lama',
                                    '3'=>'Pelajar Lewat',
                                ],
                                [
                                    'prompt'=>'--Sila Pilih Jenis Pelajar--',
                                ]
                                )->label(false) ?>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Tahap Pelajar <span class="font-red">**</span></label>
                                <?= $form->field($model, 'tahap_pelajar')->dropDownList([
                                    '1'=>'1',
                                    '2'=>'2',
                                    '3'=>'3',
                                    '4'=>'4',
                                    '5'=>'5',
                                    '6'=>'6',
                                ], ['prompt' => '--Sila Pilih--'])->label(false) ?>
                            </div>
                        </div>
                        <div class="form-row" style="display: none" id="infoyuran">
                            <div class="form-group col-md-12">
                                <label><i><b>Sila tandakan jenis bayaran. Anda boleh menukar maklumat ini dengan memilih di ruangan tahap pelajar.</b></i></label>
                                <table class="table table-stripe" id="yurantable">
                                    <thead>
                                        <tr>
                                            <th width="10">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheckThis">
                                                    <label class="custom-control-label" for="customCheckThis"></label>
                                                </div>
                                            </th>
                                            <th>Jenis Bayaran</th>
                                            <th>Jumlah (RM)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <span class="calc btn btn-primary mb-4">Lakukan Pengiraan</span>
                        </div>
                        <div class="proceed" style="display: none;">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>Jumlah perlu dibayar</label>
                                    <?= $form->field($model, 'jumlah_yuran')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off','readonly'=>'','type'=>'number'])->label(false)?>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Jumlah diterima <span class="font-red">**</span></label>
                                    <?= $form->field($model, 'jumlah_bayaran')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off','type'=>'number','disabled'=>''])->label(false)?>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Baki Tertunggak</label>
                                    <?= $form->field($model, 'yuran_tertunggak_daftar')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off','readonly'=>'','type'=>'number'])->label(false)?>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>Nama Penerima <span class="font-red">**</span></label>
                                    <?= $form->field($model, 'nama_penerima')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off','disabled'=>''])->label(false)?>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Jenis Bayaran</label>
                                    <?= $form->field($model,'jenis_bayaran')->dropDownList([
                                        'Tunai'=>'Tunai',
                                        'Cek'=>'Cek',
                                        'Pindahan Bank'=>'Pindahan Bank',
                                    ],
                                    [
                                        'prompt'=>'--Sila Pilih Jenis Bayaran--',
                                    ]
                                    )->label(false) ?>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>No. Rujukan Pindahan Wang</label>
                                    <?= $form->field($model, 'no_rujukan_pindahan_wang')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false)?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Nota</label>
                                <?= $form->field($model, 'nota')->textArea(['rows'=>6])->label(false)?>

                            </div>
                        </div>
                    </form>
                    <div class="box-footer">
                        <div class="col-md-12">
                            <div class="row" style="display: none;" id="ralatyuran">
                                <div class="col-12">
                                    <div class="alert alert-danger" role="alert">
                                        Ralat ! Anda diminta lakukan pengiraan yuran semula.
                                    </div>
                                </div>
                            </div>
                            <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>
                            
                            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success btn-block mb-1','id'=>'btnsbmit','disabled'=>'']) ?>
                            <?= Html::a('Batal',['index'],['class'=>'btn btn-secondary btn-block mb-1']) ?>
                        </div>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
