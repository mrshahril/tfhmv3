<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\kewangan\models\YuranPendaftaran */

$this->title = 'Update Yuran Pendaftaran: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Yuran Pendaftarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="yuran-pendaftaran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
