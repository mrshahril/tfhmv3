<?php

namespace app\modules\kewangan\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

use app\modules\hr\pelajar\models\MaklumatPelajarPenjaga;
use app\modules\kewangan\models\YuranPendaftaran;
use app\modules\kewangan\models\YuranPendaftaranDetail;
use app\modules\kewangan\models\YuranPaymentDetails;
use app\modules\kewangan\models\YuranPendaftaranSearch;
use app\modules\oldyuran\models\OldYuranDaftar;

use app\models\LookupYurandaftar;

/**
 * YuranPendaftaranController implements the CRUD actions for YuranPendaftaran model.
 */
class YuranPendaftaranController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all YuranPendaftaran models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new YuranPendaftaranSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['tahun'=>date('Y')]);
        $dataProvider->query->andWhere(['pusat_pengajian'=>Yii::$app->user->identity->tahfiz]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single YuranPendaftaran model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new YuranPendaftaran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new YuranPendaftaran();
        $model2 = new YuranPendaftaranDetail();

        $model->scenario = 'addbill';

        if ($model->load(Yii::$app->request->post()) ) {
            $model->idstd = $_POST['YuranPendaftaran']['idstd'];
            $model->created_at = date('Y-m-d h:i:s A');
            $model->enter_by = Yii::$app->user->identity->id;
            $model->tahun = date('Y');
            $model->pusat_pengajian = $model->namastd->pusat_pengajian_id;
            $model->status_yuran = 0;
            
            $getInv = YuranPendaftaran::find()->where(['not', ['invoice_no' => null]])->orderBy(['id' => SORT_DESC])->limit(1)->one();

            if (empty($getInv['invoice_no'])) {
                $runninNo = 10000;
                $noinv = 'INV#'.$model->namatahfiz->code_resit."-".$runninNo;
                //strlen
            } else {
                $len = strlen('INV#'.$model->namatahfiz->code_resit) + 1; // count string length and plus 1 
                $qt = substr($getInv['invoice_no'], $len); //remove code tahfiz n dash (-)
                $new = $qt + 1; // plus 1 for new no of receipt
                $runninNo = $new;

                $noinv = 'INV#'.$model->namatahfiz->code_resit."-".$runninNo;
            }

            $model->invoice_no = $noinv;

            if ($model->save()) {
                $yuran = LookupYurandaftar::find()
                    ->where(['tahap_pelajar'=>$model->tahap_pelajar])
                    ->andWhere(['jenis_pelajar'=>$model->jenis_pelajar])
                    ->all();
                foreach ($yuran as $key => $value) {
                    $model3 = new YuranPendaftaranDetail;

                    $model3->item_daftar = $value->id;
                    $model3->price = $value->jumlah;
                    $model3->daftarid = $model->id;
                    $model3->save();
                }

                //check yuran tertunggak tahun sebelum
                $oldyuran = OldYuranDaftar::find()
                            ->where(['id_pelajar'=>$model->idstd])
                            ->andWhere(['!=','yuran_tertunggak',0.00])
                            ->orderBy(['id'=>SORT_DESC])
                            ->one();

                if (isset($oldyuran)) {
                    $yuran_tertunggak = new YuranPendaftaranDetail();
                    $yuran_tertunggak->item_daftar = 27;
                    $yuran_tertunggak->price = $oldyuran->yuran_tertunggak;
                    $yuran_tertunggak->daftarid = $model->id;
                    $yuran_tertunggak->save();
                }

                Yii::$app->session->setFlash('savedone','Invois yuran pendaftaran telah berjaya dikemaskini');
                return $this->redirect(['payyuran', 'id' => base64_encode($model->id)]);   
            }   
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    public function actionRegisterday($refer,$id)
    {
        $model = new YuranPendaftaran();
        $model2 = new YuranPendaftaranDetail();
        $model3 = new YuranPaymentDetails();
        $model4 = MaklumatPelajarPenjaga::findOne(base64_decode($id));

        $getstd = MaklumatPelajarPenjaga::stdntinfo(base64_decode($id));

        $model->scenario = 'registerday';
        if ($model->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post())) {
            
        }
        else{
            return $this->render('registerday',[
                'model'=>$model,
                'model3'=>$model3,
                'model4'=>$model4,
            ]);
        }
    }

    public function actionPayyuran($id)
    {
        $model = YuranPendaftaran::findOne(base64_decode($id));
        
        return $this->render('daftaryuran/payyuran',[
           'model'=>$model,
        ]);
    }

    public function actionEdityuran($id)
    {
        $model = YuranPendaftaran::findOne(base64_decode($id));
        $model->scenario = 'edityuran';

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = date('Y-m-d h:i:s A');
            $model->update_by = Yii::$app->user->identity->id;

            if ($model->save()) {
                $model2 = LookupYurandaftar::find()
                    ->where(['tahap_pelajar'=>$model->tahap_pelajar])
                    ->andWhere(['jenis_pelajar'=>$model->jenis_pelajar])
                    ->all();

                YuranPendaftaranDetail::deleteAll('daftarid = :daftarid', [':daftarid' => $model->id]);
                foreach ($model2 as $key => $value) {
                    $model3 = new YuranPendaftaranDetail;
                    $model3->item_daftar = $value->id;
                    $model3->price = $value->jumlah;
                    $model3->daftarid = $model->id;

                    $model3->save();
                }

                //check yuran tertunggak tahun sebelum
                $oldyuran = OldYuranDaftar::find()
                            ->where(['id_pelajar'=>$model->idstd])
                            ->andWhere(['!=','yuran_tertunggak',0.00])
                            ->orderBy(['id'=>SORT_DESC])
                            ->one();
                            
                if (isset($oldyuran)) {
                    $yuran_tertunggak = new YuranPendaftaranDetail();
                    $yuran_tertunggak->item_daftar = 27;
                    $yuran_tertunggak->price = $oldyuran->yuran_tertunggak;
                    $yuran_tertunggak->daftarid = $model->id;
                    $yuran_tertunggak->save();
                }

                Yii::$app->session->setFlash('savedone','Invois yuran pendaftaran telah berjaya dikemaskini');
                return $this->redirect(['payyuran','id'=>$id]);
            }
        }
        else{
            return $this->renderAjax('daftaryuran/edityuran',[
                'model'=>$model,
            ]);
        }
    }

    public function actionPadamitemyuran($id)
    {
        $model = YuranPendaftaranDetail::findOne(base64_decode($id));
        $daftarid = $model->daftarid;

        if ($model->delete()) {
            $model2 = YuranPendaftaran::findOne($daftarid);
            if (isset($model2)) {
                Yii::$app->session->setFlash('savedone','Item yuran pendaftaran telah berjaya dihapus');

                return $this->redirect(['payyuran','id'=>base64_encode($model2->id)]);
            }
            else{

            }
        }
    }

    public function actionJanadaftar($id)
    {
        $model = YuranPendaftaran::findOne(base64_decode($id));

        if (isset($model)) {
            $model->status_yuran = 1;
            $model->updated_at = date('Y-m-d h:i:s A');
            $model->update_by = Yii::$app->user->identity->id;

            $sum = 0;

            foreach ($model->details as $key => $value) {
                $sum += $value->price;    
            }
            $model->jumlah_yuran = $sum;

            if ($model->save()) {
                if (Yii::$app->request->get('refer') == 'jana') {
                    Yii::$app->session->setFlash('savedone','Invois yuran pendaftaran telah berjaya dijana');
                    return $this->redirect(['payyuran','id'=>$id]);
                }
                else{
                    return $this->redirect(['paymentprocess','id'=>$id]);
                }
            }
        }
    }

    public function actionPaymentprocess($id)
    {
        $model = YuranPendaftaran::findOne(base64_decode($id));
        $model3 = $model->getDetails()->where(['status_pay'=>0])->all();

        $model2 = new YuranPaymentDetails();

        $model2->scenario = 'paymentprocess';
        if ($model->load(Yii::$app->request->post()) && $model2->load(Yii::$app->request->post())) {

            $model2->created_at = date('Y-m-d h:i:s A');
            $model2->enter_by = Yii::$app->user->identity->id;
            $model2->daftarid = $model->id;
            $model2->jenis_yuran = 'Yuran Daftar';

            $getInv = YuranPaymentDetails::find()->where(['not', ['no_resit' => null]])->orderBy(['id' => SORT_DESC])->limit(1)->one();

            if (empty($getInv['no_resit'])) {
                $runninNo = 10000;
                $noresit = 'REC#'.$model->namatahfiz->code_resit."-".$runninNo;
                //strlen
            } else {
                $len = strlen('REC#'.$model->namatahfiz->code_resit) + 1; // count string length and plus 1 
                $qt = substr($getInv['no_resit'], $len); //remove code tahfiz n dash (-)
                $new = $qt + 1; // plus 1 for new no of receipt
                $runninNo = $new;

                $noresit = 'REC#'.$model->namatahfiz->code_resit."-".$runninNo;
            }

            $model2->no_resit = $noresit;

            if ($model2->save()) {
                $totalbyran = YuranPaymentDetails::find()->where(['daftarid'=>$model->id])->sum('jumlah_bayaran');
                $model->jumlah_bayaran = $totalbyran;
                $model->yuran_tertunggak_daftar = $model->bakitunggak;
                $totalyuran = $model->jumlah_yuran + $model->pelarasan;

                if ($model->jumlah_bayaran != $totalyuran) {
                    $model->status_yuran = 2;
                }
                else{
                    $model->status_yuran = 3;
                }
                
                if ($model->save()) {
                    $kira = count($_POST['selectitemyuran']);

                    for ($i=0; $i < $kira; $i++) { 
                        $dftrdetail = YuranPendaftaranDetail::findOne($_POST['selectitemyuran'][$i]);
                        $dftrdetail->status_pay = 1; //telah bayar
                        $dftrdetail->payment_id = $model2->id;
                        $dftrdetail->save();
                    }

                }
                Yii::$app->session->setFlash('savedone','Proses pembayaran telah selesai.');
                return $this->redirect(['payyuran','id'=>base64_encode($model->id)]);
            }
        }
        else{
            return $this->render('daftaryuran/paymentprocess',[
                'model'=>$model,
                'model2'=>$model2,
                'model3'=>$model3,
            ]);
        }
    }

    public function actionPelarasan($id)
    {
        $model = YuranPendaftaran::findOne(base64_decode($id));

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = date('Y-m-d h:i:s A');
            $model->update_by = Yii::$app->user->identity->id;

            if ($model->save()) {
                $pelarasan = new YuranPendaftaranDetail();
                $pelarasan->item_daftar = 25;
                $pelarasan->price = $model->pelarasan;
                $pelarasan->daftarid = $model->id;

                $pelarasan->save();
                Yii::$app->session->setFlash('savedone','Pelarasan yuran telah berjaya disimpan.');
                return $this->redirect(['payyuran','id'=>base64_encode($model->id)]);

            }
        }
        else{
            return $this->render('daftaryuran/pelarasan',[
                'model'=>$model,
            ]);
        }
    }

    public function actionPrintresit($id)
    {
        $model = YuranPaymentDetails::findOne(base64_decode($id));

        $content = $this->renderPartial('daftaryuran/printresit',[
            'model'=>$model,
        ]);

        $pdf = new Pdf([
            'mode'=>Pdf::MODE_UTF8,
            'content'=>$content,
            'filename'=>'resit_yuran_'.$model->no_resit.'.pdf',
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
            'cssInline' => '.text{font-family:Courier;font-size:12px;}.txttable{font-size:12px;font-family:Courier;}', 
            'options' => ['title' => 'Yuran Pendaftaran | Resit pembayaran'],
            'methods' => [
                'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }

    public function actionPrintinv($id)
    {
        $model = YuranPendaftaran::findOne(base64_decode($id));

        $content = $this->renderPartial('daftaryuran/printinv', [
                'model'=>$model,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'content' => $content,
            'filename' => 'yuran_daftar_'.$model->namastd->nama_pelajar.'.pdf',
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
            'cssInline' => '.text{font-family:Courier;font-size:12px;}.txttable{font-size:12px;font-family:Courier;}', 
            'options' => ['title' => 'Yuran Pendaftaran | Inbois'],
            'methods' => [
                'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }
    /**
     * Updates an existing YuranPendaftaran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing YuranPendaftaran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the YuranPendaftaran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return YuranPendaftaran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = YuranPendaftaran::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionStdnt($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
        $connection = \Yii::$app->db;
        $sql = $connection->createCommand('SELECT id,nama_pelajar AS text FROM maklumat_pelajar_penjaga WHERE no_mykid LIKE "%'.$q.'%" OR no_surat_beranak LIKE "%'.$q.'%" OR nama_pelajar LIKE "%'.$q.'%" AND alumni = 0 AND pusat_pengajian_id='.Yii::$app->user->identity->tahfiz);

       // $query = "SELECT * FROM `post` where `title` LIKE 'foo%' ";
        $model = $sql->queryAll();
        $out['results'] = array_values($model);

        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => MaklumatPelajarPenjaga::find($id)->nama_pelajar];
        }
        return $out;
    }

    public function actionStdinfo($id)
    {
        $model = MaklumatPelajarPenjaga::find()->where(['id'=>$id])->one();
        
        $a = [
            'id'=>$model->id,
            'tahap_semasa'=>$model->tahap_semasa,
            'guru_kelas'=>$model->id_staf,
            'guru_kelas_nama'=>$model->id_staf ? $model->namaguru->nama : '',
        ];
        echo Json::encode($a);
        
    }

    public function actionListyuran()
    {
        $id = $_POST['id'];
        $idstd = $_POST['idstd'];
        $jenispelajar = $_POST['jenispelajar'];
        $model = LookupYurandaftar::find()
            ->where(['tahap_pelajar'=>$id])
            ->andWhere(['jenis_pelajar'=>$jenispelajar])
            ->all();

        $checktertunggak = OldYuranDaftar::find()
                        ->where(['id_pelajar'=>$idstd])
                        ->andWhere(['!=','yuran_tertunggak',0.00])
                        ->orderBy(['id'=>SORT_DESC])
                        ->one();

        $html = '';
        $total = 0;
        foreach ($model as $key => $value) {
            $html .= '<tr>';
                $html .= "<td>".$value->jenis_bayaran."</td>";
                $html .= "<td>".number_format($value->jumlah,2)."</td>";
            $html .= '</tr>';
            $total = $total + $value->jumlah;
        }

        if (isset($checktertunggak)) {
            $yuran_refer = LookupYurandaftar::find()->where(['id'=>27])->one();

            $html .= '<tr>';
                $html .= "<td>".$yuran_refer->jenis_bayaran."</td>";
                $html .= "<td>".number_format($checktertunggak->yuran_tertunggak,2)."</td>";
            $html .= '</tr>';
        }
        // $html .= '<tr><td colspan="2" align="right"><b>Jumlah Yuran</b></td><td>'.number_format($total,2).'</td></tr>';
        echo $html;
    }

    public function actionFilteryuran($id)
    {
        $model = LookupYurandaftar::find()
        ->where(['jenis_pelajar' => $id])
        ->count();

        $model2 = LookupYurandaftar::find()
        ->select('tahap_pelajar')
        ->where(['jenis_pelajar' => $id])
        ->distinct()
        ->all();

        if($model2>0){
            echo "<option value=''>-Sila Pilih-</option>";
            foreach($model2 as $jenis){
                echo "<option value='".$jenis->tahap_pelajar."'>".$jenis->tahap_pelajar."</option>";
            }
        } else {
                echo "<option></option>";
        }
    }
}
