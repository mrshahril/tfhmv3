<?php

namespace app\modules\report\pelajar;

/**
 * pelajar module definition class
 */
class Pelajar extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\report\pelajar\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
