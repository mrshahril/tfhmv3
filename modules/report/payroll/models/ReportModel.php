<?php

namespace app\modules\report\payroll\models;

use Yii;
use yii\base\Model;

use app\modules\payroll\models\PaySlip;

/**
 * ContactForm is the model behind the contact form.
 */
class ReportModel extends Model
{
    public static function title($title)
    {
        if ($title == 'gajibersih') {
            return 'Laporan Gaji Bersih';
        }
        elseif ($title == 'epf') {
            return 'Laporan KWSP';
        }
        elseif ($title == 'socso') {
            return 'Laporan SOCSO';
        }
        elseif ($title == 'sip') {
            return 'Laporan SIP';
        }
        elseif ($title == 'tbgkbj') {
            return 'Laporan Tabung Kebajikan';
        }
        elseif ($title == 'tbgguru') {
            return 'Laporan Tabung Guru';
        }
        elseif ($title == 'sewa') {
            return 'Laporan Sewa Rumah';
        }
        elseif ($title == 'ctg') {
            return 'Laporan CTG';
        }
        elseif ($title == 'kksk') {
            return 'Laporan KKSK';
        }
        elseif ($title == 'tbghaji') {
            return 'Laporan Tabung Haji';
        }
        elseif ($title == 'bank') {
            return 'Laporan Bank';
        }
    }

    public static function getTotalgajikasar($provider,$gaji_asas,$elaun_asas,$elaun_rumah)
    {
        $total = 0;
        foreach ($provider as $value) {
            $gaji_kasar = $value->gaji_asas + $value->elaun_asas + $value->elaun_rumah;
            $total += $gaji_kasar;
        }
        return '<b>'.number_format($total,2).'</b>';
    }

    public static function getGajikasar($gaji_asas,$elaun_asas,$elaun_rumah)
    {
        $gajikasar = $gaji_asas + $elaun_asas + $elaun_rumah;

        return $gajikasar; 
    }

    public static function getPelarasan($hibah,$bonus,$lain_tambahan,$tabung_haji,$tabung_guru,$sewa_rumah,$kksk,$gajitahan,$lain,$loan)
    {
        $pelarasan =  ($hibah + $bonus + $lain_tambahan) - ($tabung_haji + $tabung_guru + $sewa_rumah + $kksk + $gajitahan + $lain + $loan);

        return $pelarasan;

    }
}
