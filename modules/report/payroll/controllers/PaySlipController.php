<?php

namespace app\modules\report\payroll\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use PHPExcel;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Worksheet_PageSetup;
use PHPExcel_Style_Fill;
use PHPExcel_IOFactory;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;

use app\modules\payroll\models\PaySlip;
use app\modules\payroll\models\PaySlipSearch;

class PaySlipController extends Controller
{
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex($type)
    {
    	$type = base64_decode($type);

    	return $this->render('index',[
    		'type'=>$type,
    	]);
    }

    public function actionLaporan_gajibersih($tahun,$bulan,$type)
    {
    	$searchModel = new PaySlipSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan]);
        $amount = 0;
        if (isset($_GET['PaySlipSearch']['globalslip'])) {
	        foreach ($dataProvider->getModels() as $key => $val) {
	        	$gaji_kasar = $val->gaji_asas + $val->elaun_asas + $val->elaun_rumah;
	            $total = $gaji_kasar;
	            $amount += $total;
	        }

	    }
	    else{
	    	$gaji_kasar = PaySlip::find()->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])->all();

	    	foreach ($gaji_kasar as $key => $val) {
	    		$total = $val->gaji_asas + $val->elaun_asas + $val->elaun_rumah;
	    		$amount += $total;
	    	}
	    }

        return $this->render('laporan_gajibersih',[
        	'searchModel'=>$searchModel,
        	'dataProvider'=>$dataProvider,
        	'tahun'=>$tahun,
        	'bulan'=>$bulan,
        	'type'=>$type,
        	'amount'=>number_format($amount,2),
        ]);
    }

    public function actionCetak_gajibersih($tahun,$bulan,$to)
    {
    	$tarikhmasa = date('F', mktime(0,0,0,$bulan, 1)).'_'.$tahun;

    	$model = PaySlip::find()
    			->joinWith('namestaf')
    			->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])
    			->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC])
    			->all();
    	if ($to == 'pdf') {
    		$content = $this->renderPartial('cetak_gajibersih', [
    			'model'=>$model,
    		]);

    		$pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'content' => $content,
                'filename' => 'laporan_gaji_bersih_'.$tarikhmasa.'.pdf',
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
                'cssInline' => '.main{font-size:12px;font-family:courier}.head{font-size:15px;font-family:courier}',
                'options' => ['title' => 'Laporan | Gaji Bersih'],
                'methods' => [
                    'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
    	}
    	elseif($to == 'excel'){
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->setTitle('Laporan Gaji Bersih')
                     ->setCellValue('A1', 'Nama Kakitangan')
                     ->setCellValue('B1', 'Gaji Kasar')
                     ->setCellValue('C1', 'Sewa Rumah')
                     ->setCellValue('D1', 'EPF')
                     ->setCellValue('E1', 'Socso')
                     ->setCellValue('F1', 'SIP')
                     ->setCellValue('G1', 'Tabung Guru')
                     ->setCellValue('H1', 'Tabung Haji')
                     ->setCellValue('I1', 'KKSK')
                     ->setCellValue('J1', 'Bonus')
                     ->setCellValue('K1', 'Loan')
                     ->setCellValue('L1', 'CTG')
                     ->setCellValue('M1', 'Lain-Lain')
                     ->setCellValue('N1', 'Gaji Bersih');

            $row=2;
            foreach ($model as $key => $value) {
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$value['namestaf']['nama']);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$value->gajikasar);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$row,round(0-$value->sewa_rumah,2));
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$row,round(0-$value->kwsp,2));
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$row,round(0-$value->socso,2));
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$row,round(0-$value->sip,2));
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$row,round(0-$value->tabung_guru));
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$row,round(0-$value->tabung_haji,2));
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$row,round(0-$value->kksk));
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$row,$value->bonus);
                $objPHPExcel->getActiveSheet()->setCellValue('K'.$row,round(0-$value->loan,2));
                $objPHPExcel->getActiveSheet()->setCellValue('L'.$row,round(0-$value->ctgaji,2));
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$row,round($value->lainjumlah,2));
                $objPHPExcel->getActiveSheet()->setCellValue('N'.$row,round($value->gajibersih,2));
            	$row++;
            }

            $lastrow = $row - 1;
	        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'JUMLAH KESELURUHAN');
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, '=SUM(B2:B'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, '=SUM(C2:C'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '=SUM(D2:D'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '=SUM(E2:E'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '=SUM(F2:F'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, '=SUM(G2:G'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, '=SUM(H2:H'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, '=SUM(I2:I'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('J'.$row, '=SUM(J2:J'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('K'.$row, '=SUM(K2:K'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('L'.$row, '=SUM(L2:L'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('M'.$row, '=SUM(M2:M'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('N'.$row, '=SUM(N2:N'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
	        $objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

            $objPHPExcel->getActiveSheet()->getStyle("A".$row.":N".$lastrow)->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);

            header('Content-Type: application/vnd.ms-excel');
            $filename = "laporan_gaji_bersih.".$tarikhmasa.".xls";
            header('Content-Disposition: attachment;filename='.$filename .' ');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
    	}
    }

    public function actionLaporan_epf($tahun,$bulan,$type)
    {	
    	$searchModel = new PaySlipSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan]);
        $dataProvider->query->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC]);

        $amount = 0;
        $olehpekerja = 0;
        $olehmajikan = 0;
        $grandtotal = 0;
        if (isset($_GET['PaySlipSearch']['globalslip'])) {
	        foreach ($dataProvider->getModels() as $key => $val) {
	        	$gaji_kasar = $val->gaji_asas + $val->elaun_asas + $val->elaun_rumah;
	            $total = $gaji_kasar;
	    		$olehpekerja = $olehpekerja + $val->kwsp;
	            $olehmajikan = $olehmajikan + $val->kwsp_majikan;
	            $grandtotal = $grandtotal + ($val->kwsp + $val->kwsp_majikan);
	            $amount += $total;
	        }

	    }
	    else{
	    	$gaji_kasar = PaySlip::find()->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])->all();

	    	foreach ($gaji_kasar as $key => $val) {
	    		$total = $val->gaji_asas + $val->elaun_asas + $val->elaun_rumah;
	    		$olehpekerja = $olehpekerja + $val->kwsp;
	            $olehmajikan = $olehmajikan + $val->kwsp_majikan;
	            $grandtotal = $grandtotal + ($val->kwsp + $val->kwsp_majikan);

	    		$amount += $total;
	    	}
	    }

        return $this->render('laporan_epf',[
        	'searchModel'=>$searchModel,
        	'dataProvider'=>$dataProvider,
        	'tahun'=>$tahun,
        	'bulan'=>$bulan,
        	'type'=>$type,
        	'amount'=>number_format($amount,2),
        	'olehpekerja'=>number_format($olehpekerja,2),
        	'olehmajikan'=>number_format($olehmajikan,2),
        	'grandtotal'=>number_format($grandtotal,2),
        ]);
    }

    public function actionCetak_epf($tahun,$bulan,$to)
    {
    	$tarikhmasa = date('F', mktime(0,0,0,$bulan, 1)).'_'.$tahun;

    	$model = PaySlip::find()
    			->joinWith('namestaf')
    			->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])
    			->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC])
    			->all();
    	if ($to == 'pdf') {
    		$content = $this->renderPartial('cetak_epf', [
    			'model'=>$model,
    		]);

    		$pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'content' => $content,
                'filename' => 'laporan_kwsp_'.$tarikhmasa.'.pdf',
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
                'cssInline' => '.main{font-size:12px;font-family:courier}.head{font-size:15px;font-family:courier}',
                'options' => ['title' => 'Laporan | KWSP'],
                'methods' => [
                    'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
    	}
    	elseif($to == 'excel'){
    		$objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->setTitle('Laporan KWSP')
	            ->setCellValue('A1', 'Nama Kakitangan')
	            ->setCellValue('B1', 'No. Kad Pengenalan')
	            ->setCellValue('C1', 'No. KWSP')
	            ->setCellValue('D1', 'Gaji (RM)')
	            ->setCellValue('E1', 'Oleh Pekerja (RM)')
	            ->setCellValue('F1', 'Oleh Majikan (RM)')
	            ->setCellValue('G1', 'Jumlah (RM)');

	        $row=2;
            foreach ($model as $key => $value) {
            	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$value['namestaf']['nama']);
            	$objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$value['namestaf']['no_kp']);
            	$objPHPExcel->getActiveSheet()->setCellValue('C'.$row,$value['namestaf']['no_kwsp']);
            	$objPHPExcel->getActiveSheet()->setCellValue('D'.$row,round($value->gajikasar,2));
            	$objPHPExcel->getActiveSheet()->setCellValue('E'.$row,round($value->kwsp,2));
            	$objPHPExcel->getActiveSheet()->setCellValue('F'.$row,round($value->kwsp_majikan,2));

            	$jumlah = $value->kwsp + $value->kwsp_majikan;
            	$objPHPExcel->getActiveSheet()->setCellValue('G'.$row,round($jumlah,2));
            	$row++;
            }

            $lastrow = $row - 1;
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'JUMLAH KESELURUHAN');
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '=SUM(D2:D'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '=SUM(E2:E'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '=SUM(F2:F'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, '=SUM(G2:G'.$lastrow.')');

	        $objPHPExcel->getActiveSheet()->getStyle('D'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
	        $objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
	        $objPHPExcel->getActiveSheet()->getStyle('F'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
	        $objPHPExcel->getActiveSheet()->getStyle('G'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

            $objPHPExcel->getActiveSheet()->getStyle("A".$row.":G".$lastrow)->getFont()->setBold(true);

	    	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);

            header('Content-Type: application/vnd.ms-excel');
            $filename = "laporan_kwsp.".$tarikhmasa.".xls";
            header('Content-Disposition: attachment;filename='.$filename .' ');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
    	}

    }

    public function actionLaporan_socso($tahun,$bulan,$type)
    {
    	$searchModel = new PaySlipSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan]);
        $dataProvider->query->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC]);

        $amount = 0;
        $olehpekerja = 0;
        $olehmajikan = 0;
        $grandtotal = 0;
        if (isset($_GET['PaySlipSearch']['globalslip'])) {
	        foreach ($dataProvider->getModels() as $key => $val) {
	        	$gaji_kasar = $val->gaji_asas + $val->elaun_asas + $val->elaun_rumah;
	            $total = $gaji_kasar;
	    		$olehpekerja = $olehpekerja + $val->socso;
	            $olehmajikan = $olehmajikan + $val->socso_majikan;
	            $grandtotal = $grandtotal + ($val->socso + $val->socso_majikan);
	            $amount += $total;
	        }

	    }
	    else{
	    	$gaji_kasar = PaySlip::find()->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])->all();

	    	foreach ($gaji_kasar as $key => $val) {
	    		$total = $val->gaji_asas + $val->elaun_asas + $val->elaun_rumah;
	    		$olehpekerja = $olehpekerja + $val->socso;
	            $olehmajikan = $olehmajikan + $val->socso_majikan;
	            $grandtotal = $grandtotal + ($val->socso + $val->socso_majikan);

	    		$amount += $total;
	    	}
	    }

        return $this->render('laporan_socso',[
        	'searchModel'=>$searchModel,
        	'dataProvider'=>$dataProvider,
        	'tahun'=>$tahun,
        	'bulan'=>$bulan,
        	'type'=>$type,
        	'amount'=>number_format($amount,2),
        	'olehpekerja'=>number_format($olehpekerja,2),
        	'olehmajikan'=>number_format($olehmajikan,2),
        	'grandtotal'=>number_format($grandtotal,2),
        ]);
    }

    public function actionCetak_socso($tahun,$bulan,$to)
    {
    	$tarikhmasa = date('F', mktime(0,0,0,$bulan, 1)).'_'.$tahun;

    	$model = PaySlip::find()
    			->joinWith('namestaf')
    			->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])
    			->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC])
    			->all();
    	if ($to == 'pdf') {
    		$content = $this->renderPartial('cetak_socso', [
    			'model'=>$model,
    		]);

    		$pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'content' => $content,
                'filename' => 'laporan_socso_'.$tarikhmasa.'.pdf',
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
                'cssInline' => '.main{font-size:12px;font-family:courier}.head{font-size:15px;font-family:courier}',
                'options' => ['title' => 'Laporan | SOCSO'],
                'methods' => [
                    'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
    	}
    	elseif($to == 'excel'){
    		$objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->setTitle('Laporan SOCSO')
	            ->setCellValue('A1', 'Nama Kakitangan')
	            ->setCellValue('B1', 'No. Kad Pengenalan')
	            ->setCellValue('C1', 'Gaji (RM)')
	            ->setCellValue('D1', 'Oleh Pekerja (RM)')
	            ->setCellValue('E1', 'Oleh Majikan (RM)')
	            ->setCellValue('F1', 'Jumlah (RM)');

	        $row=2;
            foreach ($model as $key => $value) {
            	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$value['namestaf']['nama']);
            	$objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$value['namestaf']['no_kp']);
            	$objPHPExcel->getActiveSheet()->setCellValue('C'.$row,round($value->gajikasar,2));
            	$objPHPExcel->getActiveSheet()->setCellValue('D'.$row,round($value->socso,2));
            	$objPHPExcel->getActiveSheet()->setCellValue('E'.$row,round($value->socso_majikan,2));

            	$jumlah = $value->socso + $value->socso_majikan;
            	$objPHPExcel->getActiveSheet()->setCellValue('F'.$row,round($jumlah,2));
            	$row++;
            }

            $lastrow = $row - 1;
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, 'JUMLAH KESELURUHAN');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, '=SUM(C2:C'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '=SUM(D2:D'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '=SUM(E2:E'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '=SUM(F2:F'.$lastrow.')');

	        $objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
	        $objPHPExcel->getActiveSheet()->getStyle('D'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
	        $objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
	        $objPHPExcel->getActiveSheet()->getStyle('F'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

            $objPHPExcel->getActiveSheet()->getStyle("A".$row.":F".$lastrow)->getFont()->setBold(true);

	    	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);

            header('Content-Type: application/vnd.ms-excel');
            $filename = "laporan_socso.".$tarikhmasa.".xls";
            header('Content-Disposition: attachment;filename='.$filename .' ');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
    	}

    }

    public function actionLaporan_sip($tahun,$bulan,$type)
    {
    	$searchModel = new PaySlipSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan]);
        $dataProvider->query->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC]);

        $amount = 0;
        $olehpekerja = 0;
        $olehmajikan = 0;
        $grandtotal = 0;
        if (isset($_GET['PaySlipSearch']['globalslip'])) {
	        foreach ($dataProvider->getModels() as $key => $val) {
	        	$gaji_kasar = $val->gaji_asas + $val->elaun_asas + $val->elaun_rumah;
	            $total = $gaji_kasar;
	    		$olehpekerja = $olehpekerja + $val->sip;
	            $olehmajikan = $olehmajikan + $val->sip_majikan;
	            $grandtotal = $grandtotal + ($val->sip + $val->sip_majikan);
	            $amount += $total;
	        }

	    }
	    else{
	    	$gaji_kasar = PaySlip::find()->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])->all();

	    	foreach ($gaji_kasar as $key => $val) {
	    		$total = $val->gaji_asas + $val->elaun_asas + $val->elaun_rumah;
	    		$olehpekerja = $olehpekerja + $val->sip;
	            $olehmajikan = $olehmajikan + $val->sip_majikan;
	            $grandtotal = $grandtotal + ($val->sip + $val->sip_majikan);

	    		$amount += $total;
	    	}
	    }

        return $this->render('laporan_sip',[
        	'searchModel'=>$searchModel,
        	'dataProvider'=>$dataProvider,
        	'tahun'=>$tahun,
        	'bulan'=>$bulan,
        	'type'=>$type,
        	'amount'=>number_format($amount,2),
        	'olehpekerja'=>number_format($olehpekerja,2),
        	'olehmajikan'=>number_format($olehmajikan,2),
        	'grandtotal'=>number_format($grandtotal,2),
        ]);
    }

    public function actionCetak_sip($tahun,$bulan,$to)
    {
    	$tarikhmasa = date('F', mktime(0,0,0,$bulan, 1)).'_'.$tahun;

    	$model = PaySlip::find()
    			->joinWith('namestaf')
    			->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])
    			->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC])
    			->all();
    	if ($to == 'pdf') {
    		$content = $this->renderPartial('cetak_sip', [
    			'model'=>$model,
    		]);

    		$pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'content' => $content,
                'filename' => 'laporan_sip_'.$tarikhmasa.'.pdf',
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
                'cssInline' => '.main{font-size:12px;font-family:courier}.head{font-size:15px;font-family:courier}',
                'options' => ['title' => 'Laporan | SIP'],
                'methods' => [
                    'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
    	}
    	elseif($to == 'excel'){
    		$objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->setTitle('Laporan SIP')
	            ->setCellValue('A1', 'Nama Kakitangan')
	            ->setCellValue('B1', 'No. Kad Pengenalan')
	            ->setCellValue('C1', 'Gaji (RM)')
	            ->setCellValue('D1', 'Oleh Pekerja (RM)')
	            ->setCellValue('E1', 'Oleh Majikan (RM)')
	            ->setCellValue('F1', 'Jumlah (RM)');

	        $row=2;
            foreach ($model as $key => $value) {
            	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$value['namestaf']['nama']);
            	$objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$value['namestaf']['no_kp']);
            	$objPHPExcel->getActiveSheet()->setCellValue('C'.$row,round($value->gajikasar,2));
            	$objPHPExcel->getActiveSheet()->setCellValue('D'.$row,round($value->sip,2));
            	$objPHPExcel->getActiveSheet()->setCellValue('E'.$row,round($value->sip_majikan,2));

            	$jumlah = $value->sip + $value->sip_majikan;
            	$objPHPExcel->getActiveSheet()->setCellValue('F'.$row,round($jumlah,2));
            	$row++;
            }

            $lastrow = $row - 1;
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, 'JUMLAH KESELURUHAN');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, '=SUM(C2:C'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '=SUM(D2:D'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, '=SUM(E2:E'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, '=SUM(F2:F'.$lastrow.')');

	        $objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
	        $objPHPExcel->getActiveSheet()->getStyle('D'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
	        $objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
	        $objPHPExcel->getActiveSheet()->getStyle('F'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

            $objPHPExcel->getActiveSheet()->getStyle("A".$row.":F".$lastrow)->getFont()->setBold(true);

	    	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);

            header('Content-Type: application/vnd.ms-excel');
            $filename = "laporan_sip.".$tarikhmasa.".xls";
            header('Content-Disposition: attachment;filename='.$filename .' ');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
    	}

    }

    public function actionLaporan_tbgkbj($tahun,$bulan,$type)
    {
    	$searchModel = new PaySlipSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan]);
        $dataProvider->query->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC]);

        $grandtotal = 0;
        if (isset($_GET['PaySlipSearch']['globalslip'])) {
	        foreach ($dataProvider->getModels() as $key => $val) {
	            $grandtotal = $grandtotal + $val->loan;
	        }

	    }
	    else{
	    	$gaji_kasar = PaySlip::find()->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])->all();

	    	foreach ($gaji_kasar as $key => $val) {
	    		$total = $val->gaji_asas + $val->elaun_asas + $val->elaun_rumah;
	    		$grandtotal = $grandtotal + $val->loan;
	    	}
	    }

        return $this->render('laporan_tbgkbj',[
        	'searchModel'=>$searchModel,
        	'dataProvider'=>$dataProvider,
        	'tahun'=>$tahun,
        	'bulan'=>$bulan,
        	'type'=>$type,
        	'grandtotal'=>number_format($grandtotal,2),
        ]);
    }

    public function actionCetak_tbgkbj($tahun,$bulan,$to)
    {
    	$tarikhmasa = date('F', mktime(0,0,0,$bulan, 1)).'_'.$tahun;

    	$model = PaySlip::find()
    			->joinWith('namestaf')
    			->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])
    			->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC])
    			->all();
    	if ($to == 'pdf') {
    		$content = $this->renderPartial('cetak_tbgkbj', [
    			'model'=>$model,
    		]);

    		$pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'content' => $content,
                'filename' => 'laporan_tabung_kebajikan_'.$tarikhmasa.'.pdf',
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
                'cssInline' => '.main{font-size:12px;font-family:courier}.head{font-size:15px;font-family:courier}',
                'options' => ['title' => 'Laporan | Tabung Kebajikan'],
                'methods' => [
                    'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
    	}
    	elseif($to == 'excel'){
    		$objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->setTitle('Laporan SIP')
	            ->setCellValue('A1', 'Nama Kakitangan')
	            ->setCellValue('B1', 'No. Kad Pengenalan')
	            ->setCellValue('C1', 'Jumlah (RM)');

	        $row=2;
            foreach ($model as $key => $value) {
            	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$value['namestaf']['nama']);
            	$objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$value['namestaf']['no_kp']);
            	$objPHPExcel->getActiveSheet()->setCellValue('C'.$row,round($value->loan,2));
            	$row++;
            }

            $lastrow = $row - 1;
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, 'JUMLAH KESELURUHAN');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, '=SUM(C2:C'.$lastrow.')');

	        $objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

            $objPHPExcel->getActiveSheet()->getStyle("A".$row.":C".$lastrow)->getFont()->setBold(true);

	    	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);

            header('Content-Type: application/vnd.ms-excel');
            $filename = "laporan_tabung_kebajikan_.".$tarikhmasa.".xls";
            header('Content-Disposition: attachment;filename='.$filename .' ');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
    	}
    }

    public function actionLaporan_tbgguru($tahun,$bulan,$type)
    {
    	$searchModel = new PaySlipSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan]);
        $dataProvider->query->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC]);

        $grandtotal = 0;
        if (isset($_GET['PaySlipSearch']['globalslip'])) {
	        foreach ($dataProvider->getModels() as $key => $val) {
	            $grandtotal = $grandtotal + $val->tabung_guru;
	        }

	    }
	    else{
	    	$gaji_kasar = PaySlip::find()->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])->all();

	    	foreach ($gaji_kasar as $key => $val) {
	    		$grandtotal = $grandtotal + $val->tabung_guru;
	    	}
	    }

        return $this->render('laporan_tbgguru',[
        	'searchModel'=>$searchModel,
        	'dataProvider'=>$dataProvider,
        	'tahun'=>$tahun,
        	'bulan'=>$bulan,
        	'type'=>$type,
        	'grandtotal'=>number_format($grandtotal,2),
        ]);
    }

    public function actionCetak_tbgguru($tahun,$bulan,$to)
    {
    	$tarikhmasa = date('F', mktime(0,0,0,$bulan, 1)).'_'.$tahun;

    	$model = PaySlip::find()
    			->joinWith('namestaf')
    			->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])
    			->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC])
    			->all();
    	if ($to == 'pdf') {
    		$content = $this->renderPartial('cetak_tbgguru', [
    			'model'=>$model,
    		]);

    		$pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'content' => $content,
                'filename' => 'laporan_tabung_guru_'.$tarikhmasa.'.pdf',
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
                'cssInline' => '.main{font-size:12px;font-family:courier}.head{font-size:15px;font-family:courier}',
                'options' => ['title' => 'Laporan | Tabung Guru'],
                'methods' => [
                    'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
    	}
    	elseif($to == 'excel'){
    		$objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->setTitle('Laporan SIP')
	            ->setCellValue('A1', 'Nama Kakitangan')
	            ->setCellValue('B1', 'No. Kad Pengenalan')
	            ->setCellValue('C1', 'Jumlah (RM)');

	        $row=2;
            foreach ($model as $key => $value) {
            	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$value['namestaf']['nama']);
            	$objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$value['namestaf']['no_kp']);
            	$objPHPExcel->getActiveSheet()->setCellValue('C'.$row,round($value->tabung_guru,2));
            	$row++;
            }

            $lastrow = $row - 1;
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, 'JUMLAH KESELURUHAN');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, '=SUM(C2:C'.$lastrow.')');

	        $objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

            $objPHPExcel->getActiveSheet()->getStyle("A".$row.":C".$lastrow)->getFont()->setBold(true);

	    	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);

            header('Content-Type: application/vnd.ms-excel');
            $filename = "laporan_tabung_guru_.".$tarikhmasa.".xls";
            header('Content-Disposition: attachment;filename='.$filename .' ');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
    	}
    }

    public function actionLaporan_ctg($tahun,$bulan,$type)
    {
    	$searchModel = new PaySlipSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan]);
        $dataProvider->query->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC]);

        $grandtotal = 0;
        if (isset($_GET['PaySlipSearch']['globalslip'])) {
	        foreach ($dataProvider->getModels() as $key => $val) {
                $total_days = date("t",strtotime($val['tarikhmasa']));

	        	$gaji_kasar = $val['gaji_asas'] + $val['elaun_asas'] + $val['elaun_rumah'];

                $ctg = (($gaji_kasar - $val['gaji_tahan'])/ $total_days) * $val['ctg'];
	            $grandtotal = $grandtotal + $ctg;
	        }

	    }
	    else{
	    	$gaji_kasar = PaySlip::find()->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])->all();

	    	foreach ($gaji_kasar as $key => $val) {
	    		$total_days = date("t",strtotime($val['tarikhmasa']));
	        	
	        	$gaji_kasar = $val['gaji_asas'] + $val['elaun_asas'] + $val['elaun_rumah'];

                $ctg = (($gaji_kasar - $val['gaji_tahan'])/ $total_days) * $val['ctg'];
	            $grandtotal = $grandtotal + $ctg;
	    	}
	    }

        return $this->render('laporan_ctg',[
        	'searchModel'=>$searchModel,
        	'dataProvider'=>$dataProvider,
        	'tahun'=>$tahun,
        	'bulan'=>$bulan,
        	'type'=>$type,
        	'grandtotal'=>number_format($grandtotal,2),
        ]);
    }

    public function actionCetak_ctg($tahun,$bulan,$to)
    {
    	$tarikhmasa = date('F', mktime(0,0,0,$bulan, 1)).'_'.$tahun;

    	$model = PaySlip::find()
    			->joinWith('namestaf')
    			->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])
    			->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC])
    			->all();
    	if ($to == 'pdf') {
    		$content = $this->renderPartial('cetak_ctg', [
    			'model'=>$model,
    		]);

    		$pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'content' => $content,
                'filename' => 'laporan_ctg_'.$tarikhmasa.'.pdf',
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
                'cssInline' => '.main{font-size:12px;font-family:courier}.head{font-size:15px;font-family:courier}',
                'options' => ['title' => 'Laporan | CTG'],
                'methods' => [
                    'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
    	}
    	elseif($to == 'excel'){
    		$objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->setTitle('Laporan CTG')
	            ->setCellValue('A1', 'Nama Kakitangan')
	            ->setCellValue('B1', 'No. Kad Pengenalan')
	            ->setCellValue('C1', 'Jumlah (RM)');

	        $row=2;
            foreach ($model as $key => $value) {
            	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$value['namestaf']['nama']);
            	$objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$value['namestaf']['no_kp']);
            	$objPHPExcel->getActiveSheet()->setCellValue('C'.$row,round($value->ctggaji,2));
            	$row++;
            }

            $lastrow = $row - 1;
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, 'JUMLAH KESELURUHAN');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, '=SUM(C2:C'.$lastrow.')');

	        $objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

            $objPHPExcel->getActiveSheet()->getStyle("A".$row.":C".$lastrow)->getFont()->setBold(true);

	    	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);

            header('Content-Type: application/vnd.ms-excel');
            $filename = "laporan_ctg_.".$tarikhmasa.".xls";
            header('Content-Disposition: attachment;filename='.$filename .' ');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
    	}
    }

    public function actionLaporan_sewa($tahun,$bulan,$type)
    {
    	$searchModel = new PaySlipSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan]);
        $dataProvider->query->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC]);

        $grandtotal = 0;
        if (isset($_GET['PaySlipSearch']['globalslip'])) {
	        foreach ($dataProvider->getModels() as $key => $val) {
	            $grandtotal = $grandtotal + $val->sewa_rumah;
	        }

	    }
	    else{
	    	$gaji_kasar = PaySlip::find()->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])->all();

	    	foreach ($gaji_kasar as $key => $val) {
	            $grandtotal = $grandtotal + $val->sewa_rumah;
	    	}
	    }

        return $this->render('laporan_sewa',[
        	'searchModel'=>$searchModel,
        	'dataProvider'=>$dataProvider,
        	'tahun'=>$tahun,
        	'bulan'=>$bulan,
        	'type'=>$type,
        	'grandtotal'=>number_format($grandtotal,2),
        ]);
    }

    public function actionCetak_sewa($tahun,$bulan,$to)
    {
    	$tarikhmasa = date('F', mktime(0,0,0,$bulan, 1)).'_'.$tahun;

    	$model = PaySlip::find()
    			->joinWith('namestaf')
    			->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])
    			->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC])
    			->all();
    	if ($to == 'pdf') {
    		$content = $this->renderPartial('cetak_sewa', [
    			'model'=>$model,
    		]);

    		$pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'content' => $content,
                'filename' => 'laporan_sewa_'.$tarikhmasa.'.pdf',
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
                'cssInline' => '.main{font-size:12px;font-family:courier}.head{font-size:15px;font-family:courier}',
                'options' => ['title' => 'Laporan | Sewa'],
                'methods' => [
                    'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
    	}
    	elseif($to == 'excel'){
    		$objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->setTitle('Laporan Sewa')
	            ->setCellValue('A1', 'Nama Kakitangan')
	            ->setCellValue('B1', 'No. Kad Pengenalan')
	            ->setCellValue('C1', 'Jumlah (RM)');

	        $row=2;
            foreach ($model as $key => $value) {
            	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$value['namestaf']['nama']);
            	$objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$value['namestaf']['no_kp']);
            	$objPHPExcel->getActiveSheet()->setCellValue('C'.$row,round($value->sewa_rumah,2));
            	$row++;
            }

            $lastrow = $row - 1;
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, 'JUMLAH KESELURUHAN');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, '=SUM(C2:C'.$lastrow.')');

	        $objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

            $objPHPExcel->getActiveSheet()->getStyle("A".$row.":C".$lastrow)->getFont()->setBold(true);

	    	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);

            header('Content-Type: application/vnd.ms-excel');
            $filename = "laporan_sewa_.".$tarikhmasa.".xls";
            header('Content-Disposition: attachment;filename='.$filename .' ');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
    	}
    }

    public function actionLaporan_kksk($tahun,$bulan,$type)
    {
    	$searchModel = new PaySlipSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan]);
        $dataProvider->query->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC]);

        $grandtotal = 0;
        if (isset($_GET['PaySlipSearch']['globalslip'])) {
	        foreach ($dataProvider->getModels() as $key => $val) {
	            $grandtotal = $grandtotal + $val->kksk;
	        }

	    }
	    else{
	    	$gaji_kasar = PaySlip::find()->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])->all();

	    	foreach ($gaji_kasar as $key => $val) {
	            $grandtotal = $grandtotal + $val->kksk;
	    	}
	    }

        return $this->render('laporan_kksk',[
        	'searchModel'=>$searchModel,
        	'dataProvider'=>$dataProvider,
        	'tahun'=>$tahun,
        	'bulan'=>$bulan,
        	'type'=>$type,
        	'grandtotal'=>number_format($grandtotal,2),
        ]);
    }

    public function actionCetak_kksk($tahun,$bulan,$to)
    {
    	$tarikhmasa = date('F', mktime(0,0,0,$bulan, 1)).'_'.$tahun;

    	$model = PaySlip::find()
    			->joinWith('namestaf')
    			->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])
    			->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC])
    			->all();
    	if ($to == 'pdf') {
    		$content = $this->renderPartial('cetak_kksk', [
    			'model'=>$model,
    		]);

    		$pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'content' => $content,
                'filename' => 'laporan_kksk_'.$tarikhmasa.'.pdf',
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
                'cssInline' => '.main{font-size:12px;font-family:courier}.head{font-size:15px;font-family:courier}',
                'options' => ['title' => 'Laporan | KKSK'],
                'methods' => [
                    'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
    	}
    	elseif($to == 'excel'){
    		$objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->setTitle('Laporan KKSK')
	            ->setCellValue('A1', 'Nama Kakitangan')
	            ->setCellValue('B1', 'No. Kad Pengenalan')
	            ->setCellValue('C1', 'Jumlah (RM)');

	        $row=2;
            foreach ($model as $key => $value) {
            	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$value['namestaf']['nama']);
            	$objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$value['namestaf']['no_kp']);
            	$objPHPExcel->getActiveSheet()->setCellValue('C'.$row,round($value->kksk,2));
            	$row++;
            }

            $lastrow = $row - 1;
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, 'JUMLAH KESELURUHAN');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, '=SUM(C2:C'.$lastrow.')');

	        $objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

            $objPHPExcel->getActiveSheet()->getStyle("A".$row.":C".$lastrow)->getFont()->setBold(true);

	    	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);

            header('Content-Type: application/vnd.ms-excel');
            $filename = "laporan_kksk_.".$tarikhmasa.".xls";
            header('Content-Disposition: attachment;filename='.$filename .' ');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
    	}
    }

    public function actionLaporan_tbghaji($tahun,$bulan,$type)
    {
    	$searchModel = new PaySlipSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan]);
        $dataProvider->query->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC]);

        $grandtotal = 0;
        if (isset($_GET['PaySlipSearch']['globalslip'])) {
	        foreach ($dataProvider->getModels() as $key => $val) {
	            $grandtotal = $grandtotal + $val->tabung_haji;
	        }
	    }
	    else{
	    	$gaji_kasar = PaySlip::find()->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])->all();
	    	foreach ($gaji_kasar as $key => $val) {
	            $grandtotal = $grandtotal + $val->tabung_haji;
	    	}
	    }

        return $this->render('laporan_tbghaji',[
        	'searchModel'=>$searchModel,
        	'dataProvider'=>$dataProvider,
        	'tahun'=>$tahun,
        	'bulan'=>$bulan,
        	'type'=>$type,
        	'grandtotal'=>number_format($grandtotal,2),
        ]);
    }

    public function actionCetak_tbghaji($tahun,$bulan,$to)
    {
    	$tarikhmasa = date('F', mktime(0,0,0,$bulan, 1)).'_'.$tahun;

    	$model = PaySlip::find()
    			->joinWith('namestaf')
    			->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])
    			->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC])
    			->all();
    	if ($to == 'pdf') {
    		$content = $this->renderPartial('cetak_tbghaji', [
    			'model'=>$model,
    		]);

    		$pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'content' => $content,
                'filename' => 'laporan_tabung_haji_'.$tarikhmasa.'.pdf',
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
                'cssInline' => '.main{font-size:12px;font-family:courier}.head{font-size:15px;font-family:courier}',
                'options' => ['title' => 'Laporan | Tabung Haji'],
                'methods' => [
                    'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
    	}
    	elseif($to == 'excel'){
    		$objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->setTitle('Laporan Tabung Haji')
	            ->setCellValue('A1', 'Nama Kakitangan')
	            ->setCellValue('B1', 'No. Kad Pengenalan')
	            ->setCellValue('C1', 'No. Akaun')
	            ->setCellValue('D1', 'Jumlah (RM)');

	        $row=2;
            foreach ($model as $key => $value) {
            	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$value['namestaf']['nama']);
            	$objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$value['namestaf']['no_kp']);
            	$objPHPExcel->getActiveSheet()->setCellValue('C'.$row,round($value['namestaf']['acc_tabung_haji'],2));
            	$objPHPExcel->getActiveSheet()->setCellValue('D'.$row,round($value->tabung_haji,2));
            	$row++;
            }

            $lastrow = $row - 1;
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'JUMLAH KESELURUHAN');
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '=SUM(D2:D'.$lastrow.')');
	        $objPHPExcel->getActiveSheet()->getStyle('D'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

            $objPHPExcel->getActiveSheet()->getStyle("A".$row.":D".$lastrow)->getFont()->setBold(true);

	    	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);

            header('Content-Type: application/vnd.ms-excel');
            $filename = "laporan_tabung_haji_.".$tarikhmasa.".xls";
            header('Content-Disposition: attachment;filename='.$filename .' ');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
    	}
    }

    public function actionLaporan_bank($tahun,$bulan,$type)
    {
    	$searchModel = new PaySlipSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan]);
        $dataProvider->query->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC]);

        $grandtotal = 0;
        $amount = 0;
        if (isset($_GET['PaySlipSearch']['globalslip'])) {
	        foreach ($dataProvider->getModels() as $key => $val) {
	        	$gajibersih = $val->gajibersih;
	            $grandtotal += $gajibersih;

	        }
	    }
	    else{
	    	$gaji_kasar = PaySlip::find()->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])->all();
	    	foreach ($gaji_kasar as $key => $val) {
	            $gajibersih = $val->gajibersih;
	            $grandtotal += $gajibersih;
	    	}
	    }

        return $this->render('laporan_bank',[
        	'searchModel'=>$searchModel,
        	'dataProvider'=>$dataProvider,
        	'tahun'=>$tahun,
        	'bulan'=>$bulan,
        	'type'=>$type,
        	'grandtotal'=>number_format($grandtotal,2),
        ]);
    }

    public function actionCetak_bank($tahun,$bulan,$to)
    {
    	$tarikhmasa = date('F', mktime(0,0,0,$bulan, 1)).'_'.$tahun;

    	$model = PaySlip::find()
    			->joinWith('namestaf')
    			->where(['DATE_FORMAT(tarikhmasa, "%Y-%m")'=>$tahun.'-'.$bulan])
    			->orderBy(['maklumat_kakitangan.nama'=>SORT_ASC])
    			->all();
    	if ($to == 'pdf') {
    		$content = $this->renderPartial('cetak_bank', [
    			'model'=>$model,
    		]);

    		$pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'content' => $content,
                'filename' => 'laporan_bank_'.$tarikhmasa.'.pdf',
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
                'cssInline' => '.main{font-size:12px;font-family:courier}.head{font-size:15px;font-family:courier}',
                'options' => ['title' => 'Laporan | Bank'],
                'methods' => [
                    'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
    	}
    	elseif($to == 'excel'){
    		$objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);

            $objPHPExcel->getActiveSheet()->setTitle('Laporan Tabung Haji')
	            ->setCellValue('A1', 'Nama Kakitangan')
	            ->setCellValue('B1', 'No. Kad Pengenalan')
	            ->setCellValue('C1', 'No. Akaun')
	            ->setCellValue('D1', 'Gaji Bersih (RM)');

	        $row=2;
            foreach ($model as $key => $value) {
            	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$value['namestaf']['nama']);
            	$objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$value['namestaf']['no_kp']);
            	$objPHPExcel->getActiveSheet()->setCellValue('C'.$row,round($value['namestaf']['acc_bimb'],2));
            	$objPHPExcel->getActiveSheet()->setCellValue('D'.$row,round($value->gajibersih,2));
            	$row++;
            }

            $lastrow = $row - 1;
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, 'JUMLAH KESELURUHAN');
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, '=SUM(D2:D'.$lastrow.')');

	        $objPHPExcel->getActiveSheet()->getStyle('D'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

            $objPHPExcel->getActiveSheet()->getStyle("A".$row.":D".$lastrow)->getFont()->setBold(true);

	    	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);

            header('Content-Type: application/vnd.ms-excel');
            $filename = "laporan_bank_.".$tarikhmasa.".xls";
            header('Content-Disposition: attachment;filename='.$filename .' ');
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
    	}
    }
}