<?php  
use app\modules\report\payroll\models\ReportModel;


?>
<h2 class="head"><b>Laporan Gaji Bersih <?php echo date('F', mktime(0,0,0,Yii::$app->request->get('bulan'), 1)).' '.Yii::$app->request->get('tahun'); ?></b></h2>
<table class="table main">
    <thead>
        <tr>
            <th>Bil</th>
            <th>Nama Kakitangan</th>
            <th>Gaji Kasar</th>
            <th>Sewa Rumah</th>
            <th>EPF</th>
            <th>Socso</th>
            <th>SIP</th>
            <th>Tabung Guru</th>
            <th>Tabung Haji</th>
            <th>KKSK</th>
            <th>Bonus</th>
            <th>Loan</th>
            <th>CTG</th>
            <th>Lain-Lain</th>
            <th>Gaji Bersih</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($model as $key => $value): ?>
            <tr>
                <td><?= $key + 1 ?></td>
                <td><?= $value['namestaf']['nama'] ?></td>
                <td><?= number_format(ReportModel::getGajikasar($value->gaji_asas,$value->elaun_asas,$value->elaun_rumah),2) ?></td>
                <td><?= number_format(0-$value->sewa_rumah,2) ?></td>
                <td><?= number_format(0-$value->kwsp,2) ?></td>
                <td><?= number_format(0-$value->socso,2) ?></td>
                <td><?= number_format(0-$value->sip,2) ?></td>
                <td><?= number_format(0-$value->tabung_guru,2) ?></td>
                <td><?= number_format(0-$value->tabung_haji,2) ?></td>
                <td><?= number_format(0-$value->kksk,2) ?></td>
                <td><?= number_format($value->bonus,2) ?></td>
                <td><?= number_format(0-$value->loan,2) ?></td>
                <td><?= number_format(0-(((ReportModel::getGajikasar($value->gaji_asas,$value->elaun_asas,$value->elaun_rumah)) / date('t',strtotime($value->tarikhmasa))) * $value->ctg),2) ?></td>
                <td><?= number_format($lain_jumlah[$key] = $value->lain_tambahan - $value->lain,2) ?></td>
                <td>
                    <?php 
                        $gajikasar = ReportModel::getGajikasar($value->gaji_asas,$value->elaun_asas,$value->elaun_rumah);
                        $pelarasan = ReportModel::getPelarasan($value->hibah,$value->bonus,$value->lain_tambahan,$value->tabung_haji,$value->tabung_guru,$value->sewa_rumah,$value->kksk,$value->gaji_tahan,$value->lain,$value->loan);

                        $gaji_bersih = $gajikasar  + $pelarasan - $value->kwsp - $value->socso - $value->sip - ((($gajikasar + $value->gaji_tahan) / date('t',strtotime($value->tarikhmasa))) * $value->ctg);
                        echo number_format($gaji_bersih,2);
                    ?>        
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>

