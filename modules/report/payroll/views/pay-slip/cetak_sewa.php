<?php  
use app\modules\report\payroll\models\ReportModel;

?>
<h2 class="head"><b>Laporan Sewa Rumah <?php echo date('F', mktime(0,0,0,Yii::$app->request->get('bulan'), 1)).' '.Yii::$app->request->get('tahun'); ?></b></h2>
<table class="table main">
	<thead>
		<tr>
			<th>Bil</th>
            <th>Nama Kakitangan</th>
            <th>No. Kad Pengenalan</th>
            <th>Jumlah (RM)</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($model as $key => $value): ?>
			<tr>
				<td><?= $key+1 ?></td>
				<td><?= $value['namestaf']['nama'] ?></td>
				<td><?= $value['namestaf']['no_kp'] ?></td>
				<td><?= number_format($value->sewa_rumah,2) ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>
