<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;

use app\modules\report\payroll\models\ReportModel;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\hr\staff\models\MaklumatKakitanganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Laporan Gaji Bersih '.date('F', mktime(0,0,0,Yii::$app->request->get('bulan'), 1)).' '.Yii::$app->request->get('tahun');

$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
$(document).ready(function(){
    if($('#payroll').hasClass('pay-slip/laporan_gajibersih')){

        $('.main-menu > .scroll > ul > li#report').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }


});
JS;
$this->registerJs($script);

?>
<span id="payroll" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Pilih Tahun',['index','type'=>$type],['class'=>'btn btn-outline-primary btn-lg mr-1']) ?>
                <button class="btn btn-outline-primary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Cetak
                </button>
                <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -105px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <?= Html::a('PDF',['cetak_gajibersih','tahun'=>$tahun,'bulan'=>$bulan,'to'=>'pdf'],['class'=>'dropdown-item','target'=>'_blank']) ?>
                    <?= Html::a('Excel',['cetak_gajibersih','tahun'=>$tahun,'bulan'=>$bulan,'to'=>'excel'],['class'=>'dropdown-item']) ?>
                </div>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?= $this->render('_search', ['model' => $searchModel,'tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type]);  ?>
        
    </div>
</div>

<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body table-responsive">
                <?php Pjax::begin(['id'=>'some_pjax_id']); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "{summary}\n{items}\n<nav class='mt-4 mb-3'>{pager}</nav>",
                    'showFooter' => true,
                    'pager' => [
                        'firstPageLabel' => 'Mula',
                        'lastPageLabel' => 'Akhir',
                        'prevPageLabel' => 'Sebelumnya',
                        'nextPageLabel' => 'Seterusnya',

                        'maxButtonCount' => 5,

                         'options' => [
                            'tag' => 'ul',
                            'class' => 'pagination justify-content-center mb-0',
                        ],
                        'linkContainerOptions'=>['class'=>'page-item'],
                        'linkOptions' => ['class' => 'page-link'],
                        'activePageCssClass' => 'active',
                    ],
                    'tableOptions'=>['class'=>'table table-striped'],
                    'columns' => [
                        'namestaf.nama',
                        [
                            'label'=>'Gaji Kasar (RM)',
                            'format'=>'raw',
                            'footer'=>'<b>'.$amount.'</b>',
                            'value'=>function($data){
                                // $gaji_kasar = $data->gaji_asas + $data->elaun_asas + $data->elaun_rumah;
                                return number_format($data->gajikasar,2);
                                // return number_format($gaji_kasar,2);
                            }
                        ],
                        [
                            'label'=>'Sewa Rumah (RM)',
                            'format'=>'raw',
                            'value'=>function($data){
                                return number_format(0-$data->sewa_rumah,2);
                            }
                        ],
                        [
                            'label'=>'EPF (RM)',
                            'format'=>'raw',
                            'value'=>function($data){
                                return number_format(0-$data->kwsp,2);
                            }
                        ],
                        [
                            'label'=>'Socso (RM)',
                            'format'=>'raw',
                            'value'=>function($data){
                                return number_format(0-$data->socso,2);
                            }
                        ],
                        [
                            'label'=>'SIP (RM)',
                            'format'=>'raw',
                            'value'=>function($data){
                                return number_format(0-$data->sip,2);
                            }
                        ],
                        [
                            'label'=>'Tabung Guru (RM)',
                            'format'=>'raw',
                            'value'=>function($data){
                                return number_format(0-$data->tabung_guru,2);
                            }
                        ],
                        [
                            'label'=>'Tabung Haji (RM)',
                            'format'=>'raw',
                            'value'=>function($data){
                                return number_format(0-$data->tabung_haji,2);
                            }
                        ],
                        [
                            'label'=>'KKSK (RM)',
                            'format'=>'raw',
                            'value'=>function($data){
                                return number_format(0-$data->kksk,2);
                            }
                        ],
                        [
                            'label'=>'Bonus (RM)',
                            'format'=>'raw',
                            'value'=>function($data){
                                return number_format($data->bonus,2);
                            }
                        ],
                        [
                            'label'=>'Loan (RM)',
                            'format'=>'raw',
                            'value'=>function($data){
                                return number_format(0-$data->loan,2);
                            }
                        ],
                        [
                            'label'=>'CTG (RM)',
                            'format'=>'raw',
                            'value'=>function($data){
                                $gaji_kasar = $data->gaji_asas + $data->elaun_asas + $data->elaun_rumah;

                                $ctg = ($gaji_kasar / date('t',strtotime($data->tarikhmasa))) * $data->ctg;
                                return number_format(0-$ctg,2);
                            }
                        ],
                        [
                            'label'=>'Lain-Lain (RM)',
                            'format'=>'raw',
                            'value'=>function($data){
                                $lain_jumlah = $data->lain_tambahan - $data->lain;
                                return number_format($lain_jumlah,2);
                            }
                        ],
                        [
                            'label'=>'Gaji Bersih (RM)',
                            'format'=>'raw',
                            'value'=>function($data){
                                $gaji_kasar = $data->gaji_asas + $data->elaun_asas + $data->elaun_rumah;
                                $pelarasan =  ($data->hibah + $data->bonus + $data->lain_tambahan) - ($data->tabung_haji + $data->tabung_guru + $data->sewa_rumah + $data->kksk + $data->gaji_tahan + $data->lain + $data->loan);

                                $gaji_bersih = ($gaji_kasar + $pelarasan - $data->kwsp - $data->socso - $data->sip -((($gaji_kasar + $data->gaji_tahan) / date('t',strtotime($data->tarikhmasa))) * $data->ctg));

                                return number_format($data->gajibersih,2);
                            }
                        ],

                        // ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
            
        </div>
    </div>
</div>
