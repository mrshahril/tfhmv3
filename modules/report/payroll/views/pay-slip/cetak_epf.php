<?php  
use app\modules\report\payroll\models\ReportModel;

?>
<h2 class="head"><b>Laporan KWSP <?php echo date('F', mktime(0,0,0,Yii::$app->request->get('bulan'), 1)).' '.Yii::$app->request->get('tahun'); ?></b></h2>
<table class="table main">
	<thead>
		<tr>
			<th>Bil</th>
            <th>Nama Kakitangan</th>
            <th>No. Kad Pengenalan</th>
            <th>No. KWSP</th>
            <th>Gaji (RM)</th>
            <th>Oleh Pekerja (RM)</th>
            <th>Oleh Majikan (RM)</th>
            <th>Jumlah (RM)</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($model as $key => $value): ?>
			<tr>
				<td><?= $key+1 ?></td>
				<td><?= $value['namestaf']['nama'] ?></td>
				<td><?= $value['namestaf']['no_kp'] ?></td>
				<td><?= $value['namestaf']['no_kwsp'] ?></td>
				<td><?= number_format($value->gajikasar,2) ?></td>
				<td><?= number_format($value->kwsp,2) ?></td>
				<td><?= number_format($value->kwsp_majikan,2) ?></td>
				<td><?php $jumlah = $value->kwsp + $value->kwsp_majikan; echo number_format($jumlah,2) ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>
