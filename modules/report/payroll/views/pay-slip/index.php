<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use app\modules\report\payroll\models\ReportModel;

$this->title = ReportModel::title($type);

$script = <<< JS
$(document).ready(function(){
    if($('#laporan').hasClass('pay-slip/index')){
        $('.main-menu > .scroll > ul > li#report').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

});
JS;
$this->registerJs($script);
?>
<span id="laporan" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php  
                if ($type == 'gajibersih') {
                    $form = ActiveForm::begin([
                        'action' => ['laporan_gajibersih'],
                        'method' => 'get',
                    ]);
                }
                elseif ($type == 'epf') {
                    $form = ActiveForm::begin([
                        'action' => ['laporan_epf'],
                        'method' => 'get',
                    ]);
                }
                elseif ($type == 'socso') {
                    $form = ActiveForm::begin([
                        'action' => ['laporan_socso'],
                        'method' => 'get',
                    ]);
                }
                elseif ($type == 'sip') {
                    $form = ActiveForm::begin([
                        'action' => ['laporan_sip'],
                        'method' => 'get',
                    ]);
                }
                elseif ($type == 'tbgkbj') {
                    $form = ActiveForm::begin([
                        'action' => ['laporan_tbgkbj'],
                        'method' => 'get',
                    ]);
                }
                elseif ($type == 'tbgguru') {
                    $form = ActiveForm::begin([
                        'action' => ['laporan_tbgguru'],
                        'method' => 'get',
                    ]);
                }
                elseif ($type == 'ctg') {
                    $form = ActiveForm::begin([
                        'action' => ['laporan_ctg'],
                        'method' => 'get',
                    ]);
                }
                elseif ($type == 'sewa') {
                    $form = ActiveForm::begin([
                        'action' => ['laporan_sewa'],
                        'method' => 'get',
                    ]);
                }
                elseif ($type == 'kksk') {
                    $form = ActiveForm::begin([
                        'action' => ['laporan_kksk'],
                        'method' => 'get',
                    ]);
                }
                elseif ($type == 'tbghaji') {
                    $form = ActiveForm::begin([
                        'action' => ['laporan_tbghaji'],
                        'method' => 'get',
                    ]);
                }
                elseif ($type == 'bank') {
                    $form = ActiveForm::begin([
                        'action' => ['laporan_bank'],
                        'method' => 'get',
                    ]);
                }
            ?>
            
            <div class="box-body ml-4">
            	<div class="form-group row">
                	<label class="col-sm-2 col-form-label">Rekod Untuk Tahun</label>
                    <div class="col-sm-10">
                    	<select name="tahun" class="form-control" required="">
	                    <?php
	                        $already_selected_value = date('Y');
	                        $earliest_year = 2012;
	                        foreach (range(date('Y'), $earliest_year) as $x) {
	                            print '<option value="'.$x.'"'.($x === $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
	                        }
	                    ?>
		                </select>
                        <input type="hidden" name="type" value="<?php echo base64_encode($type) ?>">
                    </div>
            	</div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Bulan</label>
                    <div class="col-sm-10">
                        <select name="bulan" class="form-control" required="">
                            <?php 
                                for ($m=1; $m<=12; $m++) {
                                    $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
 
                            ?>
                                <option value="<?php echo date('m',strtotime($month)) ?>"><?php echo $month ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success mb-1">Seterusnya</button>
                    <?= Html::a('Kembali',['managesalary'],['class'=>'btn btn-danger']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

    	</div>
    </div>
</div>