<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\hr\staff\models\MaklumatKakitanganSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<?php 

    if (base64_decode($type) == 'gajibersih') {
         $form = ActiveForm::begin([
            'action' => ['laporan_gajibersih','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type],
            'method' => 'get',
        ]);
    }
    elseif( base64_decode($type) == 'epf') {
        $form = ActiveForm::begin([
            'action' => ['laporan_epf','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type],
            'method' => 'get',
        ]);
    }
    elseif( base64_decode($type) == 'socso') {
        $form = ActiveForm::begin([
            'action' => ['laporan_socso','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type],
            'method' => 'get',
        ]);
    }
    elseif( base64_decode($type) == 'sip') {
        $form = ActiveForm::begin([
            'action' => ['laporan_sip','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type],
            'method' => 'get',
        ]);
    }
    elseif( base64_decode($type) == 'tbgkbj') {
        $form = ActiveForm::begin([
            'action' => ['laporan_tbgkbj','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type],
            'method' => 'get',
        ]);
    }
    elseif( base64_decode($type) == 'tbgguru') {
        $form = ActiveForm::begin([
            'action' => ['laporan_tbgguru','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type],
            'method' => 'get',
        ]);
    }
    elseif( base64_decode($type) == 'ctg') {
        $form = ActiveForm::begin([
            'action' => ['laporan_ctg','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type],
            'method' => 'get',
        ]);
    }
    elseif( base64_decode($type) == 'sewa') {
        $form = ActiveForm::begin([
            'action' => ['laporan_sewa','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type],
            'method' => 'get',
        ]);
    }
    elseif( base64_decode($type) == 'kksk') {
        $form = ActiveForm::begin([
            'action' => ['laporan_kksk','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type],
            'method' => 'get',
        ]);
    }
    elseif( base64_decode($type) == 'tbghaji') {
        $form = ActiveForm::begin([
            'action' => ['laporan_tbghaji','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type],
            'method' => 'get',
        ]);
    }
    elseif( base64_decode($type) == 'bank') {
        $form = ActiveForm::begin([
            'action' => ['laporan_bank','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type],
            'method' => 'get',
        ]);
    }
    // elseif ($type == 'epf') {
    //     $form = ActiveForm::begin([
    //         'action' => ['laporan_epf','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type],
    //         'method' => 'get',
    //     ]);
    // }
    
?>
<div class="card mb-4">
    <div class="card-body">
        <div class="input-group">
            <input type="text" id="payslipsearch-globalstaff" class="form-control" name="PaySlipSearch[globalslip]" placeholder="Carian..." aria-describedby="basic-addon2" autocomplete="off">

            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit">Carian</button>
                <?php  
                    if (base64_decode($type) == 'gajibersih') {
                         echo Html::a('Reset', ['laporan_gajibersih','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type], ['class' => 'btn btn-outline-secondary']);
                    }
                    elseif (base64_decode($type) == 'epf') {
                        echo Html::a('Reset', ['laporan_epf','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type], ['class' => 'btn btn-outline-secondary']);
                    }
                    elseif (base64_decode($type) == 'socso') {
                        echo Html::a('Reset', ['laporan_socso','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type], ['class' => 'btn btn-outline-secondary']);
                    }
                    elseif (base64_decode($type) == 'sip') {
                        echo Html::a('Reset', ['laporan_sip','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type], ['class' => 'btn btn-outline-secondary']);
                    }
                    elseif (base64_decode($type) == 'tbgkbj') {
                        echo Html::a('Reset', ['laporan_tbgkbj','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type], ['class' => 'btn btn-outline-secondary']);
                    }
                    elseif (base64_decode($type) == 'tbgguru') {
                        echo Html::a('Reset', ['laporan_tbgguru','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type], ['class' => 'btn btn-outline-secondary']);
                    }
                    elseif (base64_decode($type) == 'ctg') {
                        echo Html::a('Reset', ['laporan_ctg','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type], ['class' => 'btn btn-outline-secondary']);
                    }
                    elseif (base64_decode($type) == 'sewa') {
                        echo Html::a('Reset', ['laporan_sewa','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type], ['class' => 'btn btn-outline-secondary']);
                    }
                    elseif (base64_decode($type) == 'kksk') {
                        echo Html::a('Reset', ['laporan_kksk','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type], ['class' => 'btn btn-outline-secondary']);
                    }
                    elseif (base64_decode($type) == 'tbghaji') {
                        echo Html::a('Reset', ['laporan_tbghaji','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type], ['class' => 'btn btn-outline-secondary']);
                    }
                    elseif (base64_decode($type) == 'bank') {
                        echo Html::a('Reset', ['laporan_bank','tahun'=>$tahun,'bulan'=>$bulan,'type'=>$type], ['class' => 'btn btn-outline-secondary']);
                    }
                ?>

            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
