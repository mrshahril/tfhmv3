<?php

namespace app\modules\report\payroll;

/**
 * pelajar module definition class
 */
class Payroll extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\report\payroll\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
