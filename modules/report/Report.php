<?php

namespace app\modules\report;

/**
 * report module definition class
 */
class Report extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\report\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules = [
            'pelajar' => [
                'class' => 'app\modules\report\pelajar\Pelajar',
            ],
            'staff' => [
                'class' => 'app\modules\report\staff\Staff',
            ],
            'payroll' => [
                'class' => 'app\modules\report\payroll\Payroll',
            ],
        ];
        

        // custom initialization code goes here
    }
}
