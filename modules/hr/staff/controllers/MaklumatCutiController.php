<?php

namespace app\modules\hr\staff\controllers;

use Yii;
use app\modules\hr\staff\models\MaklumatCuti;
use app\modules\hr\staff\models\MaklumatKakitangan;
use app\modules\hr\staff\models\MaklumatCutiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MaklumatCutiController implements the CRUD actions for MaklumatCuti model.
 */
class MaklumatCutiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MaklumatCuti models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MaklumatCutiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MaklumatCuti model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel(base64_decode($id)),
        ]);
    }

    /**
     * Creates a new MaklumatCuti model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MaklumatCuti();

        if ($model->load(Yii::$app->request->post()) ) {
            $tarikh = date("Y",strtotime($_POST['MaklumatCuti']['tarikh_mula']));
            $bulan = date("m",strtotime($_POST['MaklumatCuti']['tarikh_mula']));

            $model->tahun = $tarikh;
            $model->bulan = $bulan;
            $model->created_at = date('Y-m-d h:i:s A');
            if ($model->save()) {
                Yii::$app->session->setFlash('savedone','Maklumat cuti telah berjaya disimpan');
                return $this->redirect(['view', 'id' => base64_encode($model->id)]);
            }
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    public function actionStaff($q = null, $id = null){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
        $connection = \Yii::$app->db;
        $sql = $connection->createCommand('SELECT id_staf,nama AS text FROM maklumat_kakitangan WHERE nama LIKE "%'.$q.'%" OR no_kp ="%'.$q.'%"');

        $model = $sql->queryAll();
        $out['results'] = array_values($model);

        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => MaklumatKakitangan::find($id)->nama];
        }
        return $out;
    }
    /**
     * Updates an existing MaklumatCuti model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel(base64_decode($id));

        if ($model->load(Yii::$app->request->post()) ) {
            $model->updated_at = date('Y-m-d h:i:s A');
            if ($model->save()) {
                Yii::$app->session->setFlash('savedone','Maklumat cuti telah berjaya dikemaskini');
                return $this->redirect(['view', 'id' => $id]);
            }
            
        }
        return $this->render('_edit', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MaklumatCuti model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel(base64_decode($id));

        if ($model->delete()) {
            Yii::$app->session->setFlash('savedone','Maklumat cuti telah berjaya dihapuskan');
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the MaklumatCuti model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MaklumatCuti the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MaklumatCuti::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
