<?php

namespace app\modules\hr\staff\controllers;

use Yii;
use app\modules\hr\staff\models\MaklumatKakitangan;
use app\modules\hr\staff\models\MaklumatKakitanganSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use yii\web\UploadedFile;
use kartik\mpdf\Pdf;

/**
 * MaklumatKakitanganController implements the CRUD actions for MaklumatKakitangan model.
 */
class MaklumatKakitanganController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MaklumatKakitangan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MaklumatKakitanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status_pekerjaan'=>1]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MaklumatKakitangan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel(base64_decode($id)),
        ]);
    }

    /**
     * Creates a new MaklumatKakitangan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MaklumatKakitangan();
        $model->scenario = 'new_staff';

        if ($model->load(Yii::$app->request->post()) ) {

            $model->file = UploadedFile::getInstance($model,'file');
            if (!empty($model->file)) {
                $model->file->saveAs('uploads/'.$model->file->baseName.'.'.$model->file->extension);
                $model->foto = $model->file->baseName.'.'.$model->file->extension;
            }
            else{
                $model->foto = 'default-image.png';
            }

            $model->rujukan_tawaran = strtoupper($_POST['MaklumatKakitangan']['rujukan_tawaran']);
            $model->nama = strtoupper($_POST['MaklumatKakitangan']['nama']);
            $model->no_pekerja = strtoupper($_POST['MaklumatKakitangan']['no_pekerja']);
            
            if ($model->save()) {
                Yii::$app->session->setFlash('savedone','Maklumat kakitangan telah berjaya disimpan');
                return $this->redirect(['view', 'id' =>base64_encode($model->id_staf)]);
            }
            
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MaklumatKakitangan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_staf]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionEdit($id)
    {
        $model = $this->findModel(base64_decode($id));

        if ($model->load(Yii::$app->request->post())) {
            $model->rujukan_tawaran = strtoupper($_POST['MaklumatKakitangan']['rujukan_tawaran']);
            $model->nama = strtoupper($_POST['MaklumatKakitangan']['nama']);
            $model->no_pekerja = strtoupper($_POST['MaklumatKakitangan']['no_pekerja']);

            $model->updated_at = date('Y-m-d h:i:s A');
            // $model->updated_by = Yii::$app->user->identity->id;

            if ($model->save()) {
                Yii::$app->session->setFlash('savedone','Maklumat kakitangan telah berjaya dikemaskini');
                if (base64_decode(Yii::$app->request->get('resign')) == 'y') {
                    return $this->redirect(['view','id'=>$id,'resign'=>Yii::$app->request->get('resign')]);
                }
                else{
                    return $this->redirect(['view','id'=>$id]);
                }
                
            }
        }
        else{
            return $this->render('_edit',[
                'model'=>$model,
            ]);
        }
    }

    public function actionUpdateimg($id)
    {
        $model = $this->findModel(base64_decode($id));

        if ($model->load(Yii::$app->request->post())) {

            $model->file = UploadedFile::getInstance($model,'file');

            if (!empty($model->file)) {
                // $model->file = UploadedFile::getInstance($model,'file');

                $model->file->saveAs('uploads/'.$model->file->baseName.'.'.$model->file->extension);

                $model->foto = $model->file->baseName.'.'.$model->file->extension;
                
            }
            else{
                $model->foto = 'default-image.png';
            }

            $model->updated_at = date('Y-m-d h:i:s A');

            if ($model->save()) {
                Yii::$app->session->setFlash('savedone','Gambar kakitangan telah berjaya dikemaskini');
                if (base64_decode(Yii::$app->request->get('resign')) == 'y') {
                    return $this->redirect(['view','id'=>$id,'resign'=>Yii::$app->request->get('resign')]);
                }
                else{
                    return $this->redirect(['view', 'id' => $id]);
                }   
            }

        } else {
            Yii::$app->session->setFlash('saveerror','Ralat ! Gambar tidak berjaya dikemaskini.');
                return $this->redirect(['view', 'id' => $id]);
        }
    }

    public function actionAdd_resign($id)
    {       
        $model = $this->findModel(base64_decode($id));
        $nama = $model->nama;
        if ($model->load(Yii::$app->request->post()) ) {

            $model->updated_at = date('Y-m-d h:i:s A');
            if ($model->save()) {
                if (base64_decode(Yii::$app->request->get('resign')) == 'y') {
                    Yii::$app->session->setFlash('savedone', $nama.' telah di aktifkan semula');
                    return $this->redirect(['resign']);
                }
                else{
                    Yii::$app->session->setFlash('savedone','Maklumat '.$nama.' telah di pindah ke maklumat kakitangan berhenti');
                    return $this->redirect(['index']);
                }
            }   
        } else {
            return $this->renderAjax('add_resign',[
                'model'=>$model,
            ]);
        }
    }

    /**
     * Deletes an existing MaklumatKakitangan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel(base64_decode($id));
        $nama = $model->nama;
        $pic = $model->foto;

        if ($pic === 'default-image.png') {
            $model->delete();
        }
        else{
            $model->delete();
            unlink(Yii::getAlias('@webroot/uploads/'.$pic));
            
        }

        Yii::$app->session->setFlash('savedone','Maklumat '.$nama.' telah di hapuskan');

        if (base64_decode(Yii::$app->request->get('resign')) == 'y') {
            return $this->redirect(['resign']);
        }
        else{
            return $this->redirect(['index']);
        }
        
    }

    /**
     * Finds the MaklumatKakitangan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MaklumatKakitangan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MaklumatKakitangan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /** RESIGN **/

    public function actionResign()
    {
        $searchModel = new MaklumatKakitanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status_pekerjaan'=>2]);

        return $this->render('resign', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /** END RESIGN **/
    
    /** SURAT TAWARAN **/
    public function actionSurat()
    {
        $searchModel = new MaklumatKakitanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->andWhere(['status_pekerjaan'=>1]);

        return $this->render('surat-tawaran/surat', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSurat_tawaran($id)
    {
        $model = $this->findModel(base64_decode($id));

        if (empty($model->peringkat_gaji)) {
            Yii::$app->session->setFlash('error','Maklumat '.$model->nama.' tidak lengkap. Sila Pilih di ruangan Pilihan Kemaskini Maklumat untuk mengemaskini maklumat kakitangan');

            return $this->redirect(['surat']);
        }
        else{
            $content = $this->renderPartial('surat-tawaran/surat_tawaran', ['model'=>$model]);
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'content' => $content,
            //     'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // // any css to be embedded if required
            //     'cssInline' => '.kv-heading-1{font-size:9px}', 
                'options' => ['title' => 'Maklumat Kakitangan'],
                'methods' => [
                    'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionSurat_tawaran_pentadbiran($id)
    {
        $model = $this->findModel(base64_decode($id));

        if (empty($model->peringkat_gaji)) {
            Yii::$app->session->setFlash('error','Maklumat '.$model->nama.' tidak lengkap. Sila Pilih di ruangan Pilihan Kemaskini Maklumat untuk mengemaskini maklumat kakitangan');

            return $this->redirect(['surat']);
        }
        else{
            $content = $this->renderPartial('surat-tawaran/surat_tawaran_pentadbiran', ['model'=>$model]);
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'content' => $content, 
                'options' => ['title' => 'Maklumat Kakitangan'],
                'methods' => [
                    'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                    'SetFooter'=>['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        } 
    }

    public function actionSurat_perjanjian($id)
    {   
        $content = $this->renderPartial('surat-tawaran/surat_perjanjian', ['model'=>$this->findModel(base64_decode($id))]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'content' => $content, 
            'options' => ['title' => 'Maklumat Kakitangan'],
            'methods' => [
                'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
        return $pdf->render();   
    }

    public function actionEditsalary($id)
    {
        $model = $this->findModel(base64_decode($id));

        if ($model->load(Yii::$app->request->post()) ) {

            $nama = $model->nama;
            $model->updated_at = date('Y-m-d h:i:s A');
            if ($model->save()) {
                Yii::$app->session->setFlash('savedone','Maklumat '.$nama.' telah berjaya dikemaskini');
                return $this->redirect(['surat']);
            }
        } else {
            return $this->render('surat-tawaran/editsalary', [
                'model' => $model,
            ]);
        }
    }
    /** END SURAT **/

}
