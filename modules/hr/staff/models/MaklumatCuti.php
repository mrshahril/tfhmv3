<?php

namespace app\modules\hr\staff\models;

use Yii;

use app\modules\hr\staff\models\MaklumatKakitangan;
use app\models\LookupCuti;
/**
 * This is the model class for table "maklumat_cuti".
 *
 * @property int $id
 * @property int $id_staff
 * @property int $jenis_cuti
 * @property int $tahun
 * @property int $bulan
 * @property string $bilangan_cuti
 * @property string $tarikh_mula
 * @property string $tarikh_akhir
 * @property string $sebab_cuti
 */
class MaklumatCuti extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'maklumat_cuti';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_staff','tarikh_akhir','tarikh_mula','bilangan_cuti','jenis_cuti'],'required', 'message' => 'Ruangan {attribute} ini wajib diisi'],
            [['id_staff', 'jenis_cuti', 'tahun', 'bulan'], 'integer'],
            [['bilangan_cuti'], 'string', 'max' => 100],
            [['tarikh_mula', 'tarikh_akhir','created_at','updated_at'], 'string', 'max' => 50],
            [['sebab_cuti'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_staff' => 'Nama Kakitangan',
            'jenis_cuti' => 'Jenis Cuti',
            'tahun' => 'Tahun',
            'bulan' => 'Bulan',
            'bilangan_cuti' => 'Bilangan Cuti',
            'tarikh_mula' => 'Tarikh Mula',
            'tarikh_akhir' => 'Tarikh Akhir',
            'sebab_cuti' => 'Sebab Cuti',
        ];
    }

    public function getNama_kakitangan()
    {
        return $this->hasOne(MaklumatKakitangan::className(),['id_staf' =>'id_staff']);
    }

    public function getCuti()
    {
        return $this->hasOne(LookupCuti::className(),['id'=>'jenis_cuti']);
    }
}
