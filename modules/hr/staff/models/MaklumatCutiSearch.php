<?php

namespace app\modules\hr\staff\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\hr\staff\models\MaklumatCuti;

/**
 * MaklumatCutiSearch represents the model behind the search form of `app\modules\hr\staff\models\MaklumatCuti`.
 */
class MaklumatCutiSearch extends MaklumatCuti
{
    /**
     * {@inheritdoc}
     */

    public $globalSearch;

    public function rules()
    {
        return [
            [['id', 'id_staff', 'jenis_cuti', 'tahun', 'bulan'], 'integer'],
            [['bilangan_cuti', 'tarikh_mula', 'tarikh_akhir', 'sebab_cuti','globalSearch'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MaklumatCuti::find();
        $query->joinWith(['nama_kakitangan']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->orFilterWhere(['like', 'bilangan_cuti', $this->globalSearch])
            ->orFilterWhere(['like', 'tarikh_mula', $this->globalSearch])
            ->orFilterWhere(['like', 'tarikh_akhir', $this->globalSearch])
            ->orFilterWhere(['like', 'maklumat_kakitangan.nama', $this->globalSearch])
            ->orFilterWhere(['like', 'sebab_cuti', $this->globalSearch]);

        return $dataProvider;
    }
}
