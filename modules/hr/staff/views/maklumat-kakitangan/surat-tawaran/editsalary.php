<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\LookupPeringkatGaji;
use app\models\LookupTanggaGaji;

$peringkatgaji = ArrayHelper::map(LookupPeringkatGaji::find()->orderBy(['peringkat_gaji'=>SORT_ASC])->asArray()->all(),'id','peringkat_gaji');
$tanggagaji = ArrayHelper::map(LookupTanggaGaji::find()->orderBy(['id'=>SORT_ASC])->asArray()->all(),'id','tangga_gaji');

$this->title = 'Surat Tawaran';

$script = <<< JS
$(document).ready(function(){
	if($('#staff').hasClass('maklumat-kakitangan/editsalary')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }
});
JS;
$this->registerJs($script);
?>
<span id="staff" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['surat'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
            	<div class="card-body">
                    <h5 class="mb-4">Kemaskini Maklumat Kewangan</h5>
                    <form>
                    	<div class="form-row">
                    		<div class="form-group col-md-12">
                                 <?php if ($model->gaji_asas == 0.00 || $model->peringkat_gaji == '' || $model->tangga_gaji == '') {?>
								<div class="alert alert-danger">
									<ul>
										<?php if ($model->gaji_asas == 0.00) {?>
											<li>Gaji asas <strong><?= $model->nama ?></strong> perlu di kemaskini.</li>
										<?php }?>
										<?php if ($model->peringkat_gaji == '') {?>
											<li>Peringkat Gaji <strong><?= $model->nama ?></strong> perlu di kemaskini.</li>
										<?php }?>
										<?php if ($model->tangga_gaji == '') {?>
											<li>Tangga Gaji <strong><?= $model->nama ?></strong> perlu di kemaskini.</li>
										<?php }?>
									</ul>
				                    
				                </div>
				               	<?php }else{}?>
                    		</div>
                    	</div>
                    	<div class="form-row">
                    		<div class="form-group col-md-6">
                    			<label>Nama Kakitangan</label>
                    			<?= $form->field($model, 'nama')->textInput(['maxlength' => true,'class'=>'form-control','placeholder'=>'Cth : Ahmad Bin Abdullah','style'=>"text-transform: uppercase;",'autocomplete'=>'off','disabled'=>''])->label(false) ?>
                    		</div>
                    		<div class="form-group col-md-6">
                                 <label>Rujukan Tawaran</label>
                                 <?= $form->field($model, 'rujukan_tawaran')->textInput(['maxlength' => true,'class'=>'form-control','style'=>"text-transform: uppercase;",'autocomplete'=>'off'])->label(false) ?>
                             </div>
                    	</div>
                    	<div class="form-row">
	                         <div class="form-group col-md-12">
	                            <label>Alamat Surat Menyurat</label>
	                             <?= $form->field($model, 'alamat_surat_menyurat')->textArea(['class'=>'form-control','rows'=>5])->label(false) ?>
	                         </div>
	                     </div>
	                     <div class="form-row">
	                         <div class="form-group col-md-4">
	                            <label>Gaji Asas</label>
	                         	<?= $form->field($model, 'gaji_asas')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
	                         </div>
	                         <div class="form-group col-md-4">
	                            <label>Peringkat Gaji</label>
	                         	<?= $form->field($model, 'peringkat_gaji')->dropDownList($peringkatgaji, 
		                                [
		                                    'prompt'=>'Sila Pilih'
		                                ]
		                            )->label(false)
		                        ?>
	                         </div>
	                         <div class="form-group col-md-4">
	                            <label>Tangga Gaji</label>
	                         	<?= $form->field($model, 'tangga_gaji')->dropDownList($tanggagaji, 
		                                [
		                                    'prompt'=>'Sila Pilih'
		                                ]
		                            )->label(false)
		                        ?>
	                         </div>
	                     </div>
	                     <div class="form-row">
	                         <div class="form-group col-md-6">
	                            <label>Skim Gaji Awal</label>
	                            <?= $form->field($model, 'skim_gaji_awal')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
	                         </div>
	                         <div class="form-group col-md-6">
	                            <label>Skim Gaji</label>
	                            <?= $form->field($model, 'skim_gaji')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
	                         </div>
	                     </div>
            		</form>
            	</div>
            	<div class="box-footer">
                    <div class="col-md-12">
                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success mb-1']) ?>
                        <?= Html::a('Batal',['surat'],['class'=>'btn btn-danger mb-1']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
