<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ButtonDropdown;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MaklumatKakitanganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Surat Tawaran | Senarai Kakitangan';
$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
$(document).ready(function(){
    if($('#staff').hasClass('maklumat-kakitangan/surat')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }
});
JS;
$this->registerJs($script);
?>
<span id="staff" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>

<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <?= $this->render('_searchsurat', ['model' => $searchModel]);  ?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('error')) { ?>
            <div class="alert alert-danger" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('error'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body table-responsive">
                <?php Pjax::begin(); ?>    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'nama',
                        'no_kp',
                        // 'jawatan.jawatan',
                        //'nama_tahfiz.pusat_pengajian',
                        [
                            'attribute'=>'status_pekerjaan',
                            'format'=>'raw',
                            'value'=>function($data){
                                if ($data->status_pekerjaan == 1) {
                                    $btn = 'btn btn-xs btn-success';
                                }
                                elseif ($data->status_pekerjaan == 2) {
                                    $btn = 'btn btn-xs btn-danger';
                                }

                                return Html::a($data->statuskerja->status_pekerjaan,FALSE,['class'=>$btn,'style'=>'color:#fff;']);
                            }
                        ],
                        'rujukan_tawaran',
                        [
                            'header' => 'Tindakan',
                            'class' => 'yii\grid\ActionColumn',
                            'template'=>'{all}',
                            'buttons' => [
                                'all' => function ($url, $model, $key) {
                                    return ButtonDropdown::widget([
                                        'encodeLabel' => false, // if you're going to use html on the button label
                                        'label' => 'Pilihan Tindakan',
                                        'dropdown' => [
                                            'encodeLabels' => false, // if you're going to use html on the items' labels
                                            'items' => [
                                                [
                                                    'label' => \Yii::t('yii', 'Surat Tawaran'),
                                                    'url' => ['maklumat-kakitangan/surat_tawaran', 'id' =>base64_encode($key)],
                                                    
                                                    'linkOptions' => ['class'=>'dropdown-item','target'=>'_blank','data-pjax'=>0],
                                                    'visible' => true,  // if you want to hide an item based on a condition, use this
                                                ],
                                                [
                                                    'label' => \Yii::t('yii', 'Salinan Pentadbiran'),
                                                    'url' => ['maklumat-kakitangan/surat_tawaran_pentadbiran', 'id' =>base64_encode($key)],
                                                    
                                                    'linkOptions' => ['class'=>'dropdown-item','target'=>'_blank','data-pjax'=>0],
                                                    'visible' => true,  // if you want to hide an item based on a condition, use this
                                                ],
                                                [
                                                    'label' => \Yii::t('yii', 'Surat Perjanjian'),
                                                    'url' => ['maklumat-kakitangan/surat_perjanjian', 'id' =>base64_encode($key)],
                                                    
                                                    'linkOptions' => ['class'=>'dropdown-item','target'=>'_blank','data-pjax'=>0],
                                                    'visible' => true,  // if you want to hide an item based on a condition, use this
                                                ],
                                                [
                                                    'label' => \Yii::t('yii', 'Kemaskini Maklumat'),
                                                    'url' => ['maklumat-kakitangan/editsalary', 'id' =>base64_encode($key)],
                                                    
                                                    'linkOptions' => ['class'=>'dropdown-item'],
                                                    'visible' => true,  // if you want to hide an item based on a condition, use this
                                                ],                                       
                                            ],
                                        ],
                                        'options' => [
                                            'class' => 'btn btn-outline-primary btn-sm',   // btn-success, btn-info, et cetera
                                        ],
                                        'split' => false,    // if you want a split button
                                    ]);
                                },
                            ],

                            ],
                        
                        //['class' => 'yii\grid\ActionColumn'],
                    ],
                    'tableOptions' =>['class' => 'table table-striped table-hover'],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
