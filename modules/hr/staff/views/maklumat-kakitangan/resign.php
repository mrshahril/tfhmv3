<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\hr\staff\models\MaklumatKakitanganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Maklumat Kakitangan Berhenti';
$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
$(document).ready(function(){
    if($('#staff').hasClass('maklumat-kakitangan/resign')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    function init_click_handlers(){
        $('.modaltest').click(function(){
            $('#exampleModal').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));

        });
    }
    
    init_click_handlers(); //first run
    deleteswal();
    $("#some_pjax_id").on("pjax:success", function() {
      init_click_handlers(); //reactivate links in grid after pjax update
      deleteswal();
    });

    function deleteswal(){
        $('a.delete').on('click', function(e){
             e.preventDefault();
            var link = $(this).attr('value');
            console.log(link);
          swal({   
            title: "Anda pasti mahu hapus kakitangan ini dari sistem ?",
            // text: "Perhatian ! Proses ini tidak boleh diulang semua !",   
            type: "warning",   
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: 'Ya, saya pasti.',
            confirmButtonClass: 'btn-info',
            cancelButtonText: 'Batal',
            cancelButtonClass: 'btn-danger',

          }, 
           function(){   
                // $("#myform").submit();
                $.ajax({
                    type: 'POST',
                    url: link,
                    success: function(data) {
                    
                   }
                });
            });
        })
    }

});
JS;
$this->registerJs($script);

?>
<span id="staff" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?= $this->render('_searchresign', ['model' => $searchModel]);  ?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body table-responsive">
                <?php Pjax::begin(['id'=>'some_pjax_id']); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "{summary}\n{items}\n<nav class='mt-4 mb-3'>{pager}</nav>",
                    'pager' => [
                        'firstPageLabel' => 'Mula',
                        'lastPageLabel' => 'Akhir',
                        'prevPageLabel' => 'Sebelumnya',
                        'nextPageLabel' => 'Seterusnya',

                        'maxButtonCount' => 5,

                         'options' => [
                            'tag' => 'ul',
                            'class' => 'pagination justify-content-center mb-0',
                        ],
                        'linkContainerOptions'=>['class'=>'page-item'],
                        'linkOptions' => ['class' => 'page-link'],
                        'activePageCssClass' => 'active',
                    ],
                    'tableOptions'=>['class'=>'table table-striped'],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'Foto',
                            'format' => 'html',    
                            'value' => function ($data) {
                                $html = $data->foto ? Html::img(Yii::getAlias('@web').'/uploads/'. $data['foto'],
                                    ['width' => '70px']) : '';
                                return $html;
                            },
                        ],

                        'nama',
                        'no_kp',
                        'jawatan.jawatan',
                        'tarikh_resign',
                        //'nama_tahfiz.pusat_pengajian',
                        //'statuskerja.status_pekerjaan',
                        'no_pekerja',
                        
                        [
                            'header' => 'Tindakan',
                            'class' => 'yii\grid\ActionColumn',
                            'template'=>'{all}',
                            'buttons' => [
                                'all' => function ($url, $model, $key) {
                                    return ButtonDropdown::widget([
                                        'encodeLabel' => false, // if you're going to use html on the button label
                                        'label' => 'Pilih Tindakan',
                                        'dropdown' => [
                                            'encodeLabels' => false, // if you're going to use html on the items' labels
                                            'items' => [
                                                [
                                                    'label' => \Yii::t('yii', 'Pindah Aktif'),
                                                    'url' => FALSE,
                                                    
                                                    'linkOptions' => ['class'=>'dropdown-item modaltest','value'=>Url::to(['maklumat-kakitangan/add_resign', 'id' => base64_encode($key),'resign'=>base64_encode('y')]),'data-pjax'=>0],
                                                    'visible' => true,  // if you want to hide an item based on a condition, use this
                                                ],
                                                [
                                                    'label' => \Yii::t('yii', 'Lihat Maklumat'),
                                                    'url' => ['maklumat-kakitangan/view', 'id' =>base64_encode($key),'resign'=>base64_encode('y')],
                                                    'linkOptions' => ['class'=>'dropdown-item'],
                                                    'visible' => true,  // if you want to hide an item based on a condition, use this
                                                ],
                                                [
                                                    'label' => \Yii::t('yii', 'Kemaskini Maklumat'),
                                                    'url' => ['maklumat-kakitangan/edit', 'id' => base64_encode($key),'resign'=>base64_encode('y')],
                                                    'linkOptions' => ['class'=>'dropdown-item'],
                                                    'visible' => true,  // if you want to hide an item based on a condition, use this
                                                ],
                                                [
                                                    'label' => \Yii::t('yii', 'Hapus'),
                                                    'url'=>FALSE,
                                                    'linkOptions' => ['id'=>'delete','class'=>'dropdown-item delete','value'=>Url::to(['delete','id'=>base64_encode($key),'resign'=>base64_encode('y')])],
                                                    'visible' => true,  // if you want to hide an item based on a condition, use this
                                                ],
                                                
                                            ],
                                            
                                        ],
                                        'options' => [
                                            'class' => 'btn btn-outline-primary btn-sm',   // btn-success, btn-info, et cetera
                                        ],
                                        'split' => false,    // if you want a split button
                                    ]);
                                },
                            ],
                        ],
                        // ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
            
        </div>
    </div>
</div>
