<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\LookupJawatan;
use app\models\LookupPusatPengajian;

/* @var $this yii\web\View */
/* @var $model app\modules\hr\staff\models\MaklumatKakitangan */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Kemaskini Maklumat';

$jawatan = ArrayHelper::map(LookupJawatan::find()->orderBy(['jawatan'=>SORT_ASC])->asArray()->all(),'id','jawatan');
$tahfiz = ArrayHelper::map(LookupPusatPengajian::find()->orderBy(['pusat_pengajian'=>SORT_ASC])->asArray()->all(),'id','pusat_pengajian'); //retrieve data for dropdown

$getimage = Yii::$app->request->baseUrl.'/image/no-image.png';

$script = <<< JS
$(document).ready(function(){
    if($('#staff').hasClass('maklumat-kakitangan/edit')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }
});
JS;
$this->registerJs($script);
?>

<span id="staff" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>

<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?php  
                    if (base64_decode(Yii::$app->request->get('resign')) == 'y') {
                        echo Html::a('Kembali',['resign'],['class'=>'btn btn-outline-primary btn-lg']);
                    }
                    else{
                        echo Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg']);
                    }
                ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
                <div class="card-body">
                    <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>
                    <form>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Rujukan Tawaran</label>
                                 <?= $form->field($model, 'rujukan_tawaran')->textInput(['maxlength' => true,'class'=>'form-control','style'=>"text-transform: uppercase;"])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Nombor Pekerja <span class="font-red">**</span></label>
                                 <?= $form->field($model, 'no_pekerja')->textInput(['maxlength' => true,'class'=>'form-control','placeholder'=>'Cth : TSB000','style'=>"text-transform: uppercase;"])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Nama Kakitangan <span class="font-red">**</span></label>
                                 <?= $form->field($model, 'nama')->textInput(['maxlength' => true,'class'=>'form-control','placeholder'=>'Cth : Ahmad Bin Abdullah','style'=>"text-transform: uppercase;"])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Nombor Kad Pengenalan <span class="font-red">**</span></label>
                                 <?= $form->field($model, 'no_kp')->textInput(['maxlength' => true,'class'=>'form-control','placeholder'=>'Cth : 123456789012 (tanpa " - ")'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-12">
                                <label>Alamat Tetap</label>
                                 <?= $form->field($model, 'alamat_tetap')->textArea(['class'=>'form-control','rows'=>5])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-12">
                                <label>Alamat Surat Menyurat</label>
                                 <?= $form->field($model, 'alamat_surat_menyurat')->textArea(['class'=>'form-control','rows'=>5])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-12">
                                <label>Alamat Tempat Kerja</label>
                                 <?= $form->field($model, 'alamat_tempat_kerja')->textArea(['class'=>'form-control','rows'=>5])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                <label>Tempat Lahir</label>
                                <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Warganegara <span class="font-red">**</span></label>
                                 <?= $form->field($model, 'warganegara')->dropDownList(
                                    [
                                        'Warganegara'=>'Warganegara',
                                        'Bukan Warganegara'=>'Bukan Warganegara',
                                    ], 
                                    [
                                        'prompt'=>'Sila Pilih'
                                    ]
                                )->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Tarikh Mula Kerja</label>
                                 <?= $form->field($model, 'tarikh_mula_kerja')->textInput(['maxlength' => true,'class'=>'form-control datepicker','data-date-format'=>'yyyy-mm-dd'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Tarikh Lahir</label>
                                 <?= $form->field($model, 'tarikh_lahir')->textInput(['maxlength' => true,'class'=>'form-control datepicker','data-date-format'=>'yyyy-mm-dd'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Jawatan <span class="font-red">**</span></label>
                                 <?= $form->field($model, 'jawatan_sekarang')->dropDownList($jawatan, 
                                    [
                                        'prompt'=>'Sila Pilih'
                                    ]
                                    )->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Tahfiz <span class="font-red">**</span></label>
                                 <?= $form->field($model, 'tahfiz')->dropDownList($tahfiz, 
                                        [
                                            'prompt'=>'Sila Pilih'
                                        ]
                                    )->label(false)
                                ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Telefon Rumah</label>
                                 <?= $form->field($model, 'no_tel_rumah')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false)?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Telefon Bimbit</label>
                                 <?= $form->field($model, 'no_tel_bimbit')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Tinggi</label>
                                 <?= $form->field($model, 'tinggi_cm')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false)?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Berat</label>
                                 <?= $form->field($model, 'berat_kg')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Tahap Kesihatan</label>
                                 <?= $form->field($model, 'tahap_kesihatan')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false)?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Pengesahan Kesihatan</label>
                                 <?= $form->field($model, 'pengesahan_kesihatan')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Kecacatan</label>
                                 <?= $form->field($model, 'kecacatan')->dropDownList(
                                        [
                                            'Ya'=>'Ya',
                                            'Tidak'=>'Tidak',
                                        ], 
                                        [
                                            'prompt'=>'Sila Pilih'
                                        ]
                                    )->label(false)
                                ?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Nyatakan Kecatatan</label>
                                 <?= $form->field($model, 'nyatakan_kecacatan')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                <label>Nama Kata Usrah</label>
                                 <?= $form->field($model, 'nama_ketua_usrah')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                <label>Kumpulan Usrah</label>
                                 <?= $form->field($model, 'kumpulan_usrah')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                <label>Unit Usrah</label>
                                 <?= $form->field($model, 'unit_usrah')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                <label>No Ahli Abim</label>
                                 <?= $form->field($model, 'no_ahli_abim')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                <label>Nama Waris</label>
                                 <?= $form->field($model, 'nama_waris')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                <label>Hubungan Waris</label>
                                 <?= $form->field($model, 'hubungan_waris')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                <label>No Telefon Waris</label>
                                 <?= $form->field($model, 'no_tel_waris')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                <label>Kelulusan Tertinggi</label>
                                 <?= $form->field($model, 'kelulusan_tertinggi')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                <label>Peratus Kwsp</label>
                                 <?= $form->field($model, 'peratus_kwsp')->dropDownList(
                                        [
                                            '8'=>'8',
                                            '11'=>'11',
                                        ], 
                                        [
                                            'prompt'=>'Sila Pilih'
                                        ]
                                    )->label(false)
                                ?>
                             </div>
                             <div class="form-group col-md-6">
                                <label>Skim Gaji Awal</label>
                                 <?= $form->field($model, 'skim_gaji_awal')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                         </div>
                    </form>
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>

                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success mb-1']) ?>

                        <?= Html::a('Batal',['index'],['class'=>'btn btn-danger mb-1']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>    
    </div>
</div>
