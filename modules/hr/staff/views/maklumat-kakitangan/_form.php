<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\LookupJawatan;
use app\models\LookupPusatPengajian;

/* @var $this yii\web\View */
/* @var $model app\modules\hr\staff\models\MaklumatKakitangan */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Kakitangan Baru';

$jawatan = ArrayHelper::map(LookupJawatan::find()->orderBy(['jawatan'=>SORT_ASC])->asArray()->all(),'id','jawatan');
$tahfiz = ArrayHelper::map(LookupPusatPengajian::find()->orderBy(['pusat_pengajian'=>SORT_ASC])->asArray()->all(),'id','pusat_pengajian'); //retrieve data for dropdown

$getimage = Yii::$app->request->baseUrl.'/image/no-image.png';

$script = <<< JS
$(document).ready(function(){
    if($('#staff').hasClass('maklumat-kakitangan/create')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".inputFile").change(function () {
        readURL(this);
        $('#cancel').show();
    });
    $('#cancel').click(function(e)
    {
       $('.inputFile').val("");
       $('#image_upload_preview').attr("src","$getimage");
       $('#cancel').hide();
       
   });
});
JS;
$this->registerJs($script);
?>
<style type="text/css">
.custom-files-input {
    display: inline-block !important;
    position: relative !important;
    color: #533e00 !important;
}
.custom-files-input input {
    visibility: hidden;
    width: 150px;
    margin: 6px;
}
.custom-files-input:before {
   /* content: 'Choose File';*/
    display: block;
    /*background: -webkit-linear-gradient( -180deg, #ffdc73, #febf01);
    background: -o-linear-gradient( -180deg, #ffdc73, #febf01);
    background: -moz-linear-gradient( -180deg, #ffdc73, #febf01);
    background: linear-gradient( -180deg, #ffdc73, #febf01);*/
    border: 3px solid #337ab7;
    border-radius: 2px;
    padding: 5px 0px;
    outline: none;
    white-space: nowrap;
    cursor: pointer;
    text-shadow: 1px 1px rgba(255,255,255,0.7);
    font-weight: bold;
    text-align: center;
    font-size: 10pt;
    position: absolute;
    left: 0;
    right: 0;

}
.custom-files-input:hover:before {
    border-color: black;
}
.custom-files-input:active:before {
    background: #337ab7;
}
.file-blue:before {
    content: 'Muat Naik Gambar';
    background-color: #337ab7;
    /*background: -webkit-linear-gradient( -180deg, #99dff5, #02b0e6);
    background: -o-linear-gradient( -180deg, #99dff5, #02b0e6);
    background: -moz-linear-gradient( -180deg, #99dff5, #02b0e6);
    background: linear-gradient( -180deg, #99dff5, #02b0e6);*/
    border-color: #337ab7;
    color: #FFF;
    text-shadow: 1px 1px rgba(000,000,000,0.5);
}
.file-blue:hover:before {
    border-color: #2e6da4 !important;
    background-color: #335CD6 !important;
}
.file-blue:active:before {
    background: #337ab7;
    background-color: #335CD6;
}

.change-file-input {
    display: inline-block;
    position: relative;
    color: #533e00;
}
.change-file-input input {
    visibility: hidden;
    width: 100px
}
.change-file-input:before {
   /* content: 'Choose File';*/
    display: block;
    /*background: -webkit-linear-gradient( -180deg, #ffdc73, #febf01);
    background: -o-linear-gradient( -180deg, #ffdc73, #febf01);
    background: -moz-linear-gradient( -180deg, #ffdc73, #febf01);
    background: linear-gradient( -180deg, #ffdc73, #febf01);*/
    border: 3px solid #337ab7;
    border-radius: 2px;
    padding: 5px 0px;
    outline: none;
    white-space: nowrap;
    cursor: pointer;
    text-shadow: 1px 1px rgba(255,255,255,0.7);
    font-weight: bold;
    text-align: center;
    font-size: 10pt;
    position: absolute;
    left: 0;
    right: 0;
}
.change-file-input:hover:before {
    border-color: black;
}
.change-file-input:active:before {
    background: #337ab7;
}
.change-blue:before {
    content: 'Tukar Gambar';
    background-color: #337ab7;
    /*background: -webkit-linear-gradient( -180deg, #99dff5, #02b0e6);
    background: -o-linear-gradient( -180deg, #99dff5, #02b0e6);
    background: -moz-linear-gradient( -180deg, #99dff5, #02b0e6);
    background: linear-gradient( -180deg, #99dff5, #02b0e6);*/
    border-color: #337ab7;
    color: #FFF;
    text-shadow: 1px 1px rgba(000,000,000,0.5);
}
.change-blue:hover:before {
    border-color: #2e6da4;
    background-color: #335CD6;
}
.change-blue:active:before {
    background: #337ab7;
    background-color: #335CD6;
}

</style>
<span id="staff" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>

<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
                <div class="">
                    <h5 class="mb-4">Maklumat Kakitangan</h5>
                    <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>
                    <form>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Rujukan Tawaran</label>
                                 <?= $form->field($model, 'rujukan_tawaran')->textInput(['maxlength' => true,'class'=>'form-control','style'=>"text-transform: uppercase;",'autocomplete'=>'off'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Nombor Pekerja <span class="font-red">**</span></label>
                                 <?= $form->field($model, 'no_pekerja')->textInput(['maxlength' => true,'class'=>'form-control','placeholder'=>'Cth : TSB000','style'=>"text-transform: uppercase;",'autocomplete'=>'off'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Nama Kakitangan <span class="font-red">**</span></label>
                                 <?= $form->field($model, 'nama')->textInput(['maxlength' => true,'class'=>'form-control','placeholder'=>'Cth : Ahmad Bin Abdullah','style'=>"text-transform: uppercase;",'autocomplete'=>'off'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Nombor Kad Pengenalan <span class="font-red">**</span></label>
                                 <?= $form->field($model, 'no_kp')->textInput(['maxlength' => true,'class'=>'form-control','placeholder'=>'Cth : 123456789012 (tanpa " - ")','autocomplete'=>'off'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-12">
                                <label>Alamat Tetap</label>
                                 <?= $form->field($model, 'alamat_tetap')->textArea(['class'=>'form-control','rows'=>5])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-12">
                                <label>Alamat Surat Menyurat</label>
                                 <?= $form->field($model, 'alamat_surat_menyurat')->textArea(['class'=>'form-control','rows'=>5])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-12">
                                <label>Alamat Tempat Kerja</label>
                                 <?= $form->field($model, 'alamat_tempat_kerja')->textArea(['class'=>'form-control','rows'=>5])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                <label>Tempat Lahir</label>
                                <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Warganegara <span class="font-red">**</span></label>
                                 <?= $form->field($model, 'warganegara')->dropDownList(
                                    [
                                        'Warganegara'=>'Warganegara',
                                        'Bukan Warganegara'=>'Bukan Warganegara',
                                    ], 
                                    [
                                        'prompt'=>'Sila Pilih'
                                    ]
                                )->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Tarikh Mula Kerja</label>
                                 <?= $form->field($model, 'tarikh_mula_kerja')->textInput(['maxlength' => true,'class'=>'form-control datepicker','data-date-format'=>'yyyy-mm-dd','autocomplete'=>'off'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Tarikh Lahir</label>
                                 <?= $form->field($model, 'tarikh_lahir')->textInput(['maxlength' => true,'class'=>'form-control datepicker','data-date-format'=>'yyyy-mm-dd','autocomplete'=>'off'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Jawatan <span class="font-red">**</span></label>
                                 <?= $form->field($model, 'jawatan_sekarang')->dropDownList($jawatan, 
                                    [
                                        'prompt'=>'Sila Pilih'
                                    ]
                                    )->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Tahfiz <span class="font-red">**</span></label>
                                 <?= $form->field($model, 'tahfiz')->dropDownList($tahfiz, 
                                        [
                                            'prompt'=>'Sila Pilih'
                                        ]
                                    )->label(false)
                                ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Telefon Rumah</label>
                                 <?= $form->field($model, 'no_tel_rumah')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false)?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Telefon Bimbit</label>
                                 <?= $form->field($model, 'no_tel_bimbit')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Tinggi</label>
                                 <?= $form->field($model, 'tinggi_cm')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false)?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Berat</label>
                                 <?= $form->field($model, 'berat_kg')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Tahap Kesihatan</label>
                                 <?= $form->field($model, 'tahap_kesihatan')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false)?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Pengesahan Kesihatan</label>
                                 <?= $form->field($model, 'pengesahan_kesihatan')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>Kecacatan</label>
                                 <?= $form->field($model, 'kecacatan')->dropDownList(
                                        [
                                            'Ya'=>'Ya',
                                            'Tidak'=>'Tidak',
                                        ], 
                                        [
                                            'prompt'=>'Sila Pilih'
                                        ]
                                    )->label(false)
                                ?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Nyatakan Kecatatan</label>
                                 <?= $form->field($model, 'nyatakan_kecacatan')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                <label>Nama Kata Usrah</label>
                                 <?= $form->field($model, 'nama_ketua_usrah')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                <label>Kumpulan Usrah</label>
                                 <?= $form->field($model, 'kumpulan_usrah')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                <label>Unit Usrah</label>
                                 <?= $form->field($model, 'unit_usrah')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                <label>No Ahli Abim</label>
                                 <?= $form->field($model, 'no_ahli_abim')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                <label>Nama Waris</label>
                                 <?= $form->field($model, 'nama_waris')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                <label>Hubungan Waris</label>
                                 <?= $form->field($model, 'hubungan_waris')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                <label>No Telefon Waris</label>
                                 <?= $form->field($model, 'no_tel_waris')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                <label>Kelulusan Tertinggi</label>
                                 <?= $form->field($model, 'kelulusan_tertinggi')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                <label>Peratus Kwsp</label>
                                 <?= $form->field($model, 'peratus_kwsp')->dropDownList(
                                        [
                                            '8'=>'8',
                                            '11'=>'11',
                                        ], 
                                        [
                                            'prompt'=>'Sila Pilih'
                                        ]
                                    )->label(false)
                                ?>
                             </div>
                             <div class="form-group col-md-6">
                                <label>Skim Gaji Awal</label>
                                 <?= $form->field($model, 'skim_gaji_awal')->textInput(['maxlength' => true,'class'=>'form-control'])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                            <h5 class="mb-4">Anda di minta untuk memuat naik gambar kakitangan</h5>
                             <div class="form-group col-md-12">
                                <center>
                                    <img id="image_upload_preview" src="<?php echo Yii::$app->request->baseUrl; ?>/image/no-image.png?>" alt="your image" height='300' width='300' />
                                </center>
                             </div>
                             <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label id="muat" class="custom-files-input file-blue">
                                        <?= $form->field($model, 'file')->fileInput(['class'=>"inputFile"])->label(false)?>
                                        </label>
                                        <a href="javascript:;" id="cancel" class="btn btn-danger mb-1" style="display:none;"> Buang </a>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </form>
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>

                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success mb-1']) ?>

                        <?= Html::a('Batal',['index'],['class'=>'btn btn-danger mb-1']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>    
    </div>
</div>
