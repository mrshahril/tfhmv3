<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\hr\staff\models\MaklumatKakitangan */

$this->title = 'Maklumat Terperinci';

$getimage = Yii::$app->request->baseUrl.'/image/no-image.png';

$script = <<< JS
$(document).ready(function(){
    if($('#staff').hasClass('maklumat-kakitangan/view')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".inputFile").change(function () {
        readURL(this);
        $('#cancel').show();
    });
    $('#cancel').click(function(e)
    {
       $('.inputFile').val("");
       $('#image_upload_preview').attr("src","$getimage");
       $('#cancel').hide();
       
   });

});
JS;
$this->registerJs($script);
?>
<style type="text/css">
    .list-thumbnail{
        height: 195px !important;
    }

    .custom-files-input {
    display: inline-block !important;
    position: relative !important;
    color: #533e00 !important;
    }
    .custom-files-input input {
        visibility: hidden;
        width: 150px;
        margin: 6px;
    }
    .custom-files-input:before {
       /* content: 'Choose File';*/
        display: block;
        /*background: -webkit-linear-gradient( -180deg, #ffdc73, #febf01);
        background: -o-linear-gradient( -180deg, #ffdc73, #febf01);
        background: -moz-linear-gradient( -180deg, #ffdc73, #febf01);
        background: linear-gradient( -180deg, #ffdc73, #febf01);*/
        border: 3px solid #337ab7;
        border-radius: 2px;
        padding: 5px 0px;
        outline: none;
        white-space: nowrap;
        cursor: pointer;
        text-shadow: 1px 1px rgba(255,255,255,0.7);
        font-weight: bold;
        text-align: center;
        font-size: 10pt;
        position: absolute;
        left: 0;
        right: 0;

    }
    .custom-files-input:hover:before {
        border-color: black;
    }
    .custom-files-input:active:before {
        background: #337ab7;
    }
    .file-blue:before {
        content: 'Muat Naik Gambar';
        background-color: #337ab7;
        /*background: -webkit-linear-gradient( -180deg, #99dff5, #02b0e6);
        background: -o-linear-gradient( -180deg, #99dff5, #02b0e6);
        background: -moz-linear-gradient( -180deg, #99dff5, #02b0e6);
        background: linear-gradient( -180deg, #99dff5, #02b0e6);*/
        border-color: #337ab7;
        color: #FFF;
        text-shadow: 1px 1px rgba(000,000,000,0.5);
    }
    .file-blue:hover:before {
        border-color: #2e6da4 !important;
        background-color: #335CD6 !important;
    }
    .file-blue:active:before {
        background: #337ab7;
        background-color: #335CD6;
    }

    .change-file-input {
        display: inline-block;
        position: relative;
        color: #533e00;
    }
    .change-file-input input {
        visibility: hidden;
        width: 100px
    }
    .change-file-input:before {
       /* content: 'Choose File';*/
        display: block;
        /*background: -webkit-linear-gradient( -180deg, #ffdc73, #febf01);
        background: -o-linear-gradient( -180deg, #ffdc73, #febf01);
        background: -moz-linear-gradient( -180deg, #ffdc73, #febf01);
        background: linear-gradient( -180deg, #ffdc73, #febf01);*/
        border: 3px solid #337ab7;
        border-radius: 2px;
        padding: 5px 0px;
        outline: none;
        white-space: nowrap;
        cursor: pointer;
        text-shadow: 1px 1px rgba(255,255,255,0.7);
        font-weight: bold;
        text-align: center;
        font-size: 10pt;
        position: absolute;
        left: 0;
        right: 0;
    }
    .change-file-input:hover:before {
        border-color: black;
    }
    .change-file-input:active:before {
        background: #337ab7;
    }
    .change-blue:before {
        content: 'Tukar Gambar';
        background-color: #337ab7;
        /*background: -webkit-linear-gradient( -180deg, #99dff5, #02b0e6);
        background: -o-linear-gradient( -180deg, #99dff5, #02b0e6);
        background: -moz-linear-gradient( -180deg, #99dff5, #02b0e6);
        background: linear-gradient( -180deg, #99dff5, #02b0e6);*/
        border-color: #337ab7;
        color: #FFF;
        text-shadow: 1px 1px rgba(000,000,000,0.5);
    }
    .change-blue:hover:before {
        border-color: #2e6da4;
        background-color: #335CD6;
    }
    .change-blue:active:before {
        background: #337ab7;
        background-color: #335CD6;
    }
</style>
<span id="staff" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>

<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?php  
                    if (base64_decode(Yii::$app->request->get('resign')) == 'y') {
                        echo Html::a('Kembali',['resign'],['class'=>'btn btn-outline-primary btn-lg']);
                        
                    }
                    else{
                        echo Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg']);
                    }
                ?>
                
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-6 col-lg-4 col-12 mb-4">
        <div class="card ">
            <div class="card-body">
                <div class="text-center">
                    <img alt="Profile" src="<?= Yii::getAlias('@web').'/uploads/'. $model->foto ?>" class="img-thumbnail border-0 rounded-circle mb-4 list-thumbnail">
                    <p class="list-item-heading mb-1"><?= $model->nama ?></p>
                    <p class="mb-4 text-muted text-small"><?= $model->jawatan->jawatan ?></p>
                    <?php 
                        if (base64_decode(Yii::$app->request->get('resign')) == 'y') {
                            echo Html::a('Kemaskini',['edit','id'=>base64_encode($model->id_staf),'resign'=>base64_encode('y')],['class'=>'btn btn-sm btn-outline-primary']);
                        }
                        else{
                            echo Html::a('Kemaskini',['edit','id'=>base64_encode($model->id_staf)],['class'=>'btn btn-sm btn-outline-primary']);
                        }
                    ?>
                    <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-backdrop="static"
                                data-target="#exampleModalRight">Tukar Gambar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-lg-8 col-12 mb-4">
        <div class="card d-flex flex-row mb-4">
            <div class="card-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [

                        'statuskerja.status_pekerjaan',
                        'nama',
                        'no_kp',
                        'tarikh_mula_kerja',
                        'jawatan.jawatan',
                        'no_pekerja',
                        'nama_tahfiz.pusat_pengajian',
                        'alamat_tempat_kerja:ntext',
                        'warganegara',
                        'tarikh_lahir',
                        'tempat_lahir:ntext',
                        [
                            'label' => 'Alamat Surat Menyurat',
                           // 'format' => ['raw'],
                            'value' => strip_tags($model->alamat_surat_menyurat),
                        ],
                        'no_tel_rumah',
                        'no_tel_bimbit',
                        'tahap_kesihatan',
                        'pengesahan_kesihatan',
                        'kecacatan',
                        'nyatakan_kecacatan',
                        'tinggi_cm',
                        'berat_kg',
                        'kumpulan_usrah',
                        'nama_ketua_usrah',
                        'unit_usrah',
                        'no_ahli_abim',
                        'rujukan_tawaran',
                        'nama_waris',
                        'hubungan_waris',
                        'no_tel_waris',
                        'kelulusan_tertinggi',
                        'skim_gaji_awal',
                        'peratus_kwsp',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-right" id="exampleModalRight" tabindex="-1" role="dialog" aria-labelledby="exampleModalRight" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tukar Gambar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php 
                if (base64_decode(Yii::$app->request->get('resign')) == 'y') {
                    $form = ActiveForm::begin(
                        [
                            'action' => ['updateimg','id'=>base64_encode($model->id_staf),'resign'=>Yii::$app->request->get('resign')],
                            'method' => 'post',
                        ]
                    );
                }
                else{
                    $form = ActiveForm::begin(
                        [
                            'action' => ['updateimg','id'=>base64_encode($model->id_staf)],
                            'method' => 'post',
                        ]
                    );
                }
             ?>
            <div class="modal-body">
                    <div class="form-group">
                        <center>
                            <img id="image_upload_preview" src="<?=Yii::$app->request->BaseUrl.'/uploads/'.$model->foto ?>" alt="your image" height='300' width='300' class="img-thumbnail border-0 rounded-circle mb-4 list-thumbnail" />
                        </center>
                     </div>
                     <div class="form-group">
                        <div class="row">
                            <center>
                                <label id="muat" class="custom-files-input file-blue">
                                <?= $form->field($model, 'file')->fileInput(['class'=>"inputFile"])->label(false)?>
                                </label>
                                <a href="javascript:;" id="cancel" class="btn btn-danger mb-1" style="display:none;"> Buang </a>
                            </center>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
                <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary mb-1']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>