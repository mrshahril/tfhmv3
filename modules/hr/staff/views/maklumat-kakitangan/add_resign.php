<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\LookupStatusPekerjaan;

$this->title = 'Kemaskini Status Pekerjaan';

$status = ArrayHelper::map(LookupStatusPekerjaan::find()->asArray()->all(),'id','status_pekerjaan'); //retrieve data for dropdown

$script = <<< JS
$(document).ready(function(){
	// $(".date-picker").datepicker(); //date
	$("body").delegate(".datepicker", "focusin", function(){
        $(this).datepicker();
    });
});
JS;
$this->registerJs($script);
?>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h5><?= Html::encode($this->title) ?></h5>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
	<div class="col-md-12">
        <div class="box box-danger">
        	<?php $form = ActiveForm::begin([
                    'id' => 'form-pr',
                    'enableClientValidation' => true,
                    'enableAjaxValidation' => false,
                ]); ?>
        	<div class="form-row">
                <div class="form-group col-md-12">
	                <label>Nama Kakitangan</label>
	                <?= $form->field($model, 'nama')->textInput(['maxlength' => true,'class'=>'form-control','disabled'=>''])->label(false) ?>
	            </div>
        	</div>
        	<div class="form-row">
                <div class="form-group col-md-12">
	                <label>Status Pekerjaan</label>
	                <?= $form->field($model, 'status_pekerjaan')->dropDownList($status, 
                            [
                                'prompt'=>'Sila Pilih'
                            ]
                        )->label(false)
                    ?>
        		</div>
        	</div>
        	<div class="form-row">
                <div class="form-group col-md-12">
	                <label>Tarikh Berhenti Kerja</label>
	                <?= $form->field($model, 'tarikh_resign')->textInput(['maxlength' => true,'class'=>'form-control datepicker','data-date-format'=>'yyyy-mm-dd'])->label(false) ?>
        		</div>
        	</div>
        	<div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary mb-1']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
	</div>
</div>