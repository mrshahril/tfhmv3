<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;

use app\models\LookupCuti;
use app\modules\hr\staff\models\MaklumatKakitangan;

$nama = ArrayHelper::map(MaklumatKakitangan::find()->where(['status_pekerjaan'=>1])->asArray()->orderBy(['nama'=>SORT_ASC])->all(),'id_staf','nama'); //retrieve data for dropdown
$cuti = ArrayHelper::map(LookupCuti::find()->asArray()->orderBy(['id'=>SORT_ASC])->all(),'id','jenis_cuti');

$this->title = 'Tambah Maklumat Cuti';

$script = <<< JS
$(document).ready(function(){
    if($('#staff').hasClass('maklumat-cuti/create')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }
});
JS;
$this->registerJs($script);
?>

<span id="staff" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
                <div class="card-body">
                    <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>
                    <form>
                        <div class="form-row">
                             <div class="form-group col-md-6">
                                <label>Nama Kakitangan <span class="font-red">**</span></label>
                                <?= $form->field($model, 'id_staff')->dropDownList($nama, 
                                    [
                                        'prompt'=>'Sila Pilih',
                                        'class'=>'select2-single'
                                    ]
                                    )->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                <label>Jenis Cuti <span class="font-red">**</span></label>
                                <?= $form->field($model, 'jenis_cuti')->dropDownList($cuti, 
                                    [
                                        'prompt'=>'Sila Pilih',
                                        'class'=>'select2-single'
                                    ]
                                    )->label(false) ?>
                             </div>
                         </div>
                        <div class="form-row">
                             <div class="form-group col-md-12">
                                <label>Nyatakan Sebab Bercuti</label>
                                <?= $form->field($model, 'sebab_cuti')->textArea(['class'=>'form-control','rows'=>5])->label(false) ?>
                             </div>
                        </div>
                        <div class="form-row">
                             <div class="form-group col-md-4">
                                <label>Tarikh Mula <span class="font-red">**</span></label>
                                <?= $form->field($model, 'tarikh_mula')->textInput(['maxlength' => true,'class'=>'form-control datepicker','data-date-format'=>'yyyy-mm-dd','autocomplete'=>'off'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-4">
                                <label>Tarikh Akhir <span class="font-red">**</span></label>
                                <?= $form->field($model, 'tarikh_akhir')->textInput(['maxlength' => true,'class'=>'form-control datepicker','data-date-format'=>'yyyy-mm-dd','autocomplete'=>'off'])->label(false) ?>
                             </div>
                             <div class="form-group col-md-4">
                                <label>Tempoh Cuti (Hari) <span class="font-red">**</span></label>
                                <?= $form->field($model, 'bilangan_cuti')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                             </div>
                        </div>
                    </form>
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>

                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success mb-1']) ?>

                        <?= Html::a('Batal',['index'],['class'=>'btn btn-danger mb-1']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
