<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\hr\staff\models\MaklumatCuti */

$this->title = 'Create Maklumat Cuti';
$this->params['breadcrumbs'][] = ['label' => 'Maklumat Cutis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maklumat-cuti-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
