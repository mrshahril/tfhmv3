<?php

namespace app\modules\hr\pelajar;

/**
 * pelajar module definition class
 */
class Pelajar extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\hr\pelajar\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
