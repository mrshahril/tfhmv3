<?php

namespace app\modules\hr\pelajar\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

use app\modules\hr\pelajar\models\SubsidiPelajar;
use app\modules\hr\pelajar\models\MaklumatPelajarPenjaga;
use app\modules\hr\pelajar\models\SubsidiPelajarSearch;

/**
 * SubsidiPelajarController implements the CRUD actions for SubsidiPelajar model.
 */
class SubsidiPelajarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SubsidiPelajar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SubsidiPelajarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['tahun'=>date('Y')]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SubsidiPelajar model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {   
        $model = SubsidiPelajar::find()
                ->where(['no_siri'=>base64_decode($id)])
                ->all();
        $model2 = SubsidiPelajar::find()
                ->where(['no_siri'=>base64_decode($id)])
                ->one();

        return $this->render('view', [
            'model' => $model,
            'model2' => $model2,
        ]);
    }

    /**
     * Creates a new SubsidiPelajar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SubsidiPelajar();

        if ($model->load(Yii::$app->request->post()) ) {
            $six_digit_random_number = mt_rand(1000, 9999); //generate random number for no siri permohonan subsidi

            $kira = count($_POST['idpelajar']);

            for ($i=0; $i < $kira; $i++) { 
                $model = new SubsidiPelajar;

                $model->id_pelajar = $_POST['idpelajar'][$i];
                $model->tahun = date('Y',time());
                $model->date_add = date("Y-m-d",time());
                $model->enter_by = Yii::$app->user->identity->id;
                $model->date_apply = date('Y-m-d',time());
                $model->status = "Lulus"; //temporary . key in terus lulus . no need approval .
                $model->no_siri = "TSB-".$six_digit_random_number;
                $model->category_subsidi = $_POST['SubsidiPelajar']['category_subsidi'];
                $model->peruntukan = $_POST['SubsidiPelajar']['peruntukan'];
                $model->tahap_semasa= $_POST['tahapsemasa'][$i];

                $model->bilangan_tanggungan = $_POST['SubsidiPelajar']['bilangan_tanggungan'];

                if ($model->save()) {
                    $namabapa = $_POST['namabapa'];
                    $nokpbapa = $_POST['nokpbapa'];
                    $notelbapa = $_POST['notelbapa'];
                    $nohpbapa = $_POST['nohpbapa'];
                    $pekerjaanbapa = $_POST['pekerjaanbapa'];
                    $alamatmajikanbapa = $_POST['alamatmajikanbapa'];
                    $gajibapa = $_POST['gajibapa'];

                    $namaibu = $_POST['namaibu'];
                    $nokpibu = $_POST['nokpibu'];
                    $notelibu = $_POST['notelibu'];
                    $nohpibu = $_POST['nohpibu'];
                    $pekerjaanibu = $_POST['pekerjaanibu'];
                    $alamatmajikanibu = $_POST['alamatmajikanibu'];
                    $gajiibu = $_POST['gajiibu'];

                    $model2 = MaklumatPelajarPenjaga::find()->where(['id'=>$model->id_pelajar])->one();

                    $model2->nama_bapa = $namabapa;
                    $model2->no_kad_pengenalan_bapa = $nokpbapa;
                    $model2->no_tel_bapa = $notelbapa;
                    $model2->no_hp_bapa = $nohpbapa;
                    $model2->pekerjaan_bapa_penjaga = $pekerjaanbapa;
                    $model2->alamat_majikan_bapa_penjaga = $alamatmajikanbapa;
                    $model2->gaji_bapa = $gajibapa;

                    $model2->nama_ibu = $namaibu;
                    $model2->no_kad_pengenalan_ibu = $nokpibu;
                    $model2->no_tel_ibu = $notelibu;
                    $model2->no_hp_ibu = $nohpibu;
                    $model2->pekerjaan_ibu = $pekerjaanibu;
                    $model2->alamat_majikan_ibu = $alamatmajikanibu;
                    $model2->gaji_ibu = $gajiibu;
                    $model2->tahap_semasa = $model->tahap_semasa;

                    $model2->save();
                }
            }
            Yii::$app->session->setFlash('savedone','Subsidi pelajar telah berjaya disimpan.');

            return $this->redirect(['view', 'id' => base64_encode($model->no_siri)]);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    public function actionStdnt($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
        $connection = \Yii::$app->db;
        $sql = $connection->createCommand('SELECT id,nama_pelajar AS text FROM maklumat_pelajar_penjaga WHERE nama_pelajar LIKE "%'.$q.'%" ');

       // $query = "SELECT * FROM `post` where `title` LIKE 'foo%' ";
        $model = $sql->queryAll();
        $out['results'] = array_values($model);

        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => MaklumatPelajarPenjaga::find($id)->nama_pelajar];
        }
        return $out;
    }

    public function actionCheck($id){
        $model = SubsidiPelajar::find()
                ->where(['id_pelajar'=>$id])
                ->andWhere(['tahun'=>date('Y',time())])
                ->one();

        if (isset($model)) {
            return 'yes';
        }
        else{
            return 'no';
        }
    }
    /**
     * Updates an existing SubsidiPelajar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionAddpelajar($id)
    {
        $model = new SubsidiPelajar();
        $model2 = SubsidiPelajar::find()
                ->where(['no_siri'=>base64_decode($id)])
                ->one();

        if ($model->load(Yii::$app->request->post()) ) {
            $model3 = MaklumatPelajarPenjaga::find()
                    ->where(['id'=>$_POST['SubsidiPelajar']['id_pelajar']])
                    ->one();

            $model->tahun = date('Y',time());
            $model->date_add = date("Y-m-d",time());
            $model->enter_by = Yii::$app->user->identity->id;
            $model->date_apply = date('Y-m-d',time());
            $model->status = "Lulus"; //temporary use
            $model->no_siri = base64_decode($id);
            $model->bilangan_tanggungan = $model2->bilangan_tanggungan;
            $model->category_subsidi = $model2->category_subsidi;
            $model->peruntukan = $model2->peruntukan;
            if ($model->save()) {
                $model3->tahap_semasa = $model->tahap_semasa;
                $model3->save();
                Yii::$app->session->setFlash('savedone','Subsidi pelajar telah berjaya disimpan.');

                return $this->redirect(['view', 'id' => $id]);
            }
            else{
            }
        }
        return $this->renderAjax('addpelajar',[
            'model'=>$model,
            'model2'=>$model2,
        ]);
    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SubsidiPelajar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = SubsidiPelajar::findOne(base64_decode($id));
        $nosiri = $model->no_siri;

        if ($model->delete()) {
            $model2 = SubsidiPelajar::find()->where(['no_siri'=>$nosiri])->exists();

            if ($model2 == true) {
               Yii::$app->session->setFlash('savedone','Hapus pelajar dari senarai subsidi telah berjaya.');

                return $this->redirect(['view','id'=>base64_encode($nosiri)]);
            }
            else{
                Yii::$app->session->setFlash('savedone','Hapus pelajar dari senarai subsidi telah berjaya.');

                return $this->redirect(['index']);
            }
            
        }
    }

    public function actionDeleteall($id)
    {
        SubsidiPelajar::deleteAll('no_siri = :no_siri',[':no_siri'=>base64_decode($id)]);
        Yii::$app->session->setFlash('savedone','Hapus pelajar dari senarai subsidi telah berjaya.');
        return $this->redirect(['index']);
    }

    public function actionEdit($id)
    {
        $model = SubsidiPelajar::find()
                ->where(['no_siri'=>base64_decode($id)])
                ->one();
        $model2 = SubsidiPelajar::find()
                ->where(['no_siri'=>base64_decode($id)])
                ->all();

        if ($model->load(Yii::$app->request->post()) ) {
            foreach ($model2 as $key => $value) {
                $model3 = SubsidiPelajar::findOne($value['id']);

                $model3->category_subsidi = $model->category_subsidi;
                $model3->bilangan_tanggungan = $model->bilangan_tanggungan;
                $model3->peruntukan = $model->peruntukan;
                $model3->update_by = Yii::$app->user->identity->id;
                $model3->date_update = date('Y-m-d h:i:s A');
                $model3->save();
            }
            Yii::$app->session->setFlash('savedone','Subsidi pelajar telah berjaya dikemaskini.');

            return $this->redirect(['view', 'id' => $id]);
        }
        return $this->renderAjax('edit',[
            'model'=>$model,
        ]);
    }
    /**
     * Finds the SubsidiPelajar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SubsidiPelajar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SubsidiPelajar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionStdntdetail($id)
    {
        $model = MaklumatPelajarPenjaga::findOne($id);

        return Json::encode($model);
    }
}
