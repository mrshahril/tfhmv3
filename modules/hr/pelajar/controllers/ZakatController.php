<?php

namespace app\modules\hr\pelajar\controllers;

use Yii;
use app\modules\hr\pelajar\models\Zakat;
use app\modules\hr\pelajar\models\ZakatSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ZakatController implements the CRUD actions for Zakat model.
 */
class ZakatController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Zakat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ZakatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Zakat model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel(base64_decode($id)),
        ]);
    }

    /**
     * Creates a new Zakat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Zakat();

        if ($model->load(Yii::$app->request->post()) ) {
            $model->date_added = date('Y-m-d h:i:s A');
            $model->enter_by = Yii::$app->user->identity->id;
            $model->tahun = $_POST['Zakat']['tahun'];

            if ($model->save()) {
                Yii::$app->session->setFlash('savedone','Maklumat zakat telah berjaya disimpan');
                return $this->redirect(['view','id'=>base64_encode($model->id)]);
            }
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Zakat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel(base64_decode($id));

        if ($model->load(Yii::$app->request->post()) ) {
            $model->date_update = date('Y-m-d h:i:s A');
            $model->update_by = Yii::$app->user->identity->id;
            $model->tahun = $_POST['Zakat']['tahun'];

            if ($model->save()) {
                Yii::$app->session->setFlash('savedone','Maklumat zakat telah berjaya dikemaskini');
                return $this->redirect(['view','id'=>base64_encode($model->id)]);
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Zakat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel(base64_decode($id))->delete();
        Yii::$app->session->setFlash('savedone','Maklumat zakat telah berjaya dihapus');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Zakat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Zakat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Zakat::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
