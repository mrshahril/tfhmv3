<?php

namespace app\modules\hr\pelajar\controllers;

use Yii;
use app\modules\hr\pelajar\models\MaklumatPelajarPenjaga;
use app\modules\hr\pelajar\models\PusatPendaftaran;
use app\modules\hr\pelajar\models\MaklumatPilihanPusatPengajian;
use app\modules\hr\pelajar\models\MaklumatPelajarPenjagaSearch;
use app\modules\kewangan\models\YuranPendaftaran;
use app\modules\kewangan\models\YuranPendaftaranDetail;
use app\modules\kewangan\models\YuranPendaftaranSearch;
use app\modules\kewangan\models\YuranPaymentDetails;
use app\models\LookupYurandaftar;

use app\models\LookupDistrict;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * MaklumatPelajarPenjagaController implements the CRUD actions for MaklumatPelajarPenjaga model.
 */
class MaklumatPelajarPenjagaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MaklumatPelajarPenjaga models.
     * @return mixed
     */

    /** ALL **/
    public function actionAll()
    {
        $searchModel = new MaklumatPelajarPenjagaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andWhere(['maklumat_pelajar_penjaga.status_deleted'=>2]);

        return $this->render('senarai_keseluruhan',[
            'searchModel'=>$searchModel,
            'dataProvider'=>$dataProvider,
        ]);
    }
    /** END ALL **/
    public function actionIndex()
    {
        $searchModel = new MaklumatPelajarPenjagaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $dataProvider->query->andWhere(['maklumat_pelajar_penjaga.pusat_pengajian_id'=>Yii::$app->user->identity->tahfiz]);
        $dataProvider->query->andWhere(['maklumat_pelajar_penjaga.alumni'=>0]);
        $dataProvider->query->andWhere(['maklumat_pelajar_penjaga.status_deleted'=>2]);

        return $this->render('aktif/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MaklumatPelajarPenjaga model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('aktif/view', [
            'model' => $this->findModel(base64_decode($id)),
        ]);
    }

    /**
     * Creates a new MaklumatPelajarPenjaga model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MaklumatPelajarPenjaga();
        $model3 = new PusatPendaftaran();
        $model2 = new YuranPendaftaran();
        $model4 = new YuranPendaftaranDetail();

        $model->scenario = 'registerday';
        $model2->scenario = 'yuranregisterday';
        
        if($model->load(Yii::$app->request->post()) && $model3->load(Yii::$app->request->post()) && $model2->load(Yii::$app->request->post()) )
        {
            //identify dlu user key in no mykid atau no passport
            if ($_POST['optionidentity'] == 'mykid') {

                $lengnama = str_replace('_', '', $model->no_mykid);
                if(strlen($lengnama) == 14){
                    
                }
                else{
                    Yii::$app->session->setFlash('saveerror','Nombor mykid pelajar perlu mengikut format di tetapkan seperti 123456-78-9101');
                    
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }
            else{
                //do nothing
                // $model->no_mykid = $model->passport;
            }
            
            $model->status="Approved";
            $model->date_create = date('Y-m-d h:i:s A');
            $model->enter_by = Yii::$app->user->identity->id;
            $model->pusat_pendaftaran = $model3->pusat_pendaftaran;
            $model->tahap_semasa = $model->tahun_mula;
            $model->nama_pelajar = strtoupper($_POST["MaklumatPelajarPenjaga"]["nama_pelajar"]);

            if($model->save()){
                //save bill
                $bill = YuranPendaftaran::savebill($model->id,$model2->jenis_pelajar);

                $last_id = Yii::$app->db->getLastInsertID();
                
                $model3->id_pelajar = $last_id;
                $model3->save();

                if ($_POST['optionyuran'] == 'Ya') {
                    Yii::$app->session->setFlash('savedone','Maklumat pelajar telah berjaya disimpan. Maklumat dibawah adalah invois yuran pendaftaran.');
                    return $this->redirect(['/kewangan/yuran-pendaftaran/payyuran','id'=>base64_encode($bill)]);
                }
                else{
                    Yii::$app->session->setFlash('savedone','Maklumat pelajar telah berjaya disimpan');
                    return $this->redirect(['create']);
                }
                
            }
        }
        else{
            return $this->render('aktif/_form', [
                'model' => $model,
                'model3' => $model3,
                'model2'=>$model2,
            ]);
        }        
    }

    public function actionExistdata($id)
    {
        $model = MaklumatPelajarPenjaga::findOne($id);

        $model2 = new YuranPendaftaran();
        $model4 = new YuranPendaftaranDetail();

        $model->scenario = 'registerday_existdata';
        $model2->scenario = 'yuranregisterday';

        if ($model->load(Yii::$app->request->post()) && $model2->load(Yii::$app->request->post())) {

            //identify dlu user key in no mykid atau no passport
            if ($_POST['optionidentity'] == 'mykid') {

                $lengnama = str_replace('_', '', $model->no_mykid);
                if(strlen($lengnama) == 14){
                    #do nothing
                }
                else{
                    Yii::$app->session->setFlash('saveerror','Nombor mykid pelajar perlu mengikut format di tetapkan seperti 123456-78-9101');
                    
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }
            else{
                //do nothing
                // $model->no_mykid = $model->passport;
            }
            
            $model->date_update = date('Y-m-d h:i:s A');
            $model->update_by = Yii::$app->user->identity->id;
            $model->nama_pelajar = strtoupper($_POST["MaklumatPelajarPenjaga"]["nama_pelajar"]);

            if ($model->save()) {
                //save bill
                $bill = YuranPendaftaran::savebill($model->id,$model2->jenis_pelajar);

                if ($_POST['optionyuran'] == 'Ya') {
                    Yii::$app->session->setFlash('savedone','Maklumat pelajar telah berjaya disimpan. Maklumat dibawah adalah invois yuran pendaftaran.');
                    return $this->redirect(['/kewangan/yuran-pendaftaran/payyuran','id'=>base64_encode($bill)]);
                }
                else{
                    Yii::$app->session->setFlash('savedone','Maklumat pelajar telah berjaya disimpan');
                    return $this->redirect(['create']);
                }
            }
        }
        else{
            return $this->render('aktif/existdata',[
                'model'=>$model,
                'model2'=>$model2,
                // 'model4'=>$model4,
            ]);
        }
    }
    /**
     * Updates an existing MaklumatPelajarPenjaga model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = MaklumatPelajarPenjaga::find()
            ->where(['id'=>base64_decode($id)])
            ->one();

        if($model->load(Yii::$app->request->post()) )
        {

            if ($_POST['optionidentity'] == 'mykid') {

                $lengnama = str_replace('_', '', $model->no_mykid);
                if(strlen($lengnama) == 14){
                    
                }
                else{
                    Yii::$app->session->setFlash('saveerror','Nombor mykid pelajar perlu mengikut format di tetapkan seperti 123456-78-9101');
                    
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }
            else{
                //do nothing
            }
            $model->date_update = date('Y-m-d h:i:s A');
            $model->update_by = Yii::$app->user->identity->id;
            // $model->sesi_pengajian = $model2->sessi_pengajian;

            if($model->save()){
               Yii::$app->session->setFlash('savedone','Maklumat pelajar telah berjaya dikemaskini');
                return $this->redirect(['view','id'=>base64_encode($model->id)]);        
            }
        }else {
            return $this->render('aktif/edit', [
                'model' => $model,
            ]);
        }
    }  

    public function actionChange_alumni($id)
    {
        $model = $this->findModel(base64_decode($id));

        $model->alumni = '1';
        $model->save();
        if ($model->save()) {
            Yii::$app->session->setFlash('savedone','Maklumat pelajar telah di pindah ke ruangan alumni');
            return $this->redirect(['index']);   
        }
    }

    /**
     * Deletes an existing MaklumatPelajarPenjaga model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel(base64_decode($id));
        $model2 = MaklumatPilihanPusatPengajian::find()
                ->where(['id_pelajar'=>$model->id])
                ->one();
        $model3 = PusatPendaftaran::find()
                ->where(['id_pelajar'=>$model->id])
                ->one();

        if ($model->delete()) {
            if (isset($model2)) {
                $model2->delete();
            }
            else{

            }
            if (isset($model3)) {
                $model3->delete();
            }
            else{

            } 
            Yii::$app->session->setFlash('savedone','Maklumat pelajar telah berjaya di hapuskan');
            return $this->redirect(['index']);   
        }
        
        
    }

    public function actionDeactivated($id)
    {
        $model = $this->findModel(base64_decode($id));
        
        $model->status_deleted = MaklumatPelajarPenjaga::SOFT_DELETE;
        $model->deleted_at = date('Y-m-d h:i:s A');

        if ($model->save()) {
            Yii::$app->session->setFlash('savedone','Maklumat pelajar telah berjaya di hapuskan');
            return $this->redirect(['index']);   
        }
    }

    public function actionListdaerah($id)
    {
        $countPosts = LookupDistrict::find()
        ->where(['state_id' => $id])
        ->count();
         
        $posts = LookupDistrict::find() 
        ->where(['state_id' => $id])
        ->all();
         
        if($countPosts>0){
            echo "<option value=''>--Sila Pilih Daerah--</option>";
            foreach($posts as $post){
                echo "<option value='".$post->district_id."'>".$post->district."</option>";
            }
        } else {
                echo "<option value=''>Tiada Maklumat</option>";
        }
    }

    public function actionCheckkp($id)
    {
        $model = MaklumatPelajarPenjaga::find()
                ->where(['no_mykid'=>str_replace('-', '', $id)])
                ->one();
        if ($id != '') {
            if (isset($model)) {
                echo "ada";
                $id_p = $model->id;
            }
            else{
                $model1 = MaklumatPelajarPenjaga::find()
                        ->where(['no_mykid'=>str_replace('_', '', $id)])
                        ->one();

                if (isset($model1)) {
                    echo 'ada';
                }
                else{
                    echo 'tak ada';
                }
                
            }
        }
        else{
            echo "No Data";
        }
    }

    public function actionCheckkp_id($id)
    {
        $model = MaklumatPelajarPenjaga::find()
                ->where(['no_mykid'=>str_replace('-', '', $id)])
                ->one();
        if ($id != '') {
            if (isset($model)) {
                $id_p = $model->id;
                echo $id_p;
            }
            else{
                $model1 = MaklumatPelajarPenjaga::find()
                        ->where(['no_mykid'=>str_replace('_', '', $id)])
                        ->one();

                if (isset($model1)) {
                    $id_p = $model1->id;
                    echo $id_p;
                }
                else{
                    echo 'tak ada';
                }
                
            }
        }
        else{
            echo "No Data";
        }
    }
    /**
     * Finds the MaklumatPelajarPenjaga model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MaklumatPelajarPenjaga the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MaklumatPelajarPenjaga::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    /** ALUMNI **/

    public function actionAlumni()
    {
        $searchModel = new MaklumatPelajarPenjagaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $dataProvider->query->andWhere(['maklumat_pelajar_penjaga.pusat_pengajian_id'=>Yii::$app->user->identity->tahfiz]);
        $dataProvider->query->andWhere(['maklumat_pelajar_penjaga.alumni'=>1]);
        $dataProvider->query->andWhere(['maklumat_pelajar_penjaga.status_deleted'=>2]);

        return $this->render('alumni/alumni', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionChange_aktif($id)
    {
        $model = $this->findModel(base64_decode($id));

        $model->alumni = '0';
        $model->save();
        if ($model->save()) {
            Yii::$app->session->setFlash('savedone','Maklumat pelajar telah di pindah ke ruangan pelajar aktif');
            return $this->redirect(['alumni']);   
        }
    }

    public function actionViewalumni($id)
    {
        return $this->render('alumni/viewalumni', [
            'model' => $this->findModel(base64_decode($id)),
        ]);
    }

    public function actionUpdatealumni($id)
    {
        $model = MaklumatPelajarPenjaga::find()
            ->where(['id'=>base64_decode($id)])
            ->one();

        if($model->load(Yii::$app->request->post()) )
        {

            if ($_POST['optionidentity'] == 'mykid') {

                $lengnama = str_replace('_', '', $model->no_mykid);
                if(strlen($lengnama) == 14){
                    
                }
                else{
                    Yii::$app->session->setFlash('saveerror','Nombor mykid pelajar perlu mengikut format di tetapkan seperti 123456-78-9101');
                    
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }
            else{
                //do nothing
            }
            $model->date_update = date('Y-m-d h:i:s A');
            $model->update_by = Yii::$app->user->identity->id;
            // $model->sesi_pengajian = $model2->sessi_pengajian;

            if($model->save()){
               Yii::$app->session->setFlash('savedone','Maklumat pelajar telah berjaya dikemaskini');
                return $this->redirect(['viewalumni','id'=>base64_encode($model->id)]);        
            }
        }else {
            return $this->render('alumni/edit', [
                'model' => $model,
            ]);
        }
    }

    public function actionDeactivatedalumni($id)
    {
        $model = $this->findModel(base64_decode($id));
        
        $model->status_deleted = MaklumatPelajarPenjaga::SOFT_DELETE;
        $model->deleted_at = date('Y-m-d h:i:s A');
        $model->deleted_by = Yii::$app->user->identity->id;

        if ($model->save()) {
            Yii::$app->session->setFlash('savedone','Maklumat pelajar telah berjaya di hapuskan');
            return $this->redirect(['alumni']);   
        }
    }

    /** END ALUMNI **/

    /** PENTADBIRAN **/
    public function actionPending()
    {
        $searchModel = new MaklumatPelajarPenjagaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status'=>'Pending']);

        return $this->render('hq/pending', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdatepending($id)
    {
        $model = $this->findModel(base64_decode($id));
        $model2 = MaklumatPilihanPusatPengajian::find()
                ->where(['id_pelajar'=>$model->id])
                ->one();
        $model3 = PusatPendaftaran::find()
                ->where(['id_pelajar'=>$model->id])
                ->one();

        if($model->load(Yii::$app->request->post()) && $model2->load(Yii::$app->request->post()) && $model3->load(Yii::$app->request->post()) )
        {
            if ($_POST['optionidentity'] == 'mykid') {

                $lengnama = str_replace('_', '', $model->no_mykid);
                if(strlen($lengnama) == 14){
                    
                }
                else{
                    Yii::$app->session->setFlash('saveerror','Nombor mykid pelajar perlu mengikut format di tetapkan seperti 123456-78-9101');
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }
            $model->status="Pending";
            // $model->date_create = date('Y/m/d');
            $model->sesi_pengajian = $model2->sessi_pengajian;
            $model->pusat_pendaftaran = $model3->pusat_pendaftaran;
            $model->update_by = Yii::$app->user->identity->id;
            $model->date_update = date('Y-m-d h:i:s A');

            if($model->save()) {
                
                // $model2->id_pelajar = $last_id;
                // $model3->id_pelajar = $last_id;
                $model3->save();
                $model2->save();
                Yii::$app->session->setFlash('savedone','Maklumat pelajar telah berjaya dikemaskini');
                return $this->redirect(['pending']);
            }            
        } else {
            return $this->render('hq/updatepending', [
                'model' => $model,
                'model2' => $model2,
                'model3'=>$model3,
            ]);
        }
    }

    public function actionStatus($id)
    {
        $model = $this->findModel(base64_decode($id));
        $model2 = MaklumatPilihanPusatPengajian::find()->where(['id_pelajar'=>$model->id])->one();
        $model->scenario = 'status_approval';

        if ($model->load(Yii::$app->request->post()) ) {
            if ($model->save()) {
                Yii::$app->session->setFlash('savedone','Maklumat pelajar telah berjaya disahkan');
                return $this->redirect(['pending']);
            }
        } else {
            return $this->render('hq/status', [
                'model' => $model,
                'model2' => $model2,
            ]);
        }
    }
    /** END PENTADBIRAN **/

    public function actionFilteryuran($id)
    {
        $model = LookupYurandaftar::find()
        ->where(['jenis_pelajar' => $id])
        ->count();

        $model2 = LookupYurandaftar::find()
        ->select('tahap_pelajar')
        ->where(['jenis_pelajar' => $id])
        ->distinct()
        ->all();

        if($model2>0){
            echo "<option value=''>-Sila Pilih-</option>";
            foreach($model2 as $jenis){
                echo "<option value='".$jenis->tahap_pelajar."'>".$jenis->tahap_pelajar."</option>";
            }
        } else {
                echo "<option></option>";
        }
    }
}
