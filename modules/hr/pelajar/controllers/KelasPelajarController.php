<?php

namespace app\modules\hr\pelajar\controllers;

use Yii;

use app\models\LookupKelas;

use app\modules\hr\pelajar\models\KelasPelajar;
use app\modules\hr\pelajar\models\MaklumatPelajarPenjaga;
use app\modules\hr\pelajar\models\Zakat;
use app\modules\hr\pelajar\models\MaklumatPelajarPenjagaSearch;
use app\modules\hr\pelajar\models\KelasPelajarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KelasPelajarController implements the CRUD actions for KelasPelajar model.
 */
class KelasPelajarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KelasPelajar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KelasPelajarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['kelas_pelajar.id_tahfiz'=>Yii::$app->user->identity->tahfiz]);
        $dataProvider->query->andWhere(['kelas_pelajar.tahun_update'=>date('Y')]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KelasPelajar model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {   
        $model = MaklumatPelajarPenjaga::findOne(base64_decode($id));

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new KelasPelajar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionJanakelas()
    {
        $model = KelasPelajar::find()
                ->select('id_pelajar')
                ->where(['tahun_update'=>date('Y')]);

        $searchModel = new MaklumatPelajarPenjagaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['maklumat_pelajar_penjaga.pusat_pengajian_id'=>Yii::$app->user->identity->tahfiz]);
        $dataProvider->query->andWhere(['maklumat_pelajar_penjaga.alumni'=>0]);
        $dataProvider->query->andWhere(['maklumat_pelajar_penjaga.status_deleted'=>2]);
        $dataProvider->query->andWhere(['NOT IN','maklumat_pelajar_penjaga.id',$model]); 

        return $this->render('janakelas', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionCreate($id)
    {
        $model = new KelasPelajar();
        $model2 = MaklumatPelajarPenjaga::findOne(base64_decode($id));

        $checkzakat = Zakat::find()
                    ->where(['id_pelajar'=>base64_decode($id)])
                    ->andWhere(['tahun'=>date('Y',time())])
                    ->one();

        if (isset($checkzakat)) {
            $model3 = $checkzakat;
        }
        else{
            $model3 = new Zakat();
        }

        if ($model->load(Yii::$app->request->post()) && $model2->load(Yii::$app->request->post()) ) {
            //identify dlu user key in no mykid atau no passport
            if ($_POST['optionidentity'] == 'mykid') {
                $lengnama = str_replace('_', '', $model2->no_mykid);
                if(strlen($lengnama) == 14){
                
                }
                else{
                    Yii::$app->session->setFlash('saveerror','Nombor mykid pelajar perlu mengikut format di tetapkan seperti 123456-78-9101');
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }
            else{
                //do nothing
                // $model->no_mykid = $model->passport;
            }

            $model->enter_by = Yii::$app->user->identity->id;
            $model->created_at = date('Y-m-d h:i:s A');
            $model->tahun_update = date('Y');
            $model->id_tahfiz = $model2->pusat_pengajian_id;
            $model->id_pelajar = $model2->id;

            if ($model->save()) {
                $model2->id_staf = $model->id_staf;
                $model2->kelas = $model->id_kelas;
                $model2->save();

                if (isset($_POST['Zakat']['jumlah_zakat'])) {
                    if ($_POST['Zakat']['jumlah_zakat'] != NULL || $_POST['Zakat']['nota'] != NULL) {
                        $model3->id_pelajar = base64_decode($id);
                        $model3->tahun = date('Y',time());
                        $model3->date_added = date('Y-m-d h:i:s A');
                        $model3->jumlah_zakat = $_POST['Zakat']['jumlah_zakat'];
                        $model3->enter_by = $model->enter_by;
                        $model3->nota = $_POST['Zakat']['nota'];
                        $model3->save();
                    }
                    else{
                        //do nothing
                    }
                    
                }
                else{
                    //do nothing
                }
                Yii::$app->session->setFlash('savedone','Jana kelas pelajar telah berjaya.');
                return $this->redirect(['janakelas']);
            }
            
        }

        return $this->render('_form', [
            'model' => $model,
            'model2' => $model2,
            'model3' => $model3,
        ]);
    }

    public function actionViewbfjana($id)
    {
        $model = MaklumatPelajarPenjaga::findOne(base64_decode($id));

        return $this->render('viewbfjana', [
            'model' => $model,
        ]);
    }
    /**
     * Updates an existing KelasPelajar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = KelasPelajar::findOne(base64_decode($id));
        $model2 = MaklumatPelajarPenjaga::findOne($model->id_pelajar);

        if ($model->load(Yii::$app->request->post()) && $model2->load(Yii::$app->request->post()) ) {
            //identify dlu user key in no mykid atau no passport
            if ($_POST['optionidentity'] == 'mykid') {
                $lengnama = str_replace('_', '', $model2->no_mykid);
                if(strlen($lengnama) == 14){
                
                }
                else{
                    Yii::$app->session->setFlash('saveerror','Nombor mykid pelajar perlu mengikut format di tetapkan seperti 123456-78-9101');
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }
            else{
                //do nothing
                // $model->no_mykid = $model->passport;
            }

            $model->updated_by = Yii::$app->user->identity->id;
            $model->updated_at = date('Y-m-d h:i:s A');
            $model->tahun_update = date('Y');
            $model->id_tahfiz = $model2->pusat_pengajian_id;
            $model->id_pelajar = $model2->id;

            if ($model->save()) {
                $model2->id_staf = $model->id_staf;
                $model2->kelas = $model->id_kelas;
                $model2->save();

                Yii::$app->session->setFlash('savedone','Kemaskini pelajar telah berjaya.');
                return $this->redirect(['index']);
            }
            
        }

        return $this->render('edit', [
            'model' => $model,
            'model2' => $model2,
        ]);
    }

    /**
     * Deletes an existing KelasPelajar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KelasPelajar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KelasPelajar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KelasPelajar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCarikelas($id,$idp)
    {
        $countPosts = LookupKelas::find()
        ->where(['tahap' => $id])
        ->andWhere(['id_pusat_pengajian'=>$idp])
        ->count();

        $posts = LookupKelas::find()
        ->where(['tahap' => $id])
        ->andWhere(['id_pusat_pengajian'=>$idp])
        ->all();

        if($countPosts>0){
            echo "<option value=''>-Sila Pilih-</option>";
            foreach($posts as $post){
                echo "<option value='".$post->id."'>".$post->kelas."</option>";
            }
        } else {
                echo "<option></option>";
        }

    }
}
