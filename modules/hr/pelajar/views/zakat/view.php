<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\hr\pelajar\models\Zakat */

$this->title = 'Maklumat Terperinci';

$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('zakat/view')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    deleteswal();
    $("#some_pjax_id").on("pjax:success", function() {
      deleteswal();
    });

    function deleteswal(){
        $('a.delete').on('click', function(e){
             e.preventDefault();
            var link = $(this).attr('value');

          swal({   
            title: 'Anda pasti mahu hapus maklumat zakat ini ?',
            // text: "Perhatian ! Proses ini tidak boleh diulang semua !",   
            type: "warning",   
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: 'Ya, saya pasti.',
            confirmButtonClass: 'btn-info',
            cancelButtonText: 'Batal',
            cancelButtonClass: 'btn-danger',

          }, 
           function(){   
                // $("#myform").submit();
                $.ajax({
                    type: 'POST',
                    url: link,
                    success: function(data) {
                    
                   }
                });
            });
        })
    }
});
JS;
$this->registerJs($script);
?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg mr-1']) ?>
                <button class="btn btn-outline-primary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Pilih Tindakan
                </button>
                <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -105px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <?= Html::a('Tambah Baru',['create'],['class'=>'dropdown-item']) ?>
                    <?= Html::a('Kemaskini Maklumat',['update','id'=>base64_encode($model->id)],['class'=>'dropdown-item']) ?>
                    <?= Html::a('Hapus',FALSE,['class'=>'dropdown-item delete','id'=>'','value'=>Url::to(['delete','id'=>base64_encode($model->id)])]) ?>
                </div>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-12 mb-4">
        <div class="box box-danger">
            <div class="card-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'namapelajar.nama_pelajar',
                        'tahun',
                        'date_added',
                        'enter_by',
                        'jumlah_zakat',
                        'nota',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
