<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\modules\hr\pelajar\models\MaklumatPelajarPenjaga;

$nama = ArrayHelper::map(MaklumatPelajarPenjaga::find()->where(['alumni'=>0])->andWhere(['status_deleted'=>2])->asArray()->orderBy(['nama_pelajar'=>SORT_ASC])->all(),'id','nama_pelajar'); //retrieve data for dropdown

/* @var $this yii\web\View */
/* @var $model app\modules\hr\pelajar\models\Zakat */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Tambah Zakat';

$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('zakat/create')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

});
JS;
$this->registerJs($script);
?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
                <div class="">
                    <h5 class="mb-4">Maklumat Zakat</h5>
                    <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>
                    <form>
                         <div class="form-row">
                             <div class="form-group col-md-4">
                                <label>Nama Pelajar</label>
                                <?= $form->field($model, 'id_pelajar')->dropDownList($nama, 
                                    [
                                        'prompt'=>'Sila Pilih',
                                        'class'=>'select2-single'
                                    ]
                                    )->label(false) ?>
                             </div>
                             <div class="form-group col-md-4">
                                 <label>Pilih Tahun</label>
                                 <select name="Zakat[tahun]" class="form-control" required="">
                                    <?php 
                                        $already_selected_value = date('Y');
                                        $earliest_year = 2016;
                                        foreach (range(date('Y'), $earliest_year) as $x) {
                                            print '<option value="'.$x.'"'.($x === $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
                                        }
                                    ?>
                                </select>
                             </div>
                             <div class="form-group col-md-4">
                                <label>Jumlah Zakat</label>
                                <?= $form->field($model, 'jumlah_zakat')->textInput(['maxlength' => true])->label(false) ?>
                             </div>
                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-12">
                                <label>Nota</label>
                                <?= $form->field($model, 'nota')->textArea(['rows'=>6])->label(false) ?>

                             </div>
                         </div>
                    </form>
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>

                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success mb-1']) ?>

                        <?= Html::a('Batal',['index'],['class'=>'btn btn-danger mb-1']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>