<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use app\models\LookupPendapatan;

use app\modules\hr\pelajar\models\MaklumatPelajarPenjaga;

use kartik\select2\Select2;

$pendapatan = LookupPendapatan::find()
            ->all();
/* @var $this yii\web\View */
/* @var $model app\modules\hr\pelajar\models\SubsidiPelajar */
/* @var $form yii\widgets\ActiveForm */

$nama = ArrayHelper::map(MaklumatPelajarPenjaga::find()->where(['alumni'=>0])->andWhere(['status_deleted'=>2])->asArray()->orderBy(['nama_pelajar'=>SORT_ASC])->all(),'id','nama_pelajar'); //retrieve data for dropdown

$this->title = 'Tambah Subsidi Pelajar';
$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('subsidi-pelajar/create')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }
    $('#subsidipelajar-id_pelajar').change(function(){
        var check = $(this).val();
        $.ajax({
            url: 'check',
            data: {id: check},
            success: function(data) {
                if(data == 'yes'){
                    var msg = 'Pelajar ini telah memohon subsidi.'
                    $('#showdivmsg').show();
                    $('#showmsg').html(msg);
                    $('#showdivmsg1').show();
                    $('#showmsg1').html(msg);
                    $('.offbtn').attr('disabled','disabled');
                    $('#addmorestd').attr('disabled','disabled');

                }
                else{
                    $('#showdivmsg').hide();
                    $('#showdivmsg1').hide();
                    $('.offbtn').removeAttr('disabled');
                    $('#addmorestd').removeAttr('disabled');
                }
            }
        });
    });
    $('#subsidipelajar-id_pelajar').change(function(){
        var v = $(this).val();
        var f = $(".pelajar").length;

        $.get('stdntdetail',{id : v},function(data){
            var data = $.parseJSON(data);

            $('#subsidipelajar-tahap_semasa').val(data.tahap_semasa);
            $('#nama_pelajar').val(data.nama_pelajar);
            if(f == 0){
                $('#subsidipelajar-namabapa').val(data.nama_bapa);
                $('#subsidipelajar-nokpbapa').val(data.no_kad_pengenalan_bapa);
                $('#subsidipelajar-notelbapa').val(data.no_tel_bapa);
                $('#subsidipelajar-nohpbapa').val(data.no_hp_bapa);
                $('#subsidipelajar-pekerjaanbapa').val(data.pekerjaan_bapa_penjaga);
                $('#subsidipelajar-gajibapa').val(data.gaji_bapa);
                $('#subsidipelajar-alamatmajikanbapa').val(data.alamat_majikan_bapa_penjaga);

                $('#subsidipelajar-namaibu').val(data.nama_ibu);
                $('#subsidipelajar-nokpibu').val(data.no_kad_pengenalan_ibu);
                $('#subsidipelajar-notelibu').val(data.no_tel_ibu);
                $('#subsidipelajar-nohpibu').val(data.no_hp_ibu);
                $('#subsidipelajar-pekerjaanibu').val(data.pekerjaan_ibu);
                $('#subsidipelajar-alamatmajikanibu').val(data.alamat_majikan_ibu);
                $('#subsidipelajar-gajiibu').val(data.gaji_ibu);
            }
            
        });
    });

    $("#addmorestd").click(function(){
        var a = $('#subsidipelajar-tahap_semasa').val();
        var b = $('#subsidipelajar-id_pelajar').val();
        var d = $('#nama_pelajar').val();

        $('.adddiv').show();

        var formhtml = '<div class="form-row pelajar"><div class="form-group col-md-4"><label>Nama Pelajar</label><div class="form-group"><input type="text" id="namapelajar" readonly="" class="form-control" name="namapelajar[]" aria-invalid="false" value="'+d+'"><input type="hidden" id="idpelajar" class="form-control" name="idpelajar[]" aria-invalid="false" value="'+b+'"></div></div><div class="form-group col-md-4"><label>Tahun (Tahap Pengajian)</label><div class="form-group"><input type="text" id="tahapsemasa" readonly="" class="form-control" name="tahapsemasa[]" aria-invalid="false" value="'+a+'"></div></div><div class="form-group col-md-4"><div id="del" style="margin-top: 25px;"><div class="form-group"><label></label><button type="button" class="btn btn-danger btn-sm" id="deletestd">Padam</button></div></div></div></div>';
        
        $("#itemdiv").append(formhtml);

        $('.select2-selection__rendered').text('');
        $('#subsidipelajar-id_pelajar').val('')
        $('#subsidipelajar-tahap_semasa').val('');
        $('#nama_pelajar').val('');

        // $('#subsidipelajar-id_pelajar').removeAttr('id');
    });

    $("#itemdiv").on('click','#deletestd',function(){
        $(this).closest('.form-row').remove();
    });

    $('.select2-single').select2({
        placeholder:'Sila Pilih',
      ajax: {
        url: 'stdnt',
        dataType: 'json',
        delay:250
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
      }
    });

    // $('.select2-container').removeClass('select2-container--default');
    // $('.select2-container').addClass('select2-container--bootstrap');

});
JS;
$this->registerJs($script);
?>
<style type="text/css">
    .select2-selection__placeholder{
        color:black !important;
    }
</style>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
                <h5 class="mb-4">Maklumat Pelajar (Belajar Di Tahfiz)</h5>
                <div class="separator mb-5"></div>
                <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>
                <form>
                    <div class="adddiv" style="display: none">
                        <div id='itemdiv'>
                            
                        </div>
                    </div>
                    <div class="form-row">
                         <div class="form-group col-md-4">
                            <label>Nama Pelajar</label>
                                <select id="subsidipelajar-id_pelajar" class="select2-single" name="SubsidiPelajar[id_pelajar]"></select>
                         </div>
                         <div class="form-group col-md-4">
                             <label>Tahun (Tahap Pengajian)</label>
                            <?= $form->field($model, 'tahap_semasa')->textInput(['maxlength' => true])->label(false) ?>
                            <input type="hidden" name="nama_pelajar" id="nama_pelajar">
                         </div>
                         <div class="form-group col-md-4">
                             <div class="" style="margin-top: 25px;">
                                <div class="form-group">
                                    <label></label>
                                    <input type="button" class="btn btn-outline-primary btn-sm" id="addmorestd"  value="Tambah Pelajar">
                                </div>
                            </div>
                         </div>
                    </div>
                    <div id="showdivmsg" style="display: none;">
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="alert alert-danger" role="alert" id="showmsg">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label>Kategori Subsidi</label>
                            <?= $form->field($model, 'category_subsidi')->textInput(['maxlength' => true])->label(false) ?>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Peruntukan Subsidi (RM)</label>
                            <?= $form->field($model, 'peruntukan')->textInput(['maxlength' => true])->label(false) ?>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Bilangan Tanggungan</label>
                            <?= $form->field($model, 'bilangan_tanggungan')->textInput(['maxlength' => true])->label(false) ?>
                        </div>
                    </div>
                    <div class="separator mb-5"></div>
                    <h5 class="mb-4">Maklumat Bapa/Penjaga</h5>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label>Nama Bapa</label>
                            <input type="text" id="subsidipelajar-namabapa" class="form-control" name="namabapa">
                        </div>
                        <div class="form-group col-md-4">
                            <label>No K/P Bapa</label>
                            <input type="text" id="subsidipelajar-nokpbapa" class="form-control" name="nokpbapa">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Pekerjaan Bapa</label>
                            <input type="text" id="subsidipelajar-pekerjaanbapa" class="form-control" name="pekerjaanbapa">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label>No Telefon Rumah</label>
                            <input type="text" id="subsidipelajar-notelbapa" class="form-control" name="notelbapa">
                        </div>
                        <div class="form-group col-md-4">
                            <label>No Telefon Bimbit</label>
                            <input type="text" id="subsidipelajar-nohpbapa" class="form-control" name="nohpbapa">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Pendapatan Bapa</label>
                            <select id="subsidipelajar-gajibapa" name="gajibapa" class="form-control">
                                <option value="0">--Sila Pilih--</option>
                                <?php
                                    foreach ($pendapatan as $key => $value) {
                                ?>
                                    <option value="<?php echo $value['id'] ?>"><?php echo $value['pendapatan'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Alamat Majikan</label>
                            <textarea rows="6" class="form-control" id="subsidipelajar-alamatmajikanbapa" name="alamatmajikanbapa">
                            </textarea>
                        </div>
                    </div>
                    <div class="separator mb-5"></div>
                    <h5 class="mb-4">Maklumat Ibu</h5>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label>Nama Ibu</label>
                            <input type="text" id="subsidipelajar-namaibu" class="form-control" name="namaibu">
                        </div>
                        <div class="form-group col-md-4">
                            <label>No K/P Ibu</label>
                            <input type="text" id="subsidipelajar-nokpibu" class="form-control" name="nokpibu">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Pekerjaan Ibu</label>
                            <input type="text" id="subsidipelajar-pekerjaanibu" class="form-control" name="pekerjaanibu">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label>No Telefon Rumah</label>
                            <input type="text" id="subsidipelajar-notelibu" class="form-control" name="notelibu">
                        </div>
                        <div class="form-group col-md-4">
                            <label>No Telefon Bimbit</label>
                            <input type="text" id="subsidipelajar-nohpibu" class="form-control" name="nohpibu">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Pendapatan Ibu</label>
                            <select id="subsidipelajar-gajiibu" name="gajiibu" class="form-control">
                                <option value="0">--Sila Pilih--</option>
                                <?php
                                    foreach ($pendapatan as $key => $value) {
                                ?>
                                    <option value="<?php echo $value['id'] ?>"><?php echo $value['pendapatan'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Alamat Majikan</label>
                            <textarea rows="6" class="form-control" id="subsidipelajar-alamatmajikanibu" name="alamatmajikanibu">
                            </textarea>
                        </div>
                    </div>
                </form>
                <div class="box-footer">
                    <div class="col-md-12">
                        <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>

                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success mb-1 offbtn']) ?>

                        <?= Html::a('Batal',['index'],['class'=>'btn btn-danger mb-1']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
