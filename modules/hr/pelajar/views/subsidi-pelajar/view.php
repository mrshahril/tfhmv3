<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\hr\pelajar\models\SubsidiPelajar */

$this->title = 'Maklumat Subsidi Pelajar';
$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('subsidi-pelajar/view')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    $('.modaltest').click(function(){
        $('#exampleModal').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });

    deleteswal();
    function deleteswal(){
        $('a.delete').on('click', function(e){
             e.preventDefault();
            var link = $(this).attr('value');
            console.log(link);
          swal({   
            title: "Anda pasti mahu hapus pelajar ini dari senarai subsidi pelajar ?",
            // text: "Perhatian ! Proses ini tidak boleh diulang semua !",   
            type: "warning",   
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: 'Ya, saya pasti.',
            confirmButtonClass: 'btn-info',
            cancelButtonText: 'Batal',
            cancelButtonClass: 'btn-danger',

          }, 
           function(){   
                // $("#myform").submit();
                $.ajax({
                    type: 'POST',
                    url: link,
                    success: function(data) {
                    
                   }
                });
            });
        })
    }
});
JS;
$this->registerJs($script);
?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg']) ?>
                <button class="btn btn-outline-primary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Pilih Tindakan
                </button>
                <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -105px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <?= Html::a('Tambah Baru',['create'],['class'=>'dropdown-item']) ?>
                    <?= Html::a('Hapus',FALSE,['class'=>'dropdown-item delete','value'=>Url::to(['deleteall','id'=>base64_encode($model2->no_siri)])]) ?>
                </div>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row mb-4">
    <div class="col-12 col-lg-12">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Maklumat Pelajar (Belajar Di Tahfiz)</h5> 
                        <div class="separator mb-5"></div>
                        <table class="table table-striped">
                            <tr>
                                <th>Nama Pelajar</th>
                                <th>Tahap Pengajian</th>
                                <th>Tindakan</th>
                            </tr>
                            <?php foreach ($model as $key => $value): ?>
                                <tr>
                                    <td><?php echo $value->namapelajar->nama_pelajar ?></td>
                                    <td><?php echo $value->tahap_semasa ?></td>
                                    <td>
                                        <?= Html::a('Hapus',FALSE,['class'=>'btn btn-xs btn-secondary mb-1 delete','value'=>Url::to(['delete','id'=>base64_encode($value->id)])]) ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </table>
                        <div>
                            <?= Html::a('Tambah Pelajar',FALSE,['class'=>'btn btn-info mb-1 modaltest','value'=>Url::to(['addpelajar','id'=>base64_encode($model2->no_siri)])]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Maklumat Subsidi</h5> 
                        <div class="separator mb-5"></div>
                        <?= DetailView::widget([
                            'model' => $model2,
                            'attributes' => [
                                'no_siri',
                                'category_subsidi',
                                'bilangan_tanggungan',
                                [
                                    'label'=>'Peruntukan (RM)',
                                    'format'=>'raw',
                                    'value'=>number_format($model2->peruntukan,2)
                                ],
                                'date_apply',

                            ],
                        ]) ?>
                        <?= Html::a('Kemaskini Subsidi',FALSE,['class'=>'btn btn-info mb-1 modaltest','value'=>Url::to(['edit','id'=>base64_encode($model2->no_siri)])]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12 col-lg-12">
        <div class="row ">
            <div class="col-md-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Maklumat Bapa / Penjaga</h5> 
                        <div class="separator mb-5"></div>
                        <?= DetailView::widget([
                            'model' => $model2,
                            'attributes' => [
                                [
                                    'label'=>'Nama Bapa',
                                    'format'=>'raw',    
                                    'value'=>$model2->namapelajar->nama_bapa,
                                ],
                                [
                                    'label'=>'No Kad Pengenalan',
                                    'format'=>'raw',    
                                    'value'=>$model2->namapelajar->no_kad_pengenalan_bapa,
                                ],
                                [
                                    'label'=>'Nama Tel',
                                    'format'=>'raw',    
                                    'value'=>$model2->namapelajar->no_tel_bapa,
                                ],
                                [
                                    'label'=>'Nama Hp',
                                    'format'=>'raw',    
                                    'value'=>$model2->namapelajar->no_hp_bapa,
                                ],
                                [
                                    'label'=>'Pekerjaan',
                                    'format'=>'raw',    
                                    'value'=>$model2->namapelajar->pekerjaan_bapa_penjaga,
                                ],
                                [
                                    'label'=>'Alamat Majikan',
                                    'format'=>'raw',    
                                    'value'=>$model2->namapelajar->alamat_majikan_bapa_penjaga,
                                ],
                                [
                                    'label'=>'Gaji',
                                    'format'=>'raw',    
                                    'value'=>$model2->namapelajar->pendapatanbapa->pendapatan,
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Maklumat Ibu</h5> 
                        <div class="separator mb-5"></div>
                        <?= DetailView::widget([
                            'model' => $model2,
                            'attributes' => [
                                [
                                    'label'=>'Nama Bapa',
                                    'format'=>'raw',    
                                    'value'=>$model2->namapelajar->nama_ibu,
                                ],
                                [
                                    'label'=>'No Kad Pengenalan',
                                    'format'=>'raw',    
                                    'value'=>$model2->namapelajar->no_kad_pengenalan_ibu,
                                ],
                                [
                                    'label'=>'Nama Tel',
                                    'format'=>'raw',    
                                    'value'=>$model2->namapelajar->no_tel_ibu,
                                ],
                                [
                                    'label'=>'Nama Hp',
                                    'format'=>'raw',    
                                    'value'=>$model2->namapelajar->no_hp_ibu,
                                ],
                                [
                                    'label'=>'Pekerjaan',
                                    'format'=>'raw',    
                                    'value'=>$model2->namapelajar->pekerjaan_ibu,
                                ],
                                [
                                    'label'=>'Alamat Majikan',
                                    'format'=>'raw',    
                                    'value'=>$model2->namapelajar->alamat_majikan_ibu,
                                ],
                                [
                                    'label'=>'Gaji',
                                    'format'=>'raw',    
                                    'value'=>$model2->namapelajar->pendapatanibu->pendapatan,
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

