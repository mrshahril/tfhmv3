<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

$this->title = 'Tambah Pelajar';

$script = <<< JS
$(document).ready(function(){
	$('.select2-single').select2({
        placeholder:'Sila Pilih',
      ajax: {
        url: 'stdnt',
        dataType: 'json',
        delay:250
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
      }
    });
});
JS;
$this->registerJs($script);
?>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h5><?= Html::encode($this->title) ?></h5>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
	<div class="col-md-12">
        <div class="box box-danger">
        	<div class="box-body">
        		<?php $form = ActiveForm::begin([
                    'id' => 'form-pr',
                    'enableClientValidation' => true,
                    'enableAjaxValidation' => false,
                ]); ?>
                <div class="form-row">
	                <div class="form-group col-md-12">
		                <label>Nama Pelajar</label>
		                <select id="subsidipelajar-id_pelajar" class="select2-single" name="SubsidiPelajar[id_pelajar]"></select>
		            </div>
		            <div class="form-group col-md-12">
		                <label>Tahun (Tahap Pengajian)</label>
		                <?= $form->field($model, 'tahap_semasa')->textInput(['maxlength' => true])->label(false) ?>
		            </div>
	        	</div>
	        	<div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
	                <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary mb-1']) ?>
	            </div>
            	<?php ActiveForm::end(); ?>
        	</div>
        </div>
    </div>
</div>