<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\hr\pelajar\models\SubsidiPelajar */

$this->title = 'Create Subsidi Pelajar';
$this->params['breadcrumbs'][] = ['label' => 'Subsidi Pelajars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subsidi-pelajar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
