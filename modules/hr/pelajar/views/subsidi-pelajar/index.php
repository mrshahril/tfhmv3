<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\hr\pelajar\models\SubsidiPelajarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subsidi Pelajar '.date('Y');
$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('subsidi-pelajar/index')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    deleteswal();
    $("#some_pjax_id").on("pjax:success", function() {
      init_click_handlers(); //reactivate links in grid after pjax update
      deleteswal();
    });

    function deleteswal(){
        $('a.delete').on('click', function(e){
             e.preventDefault();
            var link = $(this).attr('value');
            console.log(link);
          swal({   
            title: "Anda pasti mahu hapus pelajar ini dari senarai subsidi pelajar ?",
            // text: "Perhatian ! Proses ini tidak boleh diulang semua !",   
            type: "warning",   
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: 'Ya, saya pasti.',
            confirmButtonClass: 'btn-info',
            cancelButtonText: 'Batal',
            cancelButtonClass: 'btn-danger',

          }, 
           function(){   
                // $("#myform").submit();
                $.ajax({
                    type: 'POST',
                    url: link,
                    success: function(data) {
                    
                   }
                });
            });
        })
    }
});
JS;
$this->registerJs($script);
?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Tambah Baru',['create'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?= $this->render('_search', ['model' => $searchModel]);  ?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="col-md-12">
    <div class="box box-danger">
        <div class="box-body table-responsive">
            <?php Pjax::begin(['id'=>'some_pjax_id']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'layout' => "{summary}\n{items}\n<nav class='mt-4 mb-3'>{pager}</nav>",
                'pager' => [
                    'firstPageLabel' => 'Mula',
                    'lastPageLabel' => 'Akhir',
                    'prevPageLabel' => 'Sebelumnya',
                    'nextPageLabel' => 'Seterusnya',

                    'maxButtonCount' => 5,

                     'options' => [
                        'tag' => 'ul',
                        'class' => 'pagination justify-content-center mb-0',
                    ],
                    'linkContainerOptions'=>['class'=>'page-item'],
                    'linkOptions' => ['class' => 'page-link'],
                    'activePageCssClass' => 'active',
                ],
                'tableOptions'=>['class'=>'table table-striped'],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'namapelajar.nama_pelajar',
                    [
                        'attribute'=>'Nama Bapa/Penjaga',
                        'format'=>'raw',    
                        'value' => 'namapelajar.nama_bapa',
                    ],
                    'date_apply',
                    'status',
                    'peruntukan',
                    'tahun',
                    [
                        'header' => 'Tindakan',
                        'class' => 'yii\grid\ActionColumn',
                        'template'=>'{all}',
                        'buttons' => [
                            'all' => function ($url, $model, $key) {
                                return ButtonDropdown::widget([
                                    'encodeLabel' => false, // if you're going to use html on the button label
                                    'label' => 'Pilih Tindakan',
                                    'dropdown' => [
                                        'encodeLabels' => false, // if you're going to use html on the items' labels
                                        'items' => [
                                            [
                                                'label' => \Yii::t('yii', 'Maklumat Permohonan'),
                                                'url' => ['subsidi-pelajar/view', 'id' =>base64_encode($model->no_siri)],
                                                'linkOptions' => ['class'=>'dropdown-item'],
                                                'visible' => true,  // if you want to hide an item based on a condition, use this
                                            ],
                                            [
                                                'label' => \Yii::t('yii', 'Hapus Permohonan'),
                                                'url'=>FALSE,
                                                'linkOptions' => ['class'=>'dropdown-item delete','value'=>Url::to(['deleteall','id'=>base64_encode($model->no_siri)])],
                                                'visible' => true,  // if you want to hide an item based on a condition, use this
                                            ],
                                            
                                        ],
                                        
                                    ],
                                    'options' => [
                                        'class' => 'btn btn-outline-primary btn-sm',   // btn-success, btn-info, et cetera
                                    ],
                                    'split' => false,    // if you want a split button
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
