<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\LookupKelas;
use app\models\GuruKanan;

use app\modules\hr\staff\models\MaklumatKakitangan;

/* @var $this yii\web\View */
/* @var $model app\modules\hr\pelajar\models\KelasPelajar */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Kemaskini Kelas Pelajar';

$tahap = [
    ['tahap' => '1', 'desc' => '1'],
    ['tahap' => '2', 'desc' => '2'],
    ['tahap' => '3', 'desc' => '3'],
    ['tahap' => '4', 'desc' => '4'],
    ['tahap' => '5', 'desc' => '5'],
    ['tahap' => '6', 'desc' => '6'],
];

$listtahap = ArrayHelper::map($tahap, 'tahap', 'desc');
$kelas = ArrayHelper::map(LookupKelas::find()->where(['id_pusat_pengajian'=>$model2->pusat_pengajian_id])->asArray()->all(), 'id', 'kelas');
$gurukanan = ArrayHelper::map(GuruKanan::find()->where(['id_pusat_pengajian'=>$model2->pusat_pengajian_id])->all(), 'id_staff', function($data) {
        return $data->gurukanan->nama;
    });
$namastaf = ArrayHelper::map(MaklumatKakitangan::find()->where(['status_pekerjaan'=>1])->andWhere(['tahfiz'=>$model2->pusat_pengajian_id])->asArray()->all(), 'id_staf', 'nama');

$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('kelas-pelajar/create')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    $('#customRadio1').click(function(){
        $('#divmykid').show();
        $('#divpassport').hide();
        $('#maklumatpelajarpenjaga-no_mykid').prop('disabled', false);
        $('#passport').prop('disabled', true);

    })
    $('#customRadio2').click(function(){
        $('#divmykid').hide();
        $('#divpassport').show();
        $('#maklumatpelajarpenjaga-no_mykid').prop('disabled', true);
        $('#passport').prop('disabled', false);
    })

    if($('#customRadio1').is(':checked')){
        $('#divmykid').show();
        $('#divpassport').hide();
        $('#maklumatpelajarpenjaga-no_mykid').prop('disabled', false);
        $('#passport').prop('disabled', true);
    }
    else{
        $('#divmykid').hide();
        $('#divpassport').show();
        $('#maklumatpelajarpenjaga-no_mykid').prop('disabled', true);
        $('#passport').prop('disabled', false);
    }
});
JS;
$this->registerJs($script);
?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['janakelas'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('saveerror')) { ?>
            <div class="alert alert-danger" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('saveerror'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body ml-4">
                <h5 class="mb-4 card-title">Kelas Pelajar : <?= $model2->nama_pelajar ?></h5>
                <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>
                <form>
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">Pilih nombor mykid atau nombor passport pelajar.</label>
                        <div class="col-sm-10">
                            <div class="mb-4">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="optionidentity" class="custom-control-input" value="mykid" <?php if(preg_match("/[a-z]/i", $model2->no_mykid)){
                                        echo 'checked';
                                    }else{ echo 'checked';} ?>>
                                    <label class="custom-control-label" for="customRadio1">No Mykid</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" name="optionidentity" class="custom-control-input" value="passport" <?php if(preg_match("/[a-z]/i", $model2->no_mykid)){
                                        echo 'checked=""';
                                    }?>>
                                    <label class="custom-control-label" for="customRadio2" >No Passport / No Surat Beranak</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row" style="display: none" id="divmykid">
                        <label class="col-sm-2 col-form-label">No Mykid</label>
                        <div class="col-sm-10">
                            <?= $form->field($model2, 'no_mykid')->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '999999-99-9999',
                                // 'options' => ['class' => 'form-control','style'=>'display:none']
                            ])->label(false) ?>
                        </div>
                        
                    </div>
                    <div class="form-group row" style="display: none" id="divpassport">
                        <label class="col-sm-2 col-form-label">No Passport/No Surat Beranak</label>
                        <div class="col-sm-10">
                            <?=  $form->field($model2, 'no_mykid')->textInput(['maxlength' => true,'id'=>'passport','autocomplete'=>'off'])->label(false) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Tahap</label>
                        <div class="col-sm-10">
                            <?= $form->field($model2, 'tahap_semasa')->dropDownList($listtahap, 
                                [
                                    'onchange'=>'$.post( "'.Yii::$app->urlManager->createUrl(['hr/pelajar/kelas-pelajar/carikelas','idp'=>Yii::$app->user->identity->tahfiz,'id'=>'']).'"+$(this).val(), function( data ) {$( "select#idkelas" ).html( data );});',
                                     
                                    'prompt' => '--Sila Pilih Tahap--'])->label(false) ?>

                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Kelas</label>
                        <div class="col-sm-10">
                            <?= $form->field($model, 'id_kelas')->dropDownList($kelas,
                                    [
                                        'prompt'=>'--Sila Pilih Kelas--',
                                        'id'=> 'idkelas'
                                    ]
                                )->label(false);
                            ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Guru Kanan</label>
                        <div class="col-sm-10">
                            <?= $form->field($model, 'guru_kanan')->dropDownList($gurukanan,
                                    [
                                        'prompt'=>'--Sila Pilih Guru Kanan--',
                                    ]
                                )->label(false);
                            ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Guru</label>
                        <div class="col-sm-10">
                            <?= $form->field($model, 'id_staf')->dropDownList($namastaf,
                                    [
                                        'prompt'=>'--Sila Pilih Nama Guru--',
                                    ]
                                )->label(false);
                            ?>
                        </div>
                    </div>
                </form>
                <div class="box-footer">
                    <div class="col-md-12">
                        <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>

                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success mb-1']) ?>

                        <?= Html::a('Batal',['janakelas'],['class'=>'btn btn-danger mb-1']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
