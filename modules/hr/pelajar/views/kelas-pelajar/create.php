<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\hr\pelajar\models\KelasPelajar */

$this->title = 'Create Kelas Pelajar';
$this->params['breadcrumbs'][] = ['label' => 'Kelas Pelajars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-pelajar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
