<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\hr\pelajar\models\KelasPelajarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jana Kelas Pelajar '.date('Y');

$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('kelas-pelajar/janakelas')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
        $('.app-menu > .p-4 > .scroll > ul > li#janakelas').addClass('active');
    }

});
JS;
$this->registerJs($script);
?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row app-row">
    <div class="col-12">
        <div class="mb-2">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="separator mb-5"></div>
        <div class="row">
            <div class="col-12">
                <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
                    <div class="alert alert-success" role="alert" id="messageerror2">
                        <?php echo Yii::$app->session->getFlash('savedone'); ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-12">
            <?= $this->render('_searchjanakelas', ['model' => $searchModel]);  ?>
        </div>
        <div class="col-md-12">
            <div class="box box-danger">
                <div class="box-body table-responsive">
                    <?php Pjax::begin(); ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'layout' => "{summary}\n{items}\n<nav class='mt-4 mb-3'>{pager}</nav>",
                            'pager' => [
                                'firstPageLabel' => 'Mula',
                                'lastPageLabel' => 'Akhir',
                                'prevPageLabel' => 'Sebelumnya',
                                'nextPageLabel' => 'Seterusnya',

                                'maxButtonCount' => 5,

                                 'options' => [
                                    'tag' => 'ul',
                                    'class' => 'pagination justify-content-center mb-0',
                                ],
                                'linkContainerOptions'=>['class'=>'page-item'],
                                'linkOptions' => ['class' => 'page-link'],
                                'activePageCssClass' => 'active',
                            ],
                            'tableOptions'=>['class'=>'table table-striped'],
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                'nama_pelajar',
                                'no_mykid',

                                [
                                    'header' => 'Tindakan',
                                    'class' => 'yii\grid\ActionColumn',
                                    'template'=>'{all}',
                                    'buttons' => [
                                        'all' => function ($url, $model, $key) {
                                            return ButtonDropdown::widget([
                                                'encodeLabel' => false, // if you're going to use html on the button label
                                                'label' => 'Pilih Tindakan',
                                                'dropdown' => [
                                                    'encodeLabels' => false, // if you're going to use html on the items' labels
                                                    'items' => [
                                                        [
                                                            'label' => \Yii::t('yii', 'Lihat Maklumat'),
                                                            'url' => ['kelas-pelajar/viewbfjana', 'id' =>base64_encode($key)],
                                                            'linkOptions' => ['class'=>'dropdown-item'],
                                                            'visible' => true,  // if you want to hide an item based on a condition, use this
                                                        ],
                                                        [
                                                            'label' => \Yii::t('yii', 'Jana'),
                                                            'url' => ['kelas-pelajar/create', 'id' =>base64_encode($key)],
                                                            'linkOptions' => ['class'=>'dropdown-item'],
                                                            'visible' => true,  // if you want to hide an item based on a condition, use this
                                                        ],
                                                    ],
                                                    
                                                ],
                                                'options' => [
                                                    'class' => 'btn btn-outline-primary btn-sm',   // btn-success, btn-info, et cetera
                                                ],
                                                'split' => false,    // if you want a split button
                                            ]);
                                        },
                                    ],
                                ],
                            ],
                        ]); ?>
                        <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->render('submenu.php') ?>