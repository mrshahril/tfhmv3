<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\hr\pelajar\models\MaklumatPelajarPenjaga */

$this->title = 'Maklumat Terperinci Pelajar';

$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('kelas-pelajar/viewbfjana')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

});
JS;
$this->registerJs($script);
?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg mr-1']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12 mb-4">
        <div class="box box-danger">
            <div class="card-body">
                <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'nama_pelajar',
                            'jantina',
                            'tarikh_lahir',
                            'tempat_lahir:ntext',
                            'no_surat_beranak',
                            'no_mykid',
                            'nama_tahfiz.pusat_pengajian',
                            'sesi_pengajian',
                            'tarikh_masuk',
                            'tahap_semasa',
                            'nama_kelas.kelas',
                            [
                                'attribute'=>'alamat_rumah',
                                'format'=>'raw',    
                                'value' => $model->alamat_rumah ? strip_tags($model->alamat_rumah) : ''
                            ],
                            'warganegara',
                            
                            [
                                'attribute'=>'alumni',
                                'format'=>'raw',    
                                'value' => $model->alumni == 0 ? 'Tidak' : 'Ya'
                            ],
                            'nama_bapa',
                            'no_kad_pengenalan_bapa',
                            'pekerjaan_bapa_penjaga',
                            'no_tel_bapa',
                            'no_hp_bapa',
                            'alamat_majikan_bapa_penjaga:ntext',
                            [
                                'attribute'=>'Pendapatan Bapa',
                                'format'=>'raw',    
                                'value' => $model->gaji_bapa ? $model->pendapatanbapa->pendapatan : ''
                            ],
                            [
                                'attribute'=>'Pendapatan Ibu',
                                'format'=>'raw',    
                                'value' => $model->gaji_ibu ? $model->pendapatanibu->pendapatan : ''
                            ],
                            'no_kad_pengenalan_ibu',
                            'pekerjaan_ibu',
                            'no_tel_ibu',
                            'no_hp_ibu',
                            'alamat_majikan_ibu:ntext',
                            
                            'negeri.state',
                            'daerah.district'
                        ],
                    ]) ?>
            </div>
        </div>
    </div>
</div>
