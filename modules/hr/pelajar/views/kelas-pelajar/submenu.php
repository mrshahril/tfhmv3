<?php 
use yii\helpers\Html;

use app\modules\hr\pelajar\models\KelasPelajar;

$getTotal = KelasPelajar::janakelas();
?>
<div class="app-menu">
    <div class="p-4">
        <div class="scroll">
            <p class="text-muted text-small">Status</p>
            <ul class="list-unstyled mb-5">
                <li id="listpelajar">
                    <?= Html::a('<i class="simple-icon-people"></i>Senarai Pelajar ',['index']) ?>
                </li>
                <li id="janakelas">
                    <?= Html::a('<i class="simple-icon-check"></i>Jana Kelas <span class="float-right badge badge-outline-danger">'.$getTotal.'</span>',['janakelas']) ?>
                </li>
            </ul>
        </div>
    </div>
    <a class="app-menu-button d-inline-block d-lg-none" href="#" style="font-size: 25px !important">
        <i class="simple-icon-refresh"></i>
    </a>
</div>