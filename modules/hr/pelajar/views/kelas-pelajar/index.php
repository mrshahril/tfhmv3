<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\hr\pelajar\models\KelasPelajarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelas Pelajar';

$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('kelas-pelajar/index')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
        $('.app-menu > .p-4 > .scroll > ul > li#listpelajar').addClass('active');

    }

});
JS;
$this->registerJs($script);
?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row app-row">
    <div class="col-12">
        <div class="mb-2">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="separator mb-5"></div>
        <div class="row">
            <div class="col-12">
                <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
                    <div class="alert alert-success" role="alert" id="messageerror2">
                        <?php echo Yii::$app->session->getFlash('savedone'); ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-12">
            <?= $this->render('_search', ['model' => $searchModel]);  ?>
        </div>
        <div class="col-md-12">
            <div class="box box-danger">
                <div class="col-md-12">
                    <h6 class='card-title mt-2'><i>Senarai pelajar ini adalah senarai kelas pelajar pada tahun semasa.</i></h6>
                        <div class="separator mb-5"></div>
                </div>
                <div class="box-body table-responsive">
                    <?php Pjax::begin(); ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'layout' => "{summary}\n{items}\n<nav class='mt-4 mb-3'>{pager}</nav>",
                            'pager' => [
                                'firstPageLabel' => 'Mula',
                                'lastPageLabel' => 'Akhir',
                                'prevPageLabel' => 'Sebelumnya',
                                'nextPageLabel' => 'Seterusnya',

                                'maxButtonCount' => 5,

                                 'options' => [
                                    'tag' => 'ul',
                                    'class' => 'pagination justify-content-center mb-0',
                                ],
                                'linkContainerOptions'=>['class'=>'page-item'],
                                'linkOptions' => ['class' => 'page-link'],
                                'activePageCssClass' => 'active',
                            ],
                            'tableOptions'=>['class'=>'table table-striped'],
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                'namapelajar.nama_pelajar',
                                'kelas.kelas',
                                [
                                    'attribute'=>'id_staf',
                                    'format'=>'raw',
                                    'value'=>function($data){
                                        return $data->id_staf ? $data->namaguru->nama : '';
                                    }
                                ],
                                'tahun_update',
                                //'guru_kanan',
                                //'id_tahfiz',
                                //'enter_by',
                                //'created_at',
                                //'updated_at',
                                //'updated_by',
                                [
                                    'header' => 'Tindakan',
                                    'class' => 'yii\grid\ActionColumn',
                                    'template'=>'{all}',
                                    'buttons' => [
                                        'all' => function ($url, $model, $key) {
                                            return ButtonDropdown::widget([
                                                'encodeLabel' => false, // if you're going to use html on the button label
                                                'label' => 'Pilih Tindakan',
                                                'dropdown' => [
                                                    'encodeLabels' => false, // if you're going to use html on the items' labels
                                                    'items' => [
                                                        [
                                                            'label' => \Yii::t('yii', 'Lihat Maklumat'),
                                                            'url' => ['kelas-pelajar/view', 'id' =>base64_encode($model->id_pelajar)],
                                                            'linkOptions' => ['class'=>'dropdown-item'],
                                                            'visible' => true,  // if you want to hide an item based on a condition, use this
                                                        ],
                                                        [
                                                            'label' => \Yii::t('yii', 'Kemaskini Maklumat'),
                                                            'url' => ['kelas-pelajar/update', 'id' =>base64_encode($key)],
                                                            'linkOptions' => ['class'=>'dropdown-item'],
                                                            'visible' => true,  // if you want to hide an item based on a condition, use this
                                                        ],
                                                    ],
                                                    
                                                ],
                                                'options' => [
                                                    'class' => 'btn btn-outline-primary btn-sm',   // btn-success, btn-info, et cetera
                                                ],
                                                'split' => false,    // if you want a split button
                                            ]);
                                        },
                                    ],
                                ],
                            ],
                        ]); ?>
                        <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->render('submenu.php') ?>