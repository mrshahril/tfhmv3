<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use app\models\LookupState;
use app\models\LookupDistrict;
use app\models\LookupPendapatan;
use app\models\LookupPusatPengajian;
/* @var $this yii\web\View */
/* @var $model app\modules\hr\pelajar\models\MaklumatPelajarPenjaga */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Tambah Pelajar';

$pendapatan = ArrayHelper::map(LookupPendapatan::find()->asArray()->all(), 'id', 'pendapatan');
$pendaftaran = ArrayHelper::map(LookupPusatPengajian::find()->asArray()->all(), 'id', 'pusat_pengajian');
$pengajian = ArrayHelper::map(LookupPusatPengajian::find()->where(['!=','id',14])->asArray()->all(), 'id', 'pusat_pengajian');
$state = ArrayHelper::map(LookupState::find()->asArray()->all(), 'state_id', 'state');
$district = ArrayHelper::map(LookupDistrict::find()->where(['state_id'=>$model->state_id])->asArray()->all(), 'district_id', 'district');

$link = Yii::$app->urlManager->baseUrl;

$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('maklumat-pelajar-penjaga/create')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    $('#customRadio1').click(function(){
        $('#divmykid').show();
        $('#divpassport').hide();
        $('#maklumatpelajarpenjaga-no_mykid').prop('disabled', false);
        $('#passport').prop('disabled', true);
        $('#passport').val('');
        $(".pelajarDiv").hide();
        
    })
    $('#customRadio2').click(function(){
        $('#divmykid').hide();
        $('#divpassport').show();
        $('#maklumatpelajarpenjaga-no_mykid').prop('disabled', true);
        $('#passport').prop('disabled', false);
        $('#maklumatpelajarpenjaga-no_mykid').val('');
        $(".pelajarDiv").hide();

    })
    $('#maklumatpelajarpenjaga-no_mykid').keyup(function() {
        var v = $('#maklumatpelajarpenjaga-no_mykid').val();
        $.ajax({
            url: 'checkkp',
            data: {id: v},
            success: function(data) {
                if (data == "" || data == "No Data") {
                    $(".pelajarDiv").hide();
                    $(".exist").prop("disabled", false);
                    // $(".showmessage").html(data);
                }
                else{
                    if (data == 'ada') {
                        $(".pelajarDiv").show();
                        $.ajax({
                            url: 'checkkp_id',
                            data: {id: v},
                            success: function(response) {
                                var m = "<a href='$link/hr/pelajar/maklumat-pelajar-penjaga/existdata?id="+response+"'>Maklumat Pelajar Ini Telah Wujud. Sila Klik Disini Untuk Kemaskini Maklumat Pelajar</a>";
                                $(".showmessage").html(m);
                                $(".exist").prop("disabled", true);
                            }
                        });
                    }
                    else{
                        $(".pelajarDiv").hide();
                    }
                }
            }
        });
    });
    $('#passport').keyup(function() {
        
        var v = $('#passport').val();
        
        $.ajax({
               url: 'checkkp',
               data: {id: v},
  
                success: function(data) {
                    if (data == "" || data == "No Data") {
                        $(".pelajarDiv").hide();
                        $(".exist").prop("disabled", false);
                        // $(".showmessage").html(data);
                    }
                    else{
                        if (data == 'ada') {
                            $(".pelajarDiv").show();
                            $.ajax({
                                url: 'checkkp_id',
                                data: {id: v},
                                success: function(response) {
                                    var m = "<a href='$link/hr/pelajar/maklumat-pelajar-penjaga/existdata?id="+response+"'>Maklumat Pelajar Ini Telah Wujud. Sila Klik Disini Untuk Kemaskini Maklumat Pelajar</a>";

                                    $(".showmessage").html(m);
                                    $(".exist").prop("disabled", true);
                                }
                            });

                            
                        }
                        else{
                            $(".exist").prop("disabled", false);
                            $(".pelajarDiv").hide();

                        }
                        
                    }
               }
        });
    });

    $('#yuranpendaftaran-jenis_pelajar').change(function(){
        var id = $(this).val();
        $.ajax({
            type: 'GET',
            url: 'filteryuran',
            data : {id: id},
            success: function(data) {
                $("select#maklumatpelajarpenjaga-tahun_mula").html(data);
            }
        });
    });
});
JS;
$this->registerJs($script);

?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('saveerror')) { ?>
            <div class="alert alert-danger" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('saveerror'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<?php $form = ActiveForm::begin([
    'options'=>[
        'id'=>'myForm'
    ]
]); ?>
    <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>

<div class="row">
    <div class="col-12 col-lg-12">
        <div class="row">
            <div class="col-md-12 mb-4">
                <div class="card box-bayang">
                    <div class="card-body">
                        <h5 class="card-title">Adakah Pelajar atau Ibu Bapa/Penjaga mahu membayar yuran pendaftaran ?</h5>
                        <div class="separator mb-4"></div>
                        <div class="custom-control custom-radio form-check-inline" style="display: inline-flex">
                            <input type="radio" id="customRadio3" name="optionyuran" class="custom-control-input" value="Ya" required="">
                            <label class="custom-control-label" for="customRadio3">Ya</label>
                        </div>
                        <div class="custom-control custom-radio form-check-inline" style="display: inline-flex">
                            <input type="radio" id="customRadio4" name="optionyuran" class="custom-control-input" value="Tidak" required="">
                            <label class="custom-control-label" for="customRadio4">Tidak</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mb-4">
                <div class="card box-bayang">
                    <div class="card-body">
                        <h5 class="card-title">Maklumat Pelajar</h5> 
                        <div class="separator mb-5"></div>
                        <span><strong><i>Pilih nombor mykid atau nombor passport pelajar.</i></strong></span>
                        <div class="">
                            <div class="mb-4">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="optionidentity" class="custom-control-input" value="mykid">
                                    <label class="custom-control-label" for="customRadio1">No Mykid</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" name="optionidentity" class="custom-control-input" value="passport">
                                    <label class="custom-control-label" for="customRadio2">No Passport / No Surat Beranak</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="display: none" id="divmykid">
                            <label class="control-label">No Mykid</label>

                            <?= $form->field($model, 'no_mykid')->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '999999-99-9999',
                                // 'options' => ['class' => 'form-control','style'=>'display:none']
                            ])->label(false) ?>
                        </div>
                        
                        <div class="form-group" style="display: none" id="divpassport">
                            <label class="control-label">No Passport/No Surat Beranak</label>
                            <div>
                                <?=  $form->field($model, 'no_mykid')->textInput(['maxlength' => true,'id'=>'passport','autocomplete'=>'off'])->label(false) ?>
                            </div>
                        </div>
                        <div style="display:none" class="pelajarDiv">
                            <div class="alert alert-danger showmessage">
                                
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Pusat Pendaftaran <span class="font-red">**</span></label>
                                <?= $form->field($model3, 'pusat_pendaftaran')->dropDownList($pendaftaran, ['prompt' => '--Sila Pilih--'])->label(false) ?>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Pusat Pengajian <span class="font-red">**</span></label>
                                <?= $form->field($model, 'pusat_pengajian_id')->dropDownList($pengajian, ['prompt' => '--Sila Pilih--'])->label(false) ?>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Sesi Pengajian </label>
                                <?= $form->field($model, 'sesi_pengajian')->dropDownList([ 'Pagi' => 'Pagi', 'Petang' => 'Petang', ], ['prompt' => '--Sila Pilih--'])->label(false) ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Nama Pelajar <span class="font-red">**</span></label>
                                <?= $form->field($model, 'nama_pelajar')->textInput(['maxlength' => true,'style'=>'text-transform:uppercase','placeholder'=>'Nama Pelajar','autocomplete'=>'off'])->label(false) ?>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Tarikh Lahir</label>
                                <?= $form->field($model, 'tarikh_lahir')->textInput(['maxlength' => true,'class'=>'form-control datepicker','data-date-format'=>'yyyy-mm-dd','autocomplete'=>'off','placeholder'=>'Pilih Tarikh Lahir'])->label(false) ?>
                            </div>
                            
                            <div class="form-group col-md-4">
                                <label>No Surat Beranak</label>
                                <?= $form->field($model, 'no_surat_beranak')->textInput(['maxlength' => true,'placeholder'=>'Nombor Surat Beranak'])->label(false) ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Jantina <span class="font-red">**</span></label>
                                <?= $form->field($model, 'jantina')->dropDownList([ 'Lelaki' => 'Lelaki', 'Perempuan' => 'Perempuan', ], ['prompt' => '--Sila Pilih--'])->label(false) ?>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Warganegara <span class="font-red">**</span></label>
                                <?= $form->field($model, 'warganegara')->dropDownList([ 'Warganegara' => 'Warganegara', 'Bukan Warganegara' => 'Bukan Warganegara', ], ['prompt' => '--Sila Pilih--'])->label(false) ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Tarikh Masuk <span class="font-red">**</span></label>
                                <?= $form->field($model, 'tarikh_masuk')->textInput(['maxlength' => true,'class'=>'form-control datepicker','data-date-format'=>'yyyy-mm-dd','autocomplete'=>'off'])->label(false) ?>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Jenis Pelajar <span class="font-red">**</span></label>
                                <?= $form->field($model2,'jenis_pelajar')->dropDownList([
                                    '1'=>'Pelajar Baru',
                                    '2'=>'Pelajar Lama',
                                    '3'=>'Pelajar Lewat',
                                ],
                                [
                                    'prompt'=>'--Sila Pilih Jenis Pelajar--',
                                ]
                                )->label(false) ?>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Tahap Mula/Tahap Semasa <span class="font-red">**</span></label>
                                <?= $form->field($model, 'tahun_mula')->dropDownList([ 
                                    '1' => '1', '2' => '2', '3' => '3', '4' => '4','5' => '5', '6' => '6',
                                    ], 
                                    ['prompt' => '--Sila Pilih--'])->label(false);
                                ?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <?= $form->field($model, 'tempat_lahir')->textarea(['rows' => 6]) ?>
                        </div>
                        
                        <?= $form->field($model, 'alamat_rumah')->textarea(['rows' => 6]) ?>

                        <?= $form->field($model, 'SPRA')->dropDownList([ 'Ya' => 'Ya', 'Tidak' => 'Tidak', ], ['prompt' => '--Sila Pilih--']) ?>

                        <?= $form->field($model, 'PSRA')->dropDownList([ 'Ya' => 'Ya', 'Tidak' => 'Tidak', ], ['prompt' => '--Sila Pilih--']) ?>

                        
                        
                        <div class="form-group">
                            <label class="control-label">Negeri</label>
                                <?= $form->field($model, 'state_id')->dropDownList($state,
                                        [
                                            'onchange'=>'$.post( "'.Yii::$app->urlManager->createUrl(['hr/pelajar/maklumat-pelajar-penjaga/listdaerah','id'=>'']).'"+$(this).val(), function( data ) {$( "select#daerah" ).html( data );});',
                                            'prompt'=>'--Sila Pilih--','id'=>'',
                                        ]
                                    )->label(false);
                                ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Daerah</label>
                            <?= $form->field($model, 'district_id')->dropDownList($district, 
                                [
                                    'prompt'=>'--Sila Pilih Negeri--','id'=>'daerah',   
                                    'class'=>'form-control'
                                ])->label(false); 
                            ?>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-12 mb-4">
                <div class="card box-bayang">
                    <div class="card-body">
                        <h5 class="card-title">Maklumat Bapa / Penjaga</h5>
                        <div class="separator mb-5"></div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Nama Bapa/Penjaga</label>
                                <?= $form->field($model, 'nama_bapa')->textInput(['maxlength' => true,'style'=>'text-transform:uppercase','placeholder'=>'Nama Bapa/Penjaga','autocomplete'=>'off'])->label(false) ?>
                            </div>
                            <div class="form-group col-md-4">
                                <label>No Kad Pengenalan Bapa/Penjaga</label>
                                <?= $form->field($model, 'no_kad_pengenalan_bapa')->textInput(['maxlength' => true,'autocomplete'=>'off'])->label(false) ?>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Email Bapa/Penjaga</label>
                                <?= $form->field($model, 'email_bapa_penjaga')->textInput(['maxlength' => true,'autocomplete'=>'off'])->label(false) ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Pekerjaan Bapa/Penjaga</label>
                                <?= $form->field($model, 'pekerjaan_bapa_penjaga')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Gaji Bapa/Penjaga</label>
                                <?= $form->field($model, 'gaji_bapa')->dropDownList(
                                    $pendapatan, 
                                [
                                    'prompt' => '--Sila Pilih--',

                                ]
                                )->label(false) ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>No Tel Bapa/Penjaga</label>
                                 <?= $form->field($model, 'no_tel_bapa')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                            <div class="form-group col-md-6">
                                <label>No Handphone Bapa/Penjaga</label>
                                 <?= $form->field($model, 'no_hp_bapa')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <?= $form->field($model, 'alamat_majikan_bapa_penjaga')->textarea(['rows' => 6]) ?>

                        <h5 class="card-title">Maklumat Ibu</h5>
                        <div class="separator mb-5"></div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Nama Ibu</label>
                                <?= $form->field($model, 'nama_ibu')->textInput(['maxlength' => true,'style'=>'text-transform:uppercase','autocomplete'=>'off'])->label(false) ?>
                            </div>
                            <div class="form-group col-md-4">
                                <label>No Kad Pengenalan Ibu</label>
                                <?= $form->field($model, 'no_kad_pengenalan_ibu')->textInput(['maxlength' => true,'autocomplete'=>'off'])->label(false) ?>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Email Ibu Penjaga</label>
                                <?= $form->field($model, 'email_ibu_penjaga')->textInput(['maxlength' => true,'autocomplete'=>'off'])->label(false) ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Pekerjaan Ibu</label>
                                <?= $form->field($model, 'pekerjaan_ibu')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Gaji Ibu</label>
                                <?= $form->field($model, 'gaji_ibu')->dropDownList(
                                    $pendapatan, 
                                [
                                    'prompt' => '--Sila Pilih--',

                                ])->label(false) ?>
                            </div>
                        </div>
                        <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label>No Tel Ibu</label>
                                 <?= $form->field($model, 'no_tel_ibu')->textInput(['maxlength' => true])->label(false) ?>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>No Handphone Ibu</label>
                                <?= $form->field($model, 'no_hp_ibu')->textInput(['maxlength' => true])->label(false) ?>
                             </div>
                        </div>
                        <?= $form->field($model, 'alamat_majikan_ibu')->textarea(['rows' => 6]) ?>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card box-bayang">
                    <div class="card-body">
                        <h5 class="card-title"> PENGAKUAN IBU BAPA / PENJAGA </h5>
                        <div class="separator mb-5"></div>
                        <p>Saya Ibu Bapa/penjaga kanak-kanak di atas,bersetuju menyerahkannya mengikuti <b>KELAS TAHFIZ UMMAH BANDAR BARU BANGI</b> yang dikelolakan oleh <b>ABIM</b>. Saya juga mengaku bersedia memberikan sumbangan yuran bulanan seperti yang telah ditetapkan dan saya juga akan mematuhi semua peraturan yang telah ditetapkan selama kanak-kanak tersebut mengikuti <b>KELAS TAHFIZ UMMAH BANDAR BARU BANGI</b>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mb-5"></div>
    
<div>
    <?= Html::submitButton('Simpan', ['class' => 'btn btn-success btn-block mb-1 exist']) ?>
    <?= Html::a('Batal',['index'],['class'=>'btn btn-secondary btn-block mb-1']) ?>
    
</div>
<?php ActiveForm::end(); ?>