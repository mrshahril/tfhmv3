<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\hr\pelajar\models\MaklumatPelajarPenjaga */

$this->title = 'Maklumat Terperinci';
$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('maklumat-pelajar-penjaga/view')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    function init_click_handlers(){
        $('.modaltest').click(function(){
            $('#exampleModal').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));

        });
    }
    
    init_click_handlers(); //first run
    deleteswal();
    $("#some_pjax_id").on("pjax:success", function() {
      init_click_handlers(); //reactivate links in grid after pjax update
      deleteswal();
    });

    function deleteswal(){
        $('a.delete').on('click', function(e){
             e.preventDefault();
            var link = $(this).attr('value');
            var myId = $(this).attr('id');
            if(myId == 'softdelete'){
                var title = 'Anda pasti mahu hapus maklumat pelajar ini ?';
            }
            else{
                var title = 'Anda pasti mahu pindah pelajar ini ke alumni ?';
            }
            
            console.log(link);
          swal({   
            title: title,
            // text: "Perhatian ! Proses ini tidak boleh diulang semua !",   
            type: "warning",   
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: 'Ya, saya pasti.',
            confirmButtonClass: 'btn-info',
            cancelButtonText: 'Batal',
            cancelButtonClass: 'btn-danger',

          }, 
           function(){   
                // $("#myform").submit();
                $.ajax({
                    type: 'POST',
                    url: link,
                    success: function(data) {
                    
                   }
                });
            });
        })
    }
});
JS;
$this->registerJs($script);
?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg mr-1']) ?>
                <button class="btn btn-outline-primary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Pilih Tindakan
                </button>
                <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -105px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <?= Html::a('Tambah Baru',['create'],['class'=>'dropdown-item']) ?>
                    <?= Html::a('Pindah Ke Alumni',FALSE,['class'=>'dropdown-item delete','value'=>Url::to(['change_alumni','id'=>base64_encode($model->id)])]) ?>
                    <?= Html::a('Kemaskini Maklumat',['update','id'=>base64_encode($model->id)],['class'=>'dropdown-item']) ?>
                    <?= Html::a('Hapus',FALSE,['class'=>'dropdown-item delete','id'=>'softdelete','value'=>Url::to(['deactivated','id'=>base64_encode($model->id)])]) ?>
                </div>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-12 mb-4">
        <div class="box box-danger">
            <div class="card-body">
                <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'nama_pelajar',
                            'jantina',
                            'tarikh_lahir',
                            'tempat_lahir:ntext',
                            'no_surat_beranak',
                            'no_mykid',
                            'nama_tahfiz.pusat_pengajian',
                            'sesi_pengajian',
                            'tarikh_masuk',
                            'tahun_mula',
                            [
                                'attribute'=>'alamat_rumah',
                                'format'=>'raw',    
                                'value' => strip_tags($model->alamat_rumah)
                            ],
                            'warganegara',
                            
                            [
                                'attribute'=>'alumni',
                                'format'=>'raw',    
                                'value' => $model->alumni == 0 ? 'Tidak' : 'Ya'
                            ],
                            'nama_bapa',
                            'no_kad_pengenalan_bapa',
                            'pekerjaan_bapa_penjaga',
                            'no_tel_bapa',
                            'no_hp_bapa',
                            'alamat_majikan_bapa_penjaga:ntext',
                            [
                                'attribute'=>'Pendapatan Bapa',
                                'format'=>'raw',    
                                'value' => $model->gaji_bapa ? $model->pendapatanbapa->pendapatan : ''
                            ],
                            [
                                'attribute'=>'Pendapatan Ibu',
                                'format'=>'raw',    
                                'value' => $model->gaji_ibu ? $model->pendapatanibu->pendapatan : ''
                            ],
                            'no_kad_pengenalan_ibu',
                            'pekerjaan_ibu',
                            'no_tel_ibu',
                            'no_hp_ibu',
                            'alamat_majikan_ibu:ntext',
                            
                            'negeri.state',
                            'daerah.district'
                        ],
                    ]) ?>
            </div>
        </div>
    </div>
</div>
