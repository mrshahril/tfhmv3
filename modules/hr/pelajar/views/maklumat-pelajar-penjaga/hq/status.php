<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use app\models\LookupPusatPengajian;

$this->title = 'Kemaskini Status Pelajar';

$pengajian = ArrayHelper::map(LookupPusatPengajian::find()->asArray()->all(), 'id', 'pusat_pengajian');

$script = <<< JS
$(document).ready(function(){
	if($('#pelajar').hasClass('maklumat-pelajar-penjaga/status')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }
});
JS;
$this->registerJs($script);
?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['pending'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('saveerror')) { ?>
            <div class="alert alert-danger" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('saveerror'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-12 col-lg-12">
        <div class="box box-danger">
        	<?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
            	<div class="col-md-12">
            		<h5 class="mb-4">Pilihan Pusat Pengajian</h5>
            		<table class="table table-striped">
            			<tr>
            				<td>Pilihan Pertama</td>
            				<td><?= $model2->nama_pusat_pengajian_pertama ? $model2->pertama->pusat_pengajian : '' ?></td>
            			</tr>
            			<tr>
            				<td>Pilihan Kedua</td>
            				<td><?= $model2->nama_pusat_pengajian_kedua ? $model2->kedua->pusat_pengajian : '' ?></td>
            			</tr>
            			<tr>
            				<td>Pilihan Ketiga</td>
            				<td><?= $model2->nama_pusat_pengajian_ketiga ? $model2->ketiga->pusat_pengajian : '' ?></td>
            			</tr>
            		</table>
            	</div>
            	<div class="separator mb-5"></div>
            	<?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>
                <form>
                     <div class="form-row">
                     	<div class="form-group col-md-6">
                     		<label>Pusat Pengajian</label>
                     		<?= $form->field($model, 'pusat_pengajian_id')->dropDownList($pengajian, ['prompt' => '--Sila Pilih--'])->label(false) ?>
                        </div>
                        <div class="form-group col-md-6">
                     		<label>Sesi Pengajian</label>
                     		<?= $form->field($model, 'sesi_pengajian')->dropDownList([ 'Pagi' => 'Pagi', 'Petang' => 'Petang', ], ['prompt' => '--Sila Pilih--'])->label(false) ?>
                        </div>
                     </div>
                     <div class="form-row">
                     	<div class="form-group col-md-6">
                     		<label>Status</label>
                     		<?= $form->field($model, 'status')->dropDownList([ 'Approved' => 'Approved', 'Pending' => 'Pending', ], ['prompt' => '--Sila Pilih--'])->label(false) ?>
                        </div>
                        <div class="form-group col-md-6">
                     		<label>Tahun Lewat</label>
                     		<?= $form->field($model, 'tahun_lewat')->dropDownList([ '0' => '--Sila Pilih--', '1' => 'Ya','2' => 'Tidak' ])->label(false) ?>
                        </div>
                     </div>
                </form>
                <div class="box-footer">
                    <div class="col-md-12">
                        <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>

                        <?= Html::submitButton('Kemaskini', ['class' => 'btn btn-success mb-1']) ?>

                        <?= Html::a('Batal',['pending'],['class'=>'btn btn-danger mb-1']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>            
        </div>
    </div>
</div>