<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use app\models\LookupState;
use app\models\LookupDistrict;
use app\models\LookupPendapatan;
use app\models\LookupPusatPengajian;
/* @var $this yii\web\View */
/* @var $model app\modules\hr\pelajar\models\MaklumatPelajarPenjaga */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Kemaskini Pelajar Pending';

$pendapatan = ArrayHelper::map(LookupPendapatan::find()->asArray()->all(), 'id', 'pendapatan');
$pengajian = ArrayHelper::map(LookupPusatPengajian::find()->asArray()->all(), 'id', 'pusat_pengajian');
$state = ArrayHelper::map(LookupState::find()->asArray()->all(), 'state_id', 'state');
$district = ArrayHelper::map(LookupDistrict::find()->where(['state_id'=>$model->state_id])->asArray()->all(), 'district_id', 'district');

$link = Yii::$app->urlManager->baseUrl;

$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('maklumat-pelajar-penjaga/updatepending')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

   $('#customRadio1').click(function(){
        $('#divmykid').show();
        $('#divpassport').hide();
        $('#maklumatpelajarpenjaga-no_mykid').prop('disabled', false);
        $('#passport').prop('disabled', true);

    })
    $('#customRadio2').click(function(){
        $('#divmykid').hide();
        $('#divpassport').show();
        $('#maklumatpelajarpenjaga-no_mykid').prop('disabled', true);
        $('#passport').prop('disabled', false);
    })

    if($('#customRadio1').is(':checked')){
        $('#divmykid').show();
        $('#divpassport').hide();
        $('#maklumatpelajarpenjaga-no_mykid').prop('disabled', false);
        $('#passport').prop('disabled', true);
    }
    else{
        $('#divmykid').hide();
        $('#divpassport').show();
        $('#maklumatpelajarpenjaga-no_mykid').prop('disabled', true);
        $('#passport').prop('disabled', false);
    }
});
JS;
$this->registerJs($script);

?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['pending'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('saveerror')) { ?>
            <div class="alert alert-danger" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('saveerror'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>

<div class="row">
    <div class="col-12 col-lg-12">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Maklumat Pelajar</h5> 
                        <div class="separator mb-5"></div>
                        <span><strong><i>Pilih nombor mykid atau nombor passport pelajar.</i></strong></span>
                        <div class="">
                            <div class="mb-4">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="optionidentity" class="custom-control-input" value="mykid" <?php if(preg_match("/[a-z]/i", $model->no_mykid)){
                                        echo 'checked';
                                    }else{ echo 'checked';} ?>>
                                    <label class="custom-control-label" for="customRadio1">No Mykid</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" name="optionidentity" class="custom-control-input" value="passport" <?php if(preg_match("/[a-z]/i", $model->no_mykid)){
                                        echo 'checked=""';
                                    }?>>
                                    <label class="custom-control-label" for="customRadio2">No Passport / No Surat Beranak</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="display: none" id="divmykid">
                            <label class="control-label">No Mykid</label>

                            <?= $form->field($model, 'no_mykid')->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '999999-99-9999',
                                // 'options' => ['class' => 'form-control','style'=>'display:none']
                            ])->label(false) ?>
                        </div>
                        
                        <div class="form-group" style="display: none" id="divpassport">
                            <label class="control-label">No Passport/No Surat Beranak</label>
                            <div>
                                <?=  $form->field($model, 'no_mykid')->textInput(['maxlength' => true,'id'=>'passport','autocomplete'=>'off'])->label(false) ?>
                            </div>
                        </div>
                        <div style="display:none" class="pelajarDiv">
                            <div class="alert alert-danger showmessage">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Pusat Pendaftaran <span class="font-red">**</span></label>
                            <?= $form->field($model3, 'pusat_pendaftaran')->dropDownList($pengajian, ['prompt' => '--Sila Pilih--'])->label(false) ?>
                        </div>
                        <div class="form-group">
                            <label>Nama Pelajar <span class="font-red">**</span></label>
                            <?= $form->field($model, 'nama_pelajar')->textInput(['maxlength' => true,'style'=>'text-transform:uppercase','placeholder'=>'Nama Pelajar'])->label(false) ?>
                        </div>
                        <div class="form-group">
                            <label>Jantina</label>
                            <?= $form->field($model, 'jantina')->dropDownList([ 'Lelaki' => 'Lelaki', 'Perempuan' => 'Perempuan', ], ['prompt' => '--Sila Pilih--'])->label(false) ?>
                        </div>
                        <div class="form-group">
                            <label>Tarikh Lahir</label>
                            <?= $form->field($model, 'tarikh_lahir')->textInput(['maxlength' => true,'class'=>'form-control datepicker','data-date-format'=>'yyyy-mm-dd','autocomplete'=>'off','placeholder'=>'Pilih Tarikh Lahir'])->label(false) ?>
                        </div>
                        <div class="form-group">
                            <?= $form->field($model, 'tempat_lahir')->textarea(['rows' => 6]) ?>
                        </div>
                        <?= $form->field($model, 'no_surat_beranak')->textInput(['maxlength' => true,'placeholder'=>'Nombor Surat Beranak']) ?>

                        <?= $form->field($model, 'tarikh_masuk')->textInput(['maxlength' => true,'class'=>'form-control datepicker','data-date-format'=>'yyyy-mm-dd']) ?>
                        <?= $form->field($model, 'tahun_mula')->dropDownList([ 
                                '1' => '1', '2' => '2', '3' => '3', '4' => '4','5' => '5', '6' => '6',
                            ], 
                            ['prompt' => '--Sila Pilih--']) 
                        ?>
                        <?= $form->field($model, 'alamat_rumah')->textarea(['rows' => 6]) ?>

                        <?= $form->field($model, 'SPRA')->dropDownList([ 'Ya' => 'Ya', 'Tidak' => 'Tidak', ], ['prompt' => '--Sila Pilih--']) ?>

                        <?= $form->field($model, 'PSRA')->dropDownList([ 'Ya' => 'Ya', 'Tidak' => 'Tidak', ], ['prompt' => '--Sila Pilih--']) ?>

                        <?= $form->field($model, 'warganegara')->dropDownList([ 'Warganegara' => 'Warganegara', 'Bukan Warganegara' => 'Bukan Warganegara', ], ['prompt' => '--Sila Pilih--']) ?>
                        <div class="form-group">
                            <label class="control-label">Negeri</label>
                                <?= $form->field($model, 'state_id')->dropDownList($state,
                                        [
                                            'onchange'=>'$.post( "'.Yii::$app->urlManager->createUrl(['hr/pelajar/maklumat-pelajar-penjaga/listdaerah','id'=>'']).'"+$(this).val(), function( data ) {$( "select#daerah" ).html( data );});',
                                            'prompt'=>'--Sila Pilih--','id'=>'',
                                        ]
                                    )->label(false);
                                ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Daerah</label>
                            <?= $form->field($model, 'district_id')->dropDownList($district, 
                                [
                                    'prompt'=>'--Sila Pilih Negeri--','id'=>'daerah',   
                                    'class'=>'form-control'
                                ])->label(false); 
                            ?>
                        </div>
                        <?= $form->field($model2, 'nama_pusat_pengajian_pertama')->dropDownList($pengajian, ['prompt' => '--Sila Pilih--']) ?>

                        <?= $form->field($model2, 'nama_pusat_pengajian_kedua')->dropDownList($pengajian, ['prompt' => '--Sila Pilih--']) ?>

                        <?= $form->field($model2, 'nama_pusat_pengajian_ketiga')->dropDownList($pengajian, ['prompt' => '--Sila Pilih--']) ?>

                        <?= $form->field($model2, 'sessi_pengajian')->dropDownList([ 'Pagi' => 'Pagi', 'Petang' => 'Petang', ], ['prompt' => '--Sila Pilih--']) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Maklumat Ibu Bapa / Penjaga</h5>
                        <div class="separator mb-5"></div>
                        <?= $form->field($model, 'nama_bapa')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'no_kad_pengenalan_bapa')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'pekerjaan_bapa_penjaga')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'no_tel_bapa')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'no_hp_bapa')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'alamat_majikan_bapa_penjaga')->textarea(['rows' => 6]) ?>

                        <?= $form->field($model, 'gaji_bapa')->dropDownList(
                            $pendapatan, 
                        [
                            'prompt' => '--Sila Pilih--',

                        ]
                        ) ?>

                        <?= $form->field($model, 'nama_ibu')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'no_kad_pengenalan_ibu')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'pekerjaan_ibu')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'no_tel_ibu')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'no_hp_ibu')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'alamat_majikan_ibu')->textarea(['rows' => 6]) ?>

                        <?= $form->field($model, 'gaji_ibu')->dropDownList(
                            $pendapatan, 
                        [
                            'prompt' => '--Sila Pilih--',

                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    <?= Html::submitButton('Simpan', ['class' => 'btn btn-success btn-block mb-1']) ?>
    <?= Html::a('Batal',['pending'],['class'=>'btn btn-secondary btn-block mb-1']) ?>
    
</div>
<?php ActiveForm::end(); ?>