<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\hr\pelajar\models\MaklumatPelajarPenjagaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Senarai Keseluruhan Pelajar';
$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
$(document).ready(function(){
    if($('#pelajar').hasClass('maklumat-pelajar-penjaga/all')){
        $('.main-menu > .scroll > ul > li#hr').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    function init_click_handlers(){
        $('.modaltest').click(function(){
            $('#exampleModal').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));

        });
    }
    
    init_click_handlers(); //first run
    deleteswal();
    $("#some_pjax_id").on("pjax:success", function() {
      init_click_handlers(); //reactivate links in grid after pjax update
      deleteswal();
    });

    function deleteswal(){
        $('a.delete').on('click', function(e){
             e.preventDefault();
            var link = $(this).attr('value');
            var myId = $(this).attr('id');
            if(myId == 'softdelete'){
                var title = 'Anda pasti mahu hapus maklumat pelajar ini ?';
            }
            else{
                var title = 'Anda pasti mahu pindah pelajar ini ke alumni ?';
            }
            
            console.log(link);
          swal({   
            title: title,
            // text: "Perhatian ! Proses ini tidak boleh diulang semua !",   
            type: "warning",   
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: 'Ya, saya pasti.',
            confirmButtonClass: 'btn-info',
            cancelButtonText: 'Batal',
            cancelButtonClass: 'btn-danger',

          }, 
           function(){   
                // $("#myform").submit();
                $.ajax({
                    type: 'POST',
                    url: link,
                    success: function(data) {
                    
                   }
                });
            });
        })
    }
});
JS;
$this->registerJs($script);
?>
<span id="pelajar" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Tambah Baru',['create'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?= $this->render('_search', ['model' => $searchModel]);  ?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body table-responsive">
                <?php Pjax::begin(['id'=>'some_pjax_id']); ?>    
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'layout' => "{summary}\n{items}\n<nav class='mt-4 mb-3'>{pager}</nav>",
                    'pager' => [
                        'firstPageLabel' => 'Mula',
                        'lastPageLabel' => 'Akhir',
                        'prevPageLabel' => 'Sebelumnya',
                        'nextPageLabel' => 'Seterusnya',

                        'maxButtonCount' => 5,

                         'options' => [
                            'tag' => 'ul',
                            'class' => 'pagination justify-content-center mb-0',
                        ],
                        'linkContainerOptions'=>['class'=>'page-item'],
                        'linkOptions' => ['class' => 'page-link'],
                        'activePageCssClass' => 'active',
                    ],
                    'tableOptions'=>['class'=>'table table-striped'],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'nama_pelajar',
                        'tarikh_masuk',
                        'no_mykid',
                        'nama_tahfiz.pusat_pengajian',
                        [
                            'attribute'=>'alumni',
                            'format'=>'raw',
                            'value'=>function($data){
                                $data->getStatusalumni($data->alumni);
                                if ($data->alumni == 0) {
                                    $color = 'btn-success';
                                }
                                elseif ($data->alumni == 1) {
                                    $color = 'btn-danger';
                                }

                                $html = '<button class="btn '.$color.' btn-sm">'.$data->statusdesc.'</button>';
                                return $html;
                            }
                        ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>