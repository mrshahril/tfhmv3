<?php

namespace app\modules\hr\pelajar\models;

use Yii;

use app\models\LookupKelas;
use app\modules\hr\staff\models\MaklumatKakitangan;
/**
 * This is the model class for table "kelas_pelajar".
 *
 * @property int $id
 * @property int $id_kelas
 * @property int $id_staf
 * @property int $id_pelajar
 * @property int $tahun_update
 * @property int $guru_kanan
 * @property int $id_tahfiz
 * @property int $enter_by
 * @property string $created_at
 * @property string $updated_at
 * @property int $updated_by
 */
class KelasPelajar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kelas_pelajar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kelas', 'id_staf', 'id_pelajar', 'tahun_update', 'guru_kanan', 'id_tahfiz', 'enter_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_kelas' => 'Kelas',
            'id_staf' => 'Guru Kelas',
            'id_pelajar' => 'Id Pelajar',
            'tahun_update' => 'Tahun Semasa',
            'guru_kanan' => 'Guru Kanan',
            'id_tahfiz' => 'Id Tahfiz',
            'enter_by' => 'Enter By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function getNamapelajar()
    {
        return $this->hasOne(MaklumatPelajarPenjaga::className(), ['id' => 'id_pelajar']);
    }

    public function getNamaguru()
    {
        return $this->hasOne(MaklumatKakitangan::className(), ['id_staf' => 'id_staf']);
    }

    public function getKelas()
    {
        return $this->hasOne(LookupKelas::className(),['id'=>'id_kelas']);
    }

    public static function janakelas()
    {
        $model = KelasPelajar::find()
            ->select('id_pelajar')
            ->where(['tahun_update'=>date('Y')]);

        $model2 = MaklumatPelajarPenjaga::find()
                ->where(['pusat_pengajian_id'=>Yii::$app->user->identity->tahfiz])
                ->andWhere(['alumni'=>0])
                ->andWhere(['status_deleted'=>2])
                ->andWhere(['NOT IN','id',$model])
                ->count();

        return $model2;
    }
}
