<?php

namespace app\modules\hr\pelajar\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\hr\pelajar\models\SubsidiPelajar;

/**
 * SubsidiPelajarSearch represents the model behind the search form of `app\modules\hr\pelajar\models\SubsidiPelajar`.
 */
class SubsidiPelajarSearch extends SubsidiPelajar
{
    /**
     * {@inheritdoc}
     */
    public $globalsubs;
    public function rules()
    {
        return [
            [['id', 'id_pelajar', 'tahun', 'enter_by', 'bilangan_tanggungan', 'tahap_semasa'], 'integer'],
            [['date_add', 'date_update', 'category_subsidi', 'date_apply', 'status', 'no_siri','globalsubs'], 'safe'],
            [['peruntukan'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubsidiPelajar::find();
        $query->joinWith('namapelajar');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_pelajar' => $this->id_pelajar,
            'tahun' => $this->tahun,
            'enter_by' => $this->enter_by,
            'bilangan_tanggungan' => $this->bilangan_tanggungan,
            'peruntukan' => $this->peruntukan,
            'tahap_semasa' => $this->tahap_semasa,
        ]);

        $query->orFilterWhere(['like', 'subsidi_pelajar.category_subsidi', $this->globalsubs])
            ->orFilterWhere(['like', 'subsidi_pelajar.date_apply', $this->globalsubs])
            ->orFilterWhere(['like', 'subsidi_pelajar.status', $this->globalsubs])
            ->orFilterWhere(['like', 'maklumat_pelajar_penjaga.nama_pelajar', $this->globalsubs])
            ->orFilterWhere(['like', 'no_siri', $this->globalsubs]);

        return $dataProvider;
    }
}
