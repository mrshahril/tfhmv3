<?php

namespace app\modules\hr\pelajar\models;

use Yii;

/**
 * This is the model class for table "subsidi_pelajar".
 *
 * @property int $id
 * @property int $id_pelajar
 * @property int $tahun
 * @property string $date_add
 * @property string $date_update
 * @property int $enter_by
 * @property string $category_subsidi
 * @property string $date_apply
 * @property string $status
 * @property int $bilangan_tanggungan
 * @property string $no_siri
 * @property string $peruntukan
 * @property int $tahap_semasa
 */
class SubsidiPelajar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subsidi_pelajar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pelajar', 'tahun', 'enter_by', 'bilangan_tanggungan', 'tahap_semasa','update_by'], 'integer'],
            [['peruntukan'], 'number'],
            [['date_add', 'date_apply'], 'string', 'max' => 20],
            [['date_update'], 'string', 'max' => 50],
            [['category_subsidi'], 'string', 'max' => 150],
            [['status'], 'string', 'max' => 100],
            [['no_siri'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pelajar' => 'Id Pelajar',
            'tahun' => 'Tahun',
            'date_add' => 'Date Add',
            'date_update' => 'Date Update',
            'enter_by' => 'Enter By',
            'category_subsidi' => 'Kategori Subsidi',
            'date_apply' => 'Date Apply',
            'status' => 'Status',
            'bilangan_tanggungan' => 'Bilangan Tanggungan',
            'no_siri' => 'No Siri',
            'peruntukan' => 'Peruntukan Subsidi (RM)',
            'tahap_semasa' => 'Tahap Semasa',
            'date_apply'=>'Tarikh Memohon'
        ];
    }

    public function getNamapelajar()
    {
        return $this->hasOne(MaklumatPelajarPenjaga::className(), ['id' => 'id_pelajar']);
    }
}
