<?php

namespace app\modules\hr\pelajar\models;

use Yii;
use app\models\LookupPusatPengajian;

/**
 * This is the model class for table "maklumat_pilihan_pusat_pengajian".
 *
 * @property integer $id
 * @property integer $nama_pusat_pengajian_pertama
 * @property integer $nama_pusat_pengajian_kedua
 * @property integer $nama_pusat_pengajian_ketiga
 * @property string $sessi_pengajian
 * @property string $id_pelajar
 */
class MaklumatPilihanPusatPengajian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'maklumat_pilihan_pusat_pengajian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['nama_pusat_pengajian_pertama', 'required', 'message' => 'Ruangan {attribute} wajib diisi'],

            [['nama_pusat_pengajian_pertama', 'nama_pusat_pengajian_kedua', 'nama_pusat_pengajian_ketiga'], 'integer'],
            [['id_pelajar','sessi_pengajian'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_pusat_pengajian_pertama' => 'Pilih Pusat Pengajian Pertama',
            'nama_pusat_pengajian_kedua' => 'Pilih Pusat Pengajian Kedua',
            'nama_pusat_pengajian_ketiga' => 'Pilih Pusat Pengajian Ketiga',
            'sessi_pengajian' => 'Sesi Pengajian',
            'id_pelajar' => 'Id Pelajar',
        ];
    }


    public function getPertama()
    {
        return $this->hasOne(LookupPusatPengajian::className(), ['id' => 'nama_pusat_pengajian_pertama']);
    }
    public function getKedua()
    {
        return $this->hasOne(LookupPusatPengajian::className(), ['id' => 'nama_pusat_pengajian_kedua']);
    }
    public function getKetiga()
    {
        return $this->hasOne(LookupPusatPengajian::className(), ['id' => 'nama_pusat_pengajian_ketiga']);
    }
    
}
