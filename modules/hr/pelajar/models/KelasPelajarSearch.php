<?php

namespace app\modules\hr\pelajar\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\hr\pelajar\models\KelasPelajar;

/**
 * KelasPelajarSearch represents the model behind the search form of `app\modules\hr\pelajar\models\KelasPelajar`.
 */
class KelasPelajarSearch extends KelasPelajar
{
    /**
     * {@inheritdoc}
     */

    public $globalkelas;

    public function rules()
    {
        return [
            [['id', 'id_kelas', 'id_staf', 'id_pelajar', 'tahun_update', 'guru_kanan', 'id_tahfiz', 'enter_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at','globalkelas'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KelasPelajar::find();
        $query->joinWith('namapelajar');
        $query->joinWith('namaguru');
        $query->joinWith('kelas');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_kelas' => $this->id_kelas,
            'id_staf' => $this->id_staf,
            'id_pelajar' => $this->id_pelajar,
            'tahun_update' => $this->tahun_update,
            'guru_kanan' => $this->guru_kanan,
            'id_tahfiz' => $this->id_tahfiz,
            'enter_by' => $this->enter_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->orFilterWhere(['like', 'maklumat_pelajar_penjaga.nama_pelajar', $this->globalkelas])
            ->orFilterWhere(['like', 'maklumat_pelajar_penjaga.no_mykid', $this->globalkelas])
            ->orFilterWhere(['like', 'lookup_kelas.kelas', $this->globalkelas])
            ->orFilterWhere(['like', 'maklumat_kakitangan.nama', $this->globalkelas]);

        return $dataProvider;
    }
}
