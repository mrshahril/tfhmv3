<?php

namespace app\modules\hr\pelajar\models;

use Yii;

/**
 * This is the model class for table "pusat_pendaftaran".
 *
 * @property integer $id
 * @property string $pusat_pengajian
 * @property integer $id_pelajar
 */
class PusatPendaftaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pusat_pendaftaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['pusat_pendaftaran', 'required', 'message' => 'Ruangan Pusat Pendaftaran wajib diisi'],
            [['id_pelajar'], 'integer'],
            [['pusat_pendaftaran'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pusat_pendaftaran' => 'Pusat Pendaftaran',
            'id_pelajar' => 'Id Pelajar',
        ];
    }
}
