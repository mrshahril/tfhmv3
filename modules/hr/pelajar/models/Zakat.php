<?php

namespace app\modules\hr\pelajar\models;

use Yii;

/**
 * This is the model class for table "zakat".
 *
 * @property int $id
 * @property int $id_pelajar
 * @property int $tahun
 * @property string $date_added
 * @property int $enter_by
 * @property string $jumlah_zakat
 * @property string $nota
 */
class Zakat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zakat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pelajar'],'required','message'=>'Ruangan {attribute} wajib diisi'],
            [['id_pelajar', 'tahun', 'enter_by','update_by'], 'integer'],
            [['jumlah_zakat'], 'number','message'=>'{attribute} mestilah nombor.'],
            [['date_added','date_update'], 'string', 'max' => 50],
            [['nota'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pelajar' => 'Nama Pelajar',
            'tahun' => 'Tahun',
            'date_added' => 'Date Added',
            'enter_by' => 'Enter By',
            'jumlah_zakat' => 'Jumlah Zakat',
            'nota' => 'Nota',
        ];
    }

    public function getNamapelajar()
    {
        return $this->hasOne(MaklumatPelajarPenjaga::className(), ['id' => 'id_pelajar']);
    }
}
