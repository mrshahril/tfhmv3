<?php

namespace app\modules\hr\pelajar\models;

use Yii;
use yii\helpers\Json;

use app\models\LookupPendapatan;
use app\models\LookupPusatPengajian;
use app\models\LookupState;
use app\models\LookupDistrict;
use app\models\User;

use app\models\LookupKelas;

use app\modules\hr\staff\models\MaklumatKakitangan;
/**
 * This is the model class for table "maklumat_pelajar_penjaga".
 *
 * @property int $id
 * @property string $nama_pelajar
 * @property string $jantina
 * @property string $tarikh_lahir
 * @property string $tempat_lahir
 * @property string $no_surat_beranak
 * @property string $no_mykid
 * @property int $pusat_pengajian_id
 * @property string $sesi_pengajian
 * @property string $tarikh_masuk
 * @property string $tahun_mula
 * @property string $alamat_rumah
 * @property string $SPRA
 * @property string $PSRA
 * @property string $nama_bapa
 * @property string $no_kad_pengenalan_bapa
 * @property string $pekerjaan_bapa_penjaga
 * @property string $no_tel_bapa
 * @property string $no_hp_bapa
 * @property string $alamat_majikan_bapa_penjaga
 * @property string $nama_ibu
 * @property string $no_kad_pengenalan_ibu
 * @property string $pekerjaan_ibu
 * @property string $no_tel_ibu
 * @property string $no_hp_ibu
 * @property string $alamat_majikan_ibu
 * @property string $status
 * @property int $gaji_bapa
 * @property int $gaji_ibu
 * @property string $warganegara
 * @property string $tahun_lewat
 * @property string $alumni
 * @property int $update_by
 * @property int $enter_by
 * @property string $date_update
 * @property string $date_create
 * @property int $kelas
 * @property int $state_id
 * @property int $district_id
 * @property int $id_staf
 * @property int $pusat_pendaftaran
 * @property int $tahap_semasa
 */
class MaklumatPelajarPenjaga extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    const ALUMNI = 1;
    const NON_ALUMNI = 0;
    const SOFT_DELETE = 0; // SOFT DELETE (will not show in ALL modules)
    const HARD_DELETE = 1; // CLEAR ALL INFORMATION FROM DATABASE
    const NOT_DELETE = 2; //  to show in ALL modules
    
    public $statusdesc;

    public static function tableName()
    {
        return 'maklumat_pelajar_penjaga';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //validation based on scenario
            [['kelas', 'id_staf'], 'required', 'on' => 'jana_kelas_pelajar'],
            [['pusat_pengajian_id'], 'required', 'on' => 'status_approval','message'=>'Ruangan {attribute} wajib diisi'],
            [['pusat_pengajian_id','nama_pelajar','jantina','tarikh_masuk','tahun_mula','warganegara'], 'required', 'on' => 'registerday','message'=>'Ruangan {attribute} wajib diisi'],
            [['pusat_pengajian_id','nama_pelajar','jantina','tarikh_masuk','tahap_semasa','warganegara'], 'required', 'on' => 'registerday_existdata','message'=>'Ruangan {attribute} wajib diisi'],

            [['tempat_lahir', 'alamat_rumah', 'alamat_majikan_bapa_penjaga', 'alamat_majikan_ibu'], 'string'],
            [['pusat_pengajian_id', 'gaji_bapa', 'gaji_ibu', 'enter_by', 'update_by','kelas','state_id','district_id','id_staf','pusat_pendaftaran','tahap_semasa','alumni','status_deleted','deleted_by'], 'integer'],
            ['no_mykid', 'unique','message'=>'No My Kid Sudah Wujud'],
            [['no_mykid'], 'required','message'=>'Sila Isikan No My Kid Pelajar'],
            [['nama_pelajar', 'tarikh_masuk', 'tahun_mula', 'SPRA', 'PSRA', 'warganegara', 'tahun_lewat', 'nama_bapa', 'no_kad_pengenalan_bapa', 'pekerjaan_bapa_penjaga', 'no_tel_bapa', 'no_hp_bapa', 'nama_ibu', 'no_kad_pengenalan_ibu', 'pekerjaan_ibu', 'no_tel_ibu', 'no_hp_ibu','sesi_pengajian','email_bapa_penjaga','email_ibu_penjaga'], 'string', 'max' => 255],
            [['tarikh_lahir', 'no_surat_beranak', 'no_mykid','status','jantina','date_create','date_update','deleted_at'], 'string', 'max' => 50],

            ['alumni', 'default', 'value' => self::NON_ALUMNI],
            ['alumni', 'in', 'range' => [self::NON_ALUMNI, self::ALUMNI]],

            ['status_deleted', 'default', 'value' => self::NOT_DELETE],
            ['status_deleted', 'in', 'range' => [self::SOFT_DELETE, self::HARD_DELETE,self::NOT_DELETE]],
        ];
    }

        

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_pelajar' => 'Nama Pelajar',
            'jantina' => 'Jantina',
            'tarikh_lahir' => 'Tarikh Lahir',
            'tempat_lahir' => 'Tempat Lahir',
            'no_surat_beranak' => 'No Surat Beranak',
            'no_mykid' => 'No Mykid',
            'pusat_pengajian_id' => 'Pusat Pengajian',
            'sesi_pengajian' => 'Sesi Pengajian',
            'tarikh_masuk' => 'Tarikh Masuk',
            'tahun_mula' => 'Tahap Mula',
            'alamat_rumah' => 'Alamat Rumah',
            'SPRA' => 'SPRA',
            'PSRA' => 'PSRA',
            'status' => 'Status',
            'warganegara' => 'Warganegara',
            'tahun_lewat' => 'Tahun Lewat',
            'alumni' => 'Alumni',
            'nama_bapa' => 'Nama Bapa',
            'no_kad_pengenalan_bapa' => 'No Kad Pengenalan Bapa',
            'pekerjaan_bapa_penjaga' => 'Pekerjaan Bapa Penjaga',
            'no_tel_bapa' => 'No Tel Bapa',
            'no_hp_bapa' => 'No Handphone Bapa',
            'alamat_majikan_bapa_penjaga' => 'Alamat Majikan Bapa Penjaga',
            'gaji_bapa' => 'Gaji Bapa',
            'nama_ibu' => 'Nama Ibu',
            'no_kad_pengenalan_ibu' => 'No Kad Pengenalan Ibu',
            'pekerjaan_ibu' => 'Pekerjaan Ibu',
            'no_tel_ibu' => 'No Tel Ibu',
            'no_hp_ibu' => 'No Handphone Ibu',
            'alamat_majikan_ibu' => 'Alamat Majikan Ibu',
            'gaji_ibu' => 'Gaji Ibu',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'enter_by' => 'Enter By',
            'update_by' => 'Update By',
            'kelas' => 'Kelas',
            'state_id'=>'Negeri',
            'district_id'=>'Daerah',
            'id_staf'=>'Nama Guru',
            'pusat_pendaftaran' => 'Pusat Pendaftaran',
        ];
    }

    public function getPendapatanbapa()
    {
        return $this->hasOne(LookupPendapatan::className(), ['id' => 'gaji_bapa']);
    }
    public function getPendapatanibu()
    {
        return $this->hasOne(LookupPendapatan::className(), ['id' => 'gaji_ibu']);
    }

    public function getNama_tahfiz()
    {
        return $this->hasOne(LookupPusatPengajian::className(),['id' =>'pusat_pengajian_id']);
    }

    public function getRegister()
    {
        return $this->hasOne(LookupPusatPengajian::className(),['id' =>'pusat_pendaftaran']);
    }

    public function getNegeri()
    {
        return $this->hasOne(LookupState::className(),['state_id' =>'state_id']);
    }
    public function getDaerah()
    {
        return $this->hasOne(LookupDistrict::className(),['district_id' =>'district_id']);
    }

    public function getKelas_pelajar()
    {
        return $this->hasOne(GuruPelajar::className(),['id_pelajar' =>'id']);
    }
    
    public function getNama_kelas()
    {
        return $this->hasOne(LookupKelas::className(),['id'=>'kelas']);
    }

    public function getNamaguru()
    {
        return $this->hasOne(MaklumatKakitangan::className(),['id_staf'=>'id_staf']);
    }

    public function getEnterby()
    {
        return $this->hasOne(User::className(),['id'=>'enter_by']);
    }

    public function getStatusdesc()
    {
        return $this->statusdesc;
        
    }

    public function getStatusalumni($value)
    {
        if ($value == 0) {
            $this->statusdesc = 'Ya';
        }
        elseif ($value == 1) {
            $this->statusdesc = 'Tidak';
        }
        elseif ($value == '' || $value == NULL) {
            $this->statusdesc = '(not set)';
        }
    }

    public static function stdntinfo($id)
    {
        $model = MaklumatPelajarPenjaga::find()->where(['id'=>$id])->one();

        return $model;
    }
}