<?php

namespace app\modules\hr\pelajar\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\hr\pelajar\models\MaklumatPelajarPenjaga;

/**
 * MaklumatPelajarPenjagaSearch represents the model behind the search form of `app\modules\hr\pelajar\models\MaklumatPelajarPenjaga`.
 */
class MaklumatPelajarPenjagaSearch extends MaklumatPelajarPenjaga
{
    /**
     * {@inheritdoc}
     */

    public $globalstd;

    public function rules()
    {
        return [
            [['id', 'pusat_pengajian_id', 'gaji_bapa', 'gaji_ibu', 'enter_by', 'update_by','pusat_pendaftaran'], 'integer'],
            [['nama_pelajar', 'jantina', 'tarikh_lahir', 'tempat_lahir', 'no_surat_beranak', 'no_mykid', 'sesi_pengajian', 'tarikh_masuk', 'tahun_mula', 'alamat_rumah', 'SPRA', 'PSRA', 'status', 'warganegara', 'tahun_lewat', 'alumni', 'nama_bapa', 'no_kad_pengenalan_bapa', 'pekerjaan_bapa_penjaga', 'no_tel_bapa', 'no_hp_bapa', 'alamat_majikan_bapa_penjaga', 'nama_ibu', 'no_kad_pengenalan_ibu', 'pekerjaan_ibu', 'no_tel_ibu', 'no_hp_ibu', 'alamat_majikan_ibu', 'date_create', 'date_update','globalstd'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MaklumatPelajarPenjaga::find();

        // $query->joinWith(['kelas_pelajar']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['nama_pelajar' => SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nama_pelajar'=>$this->nama_pelajar,
            'no_mykid'=>$this->no_mykid,
            
            'pusat_pengajian_id' => $this->pusat_pengajian_id,
            'gaji_bapa' => $this->gaji_bapa,
            'gaji_ibu' => $this->gaji_ibu,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
            'enter_by' => $this->enter_by,
            'update_by' => $this->update_by,
        ]);

        $query->orFilterWhere(['like', 'nama_pelajar', $this->globalstd])
            ->orFilterWhere(['like', 'jantina', $this->globalstd])
            ->orFilterWhere(['like', 'tarikh_lahir', $this->globalstd])
            ->orFilterWhere(['like', 'tempat_lahir', $this->globalstd])
            ->orFilterWhere(['like', 'no_surat_beranak', $this->globalstd])
            ->orFilterWhere(['like', 'no_mykid', $this->globalstd])
            ->orFilterWhere(['like', 'sesi_pengajian', $this->globalstd])
            ->orFilterWhere(['like', 'tarikh_masuk', $this->globalstd])
            ->orFilterWhere(['like', 'status', $this->globalstd])
            
            ->orFilterWhere(['like', 'nama_bapa', $this->globalstd])
            ->orFilterWhere(['like', 'no_kad_pengenalan_bapa', $this->globalstd])
            ->orFilterWhere(['like', 'nama_ibu', $this->globalstd])
            ->orFilterWhere(['like', 'no_kad_pengenalan_ibu', $this->globalstd]);

        return $dataProvider;
    }
}
