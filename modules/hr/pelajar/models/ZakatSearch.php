<?php

namespace app\modules\hr\pelajar\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\hr\pelajar\models\Zakat;

/**
 * ZakatSearch represents the model behind the search form of `app\modules\hr\pelajar\models\Zakat`.
 */
class ZakatSearch extends Zakat
{
    /**
     * {@inheritdoc}
     */
    public $globalzakat;

    public function rules()
    {
        return [
            [['id', 'id_pelajar', 'tahun', 'enter_by'], 'integer'],
            [['date_added', 'nota','globalzakat'], 'safe'],
            [['jumlah_zakat'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Zakat::find();
        $query->joinWith('namapelajar');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_pelajar' => $this->id_pelajar,
            'tahun' => $this->tahun,
            'enter_by' => $this->enter_by,
            'jumlah_zakat' => $this->jumlah_zakat,
        ]);

        $query->orFilterWhere(['like', 'date_added', $this->globalzakat])
            ->orFilterWhere(['like', 'maklumat_pelajar_penjaga.nama_pelajar', $this->globalzakat])
            ->orFilterWhere(['like', 'maklumat_pelajar_penjaga.no_mykid', $this->globalzakat])
            ->orFilterWhere(['like', 'nota', $this->globalzakat]);

        return $dataProvider;
    }
}
