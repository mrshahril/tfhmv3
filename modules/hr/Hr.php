<?php

namespace app\modules\hr;

/**
 * hr module definition class
 */
class Hr extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\hr\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules = [
            'staff' => [
                'class' => 'app\modules\hr\staff\Staff',
            ],
            'pelajar' => [
                'class' => 'app\modules\hr\pelajar\Pelajar',
            ],
            'exam' => [
                'class' => 'app\modules\hr\exam\Exam',
            ],
        ];

        // custom initialization code goes here
    }
}
