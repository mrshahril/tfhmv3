<?php

namespace app\modules\oldyuran;

/**
 * oldyuran module definition class
 */
class Oldyuran extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\oldyuran\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
