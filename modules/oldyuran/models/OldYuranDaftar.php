<?php

namespace app\modules\oldyuran\models;

use Yii;

/**
 * This is the model class for table "yuran_daftar".
 *
 * @property int $id
 * @property int $id_pelajar
 * @property int $id_guru
 * @property string $tarikh_bayaran
 * @property string $no_resit
 * @property string $jumlah_yuran
 * @property string $jenis_yuran
 * @property string $yuran_tertunggak
 * @property int $bulan
 * @property int $tahap_pelajar
 * @property int $guru_kelas
 * @property string $status_yuran
 * @property int $tahun
 * @property string $nota
 * @property int $jenis_pelajar
 * @property string $jenis_bayaran
 * @property string $nama_penerima
 * @property string $no_rujukan_pindahan_wang
 * @property string $pelarasan
 */
class OldYuranDaftar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yuran_daftar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pelajar', 'id_guru', 'bulan', 'tahap_pelajar', 'guru_kelas', 'tahun', 'jenis_pelajar'], 'integer'],
            [['jumlah_yuran'], 'required'],
            [['jumlah_yuran', 'yuran_tertunggak', 'pelarasan'], 'number'],
            [['tarikh_bayaran', 'jenis_yuran'], 'string', 'max' => 50],
            [['no_resit', 'status_yuran', 'jenis_bayaran', 'no_rujukan_pindahan_wang'], 'string', 'max' => 100],
            [['nota', 'nama_penerima'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pelajar' => 'Id Pelajar',
            'id_guru' => 'Id Guru',
            'tarikh_bayaran' => 'Tarikh Bayaran',
            'no_resit' => 'No Resit',
            'jumlah_yuran' => 'Jumlah Yuran',
            'jenis_yuran' => 'Jenis Yuran',
            'yuran_tertunggak' => 'Yuran Tertunggak',
            'bulan' => 'Bulan',
            'tahap_pelajar' => 'Tahap Pelajar',
            'guru_kelas' => 'Guru Kelas',
            'status_yuran' => 'Status Yuran',
            'tahun' => 'Tahun',
            'nota' => 'Nota',
            'jenis_pelajar' => 'Jenis Pelajar',
            'jenis_bayaran' => 'Jenis Bayaran',
            'nama_penerima' => 'Nama Penerima',
            'no_rujukan_pindahan_wang' => 'No Rujukan Pindahan Wang',
            'pelarasan' => 'Pelarasan',
        ];
    }
}
