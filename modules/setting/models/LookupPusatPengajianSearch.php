<?php

namespace app\modules\setting\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LookupPusatPengajian;

/**
 * LookupPusatPengajianSearch represents the model behind the search form of `app\models\LookupPusatPengajian`.
 */
class LookupPusatPengajianSearch extends LookupPusatPengajian
{
    /**
     * {@inheritdoc}
     */
    public $global;

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['pusat_pengajian', 'code_resit','global'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LookupPusatPengajian::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->orFilterWhere(['like', 'pusat_pengajian', $this->global])
            ->orFilterWhere(['like', 'code_resit', $this->global]);

        return $dataProvider;
    }
}
