<?php

namespace app\modules\setting\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LookupYurandaftar;

/**
 * LookupYurandaftarSearch represents the model behind the search form of `app\models\LookupYurandaftar`.
 */
class LookupYurandaftarSearch extends LookupYurandaftar
{
    /**
     * {@inheritdoc}
     */
    public $global;
    public function rules()
    {
        return [
            [['id', 'tahap_pelajar', 'enter_by', 'updated_by'], 'integer'],
            [['jenis_bayaran', 'created_at', 'updated_at','global'], 'safe'],
            [['jumlah'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LookupYurandaftar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tahap_pelajar' => $this->tahap_pelajar,
            'jumlah' => $this->jumlah,
            'enter_by' => $this->enter_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->orFilterWhere(['like', 'jenis_bayaran', $this->global])
            ->orFilterWhere(['like', 'tahap_pelajar', $this->global])
            ->orFilterWhere(['like', 'jumlah', $this->global]);

        return $dataProvider;
    }
}
