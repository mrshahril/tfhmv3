<?php

namespace app\modules\setting\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PerkesoSip;

/**
 * PerkesoSipSearch represents the model behind the search form of `app\models\PerkesoSip`.
 */
class PerkesoSipSearch extends PerkesoSip
{
    /**
     * {@inheritdoc}
     */
    public $global;
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['dari', 'hingga', 'syer_majikan', 'syer_pekerja', 'jumlah_caruman'], 'number'],
            [['global'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PerkesoSip::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dari' => $this->dari,
            'hingga' => $this->hingga,
            'syer_majikan' => $this->syer_majikan,
            'syer_pekerja' => $this->syer_pekerja,
            'jumlah_caruman' => $this->jumlah_caruman,
        ]);

        $query->orFilterWhere(['like', 'dari', $this->global])
            ->orFilterWhere(['like', 'hingga', $this->global])
            ->orFilterWhere(['like', 'syer_majikan', $this->global])
            ->orFilterWhere(['like', 'syer_pekerja', $this->global])
            ->orFilterWhere(['like', 'jumlah_caruman', $this->global]);

        return $dataProvider;
    }
}
