<?php

namespace app\modules\setting\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LookupJawatan;

/**
 * JawatanSearch represents the model behind the search form of `app\models\LookupJawatan`.
 */
class JawatanSearch extends LookupJawatan
{
    /**
     * {@inheritdoc}
     */
    public $global;
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['jawatan','global'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LookupJawatan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->orFilterWhere(['like', 'jawatan', $this->global]);

        return $dataProvider;
    }
}
