<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\models\LookupPendapatanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Senarai Pendapatan';

$script = <<< JS
$(document).ready(function(){
    if($('#tetapan').hasClass('pendapatan/index')){
        $('.main-menu > .scroll > ul > li#setting').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    deleteswal();

    $("#some_pjax_id").on("pjax:success", function() {
      deleteswal();
    });

    function deleteswal(){
        $('a.delete').on('click', function(e){
             e.preventDefault();
            var link = $(this).attr('value');
            var title = 'Anda pasti mahu hapus item ini ?';
          swal({   
            title: title,
            // text: "Perhatian ! Proses ini tidak boleh diulang semua !",   
            type: "warning",   
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: 'Ya, saya pasti.',
            confirmButtonClass: 'btn-info',
            cancelButtonText: 'Batal',
            cancelButtonClass: 'btn-danger',

          }, 
           function(){
                $.ajax({
                    type: 'POST',
                    url: link,
                    success: function(data) {
                    
                   }
                });
            });
        })
    }

});
JS;
$this->registerJs($script);

?>
<span id="tetapan" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Tambah Baru',['create'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?= $this->render('_search', ['model' => $searchModel]);  ?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body table-responsive">
                <?php Pjax::begin(['id'=>'some_pjax_id']); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        // 'filterModel' => $searchModel,
                        'layout' => "{summary}\n{items}\n<nav class='mt-4 mb-3'>{pager}</nav>",
                        'pager' => [
                            'firstPageLabel' => 'Mula',
                            'lastPageLabel' => 'Akhir',
                            'prevPageLabel' => 'Sebelumnya',
                            'nextPageLabel' => 'Seterusnya',

                            'maxButtonCount' => 5,

                             'options' => [
                                'tag' => 'ul',
                                'class' => 'pagination justify-content-center mb-0',
                            ],
                            'linkContainerOptions'=>['class'=>'page-item'],
                            'linkOptions' => ['class' => 'page-link'],
                            'activePageCssClass' => 'active',
                        ],
                        'tableOptions'=>['class'=>'table table-striped'],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'pendapatan',

                            [
                                'header' => 'Tindakan',
                                'class' => 'yii\grid\ActionColumn',
                                'template'=>'{edit} {delete}',
                                    'buttons' => [
                                        'edit' => function ($url, $model) {
                                            return Html::a('Edit', ['update','id'=>$model->id],['class' => 'btn btn-outline-primary','title' => 'Edit']);
                                        },
                                        // 'delete'=>function($url,$model){
                                        //     return Html::a('Padam',FALSE,['class'=>'btn btn-outline-danger delete','value'=>Url::to(['delete','id'=>base64_encode($model->id)])]);
                                        // }
                                    ],
                            ],
                        ],
                    ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
