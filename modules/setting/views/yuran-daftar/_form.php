<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LookupYurandaftar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lookup-yurandaftar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jenis_bayaran')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'tahap_pelajar')->dropDownList(
        [
            '1'=>'1',
            '2'=>'2',
            '3'=>'3',
            '4'=>'4',
            '5'=>'5',
            '6'=>'6',
        ]
    )->label() ?>
    <?= $form->field($model, 'jenis_pelajar')->dropDownList(
        [
            '1'=>'Pelajar Baru',
            '2'=>'Pelajar Lama',
            '3'=>'Pelajar Lewat',
        ]
    )->label() ?>
    <?= $form->field($model, 'jumlah')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
