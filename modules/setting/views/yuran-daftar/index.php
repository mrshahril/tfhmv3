<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\models\LookupYurandaftarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Yuran Daftar';
$script = <<< JS
$(document).ready(function(){
    if($('#tetapan').hasClass('yuran-daftar/index')){
        $('.main-menu > .scroll > ul > li#setting').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }
});
JS;
$this->registerJs($script);
?>
<span id="tetapan" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Tambah Baru',['create'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?= $this->render('_search', ['model' => $searchModel]);  ?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body table-responsive">
                <?php Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'layout' => "{summary}\n{items}\n<nav class='mt-4 mb-3'>{pager}</nav>",
                        'pager' => [
                            'firstPageLabel' => 'Mula',
                            'lastPageLabel' => 'Akhir',
                            'prevPageLabel' => 'Sebelumnya',
                            'nextPageLabel' => 'Seterusnya',

                            'maxButtonCount' => 5,

                             'options' => [
                                'tag' => 'ul',
                                'class' => 'pagination justify-content-center mb-0',
                            ],
                            'linkContainerOptions'=>['class'=>'page-item'],
                            'linkOptions' => ['class' => 'page-link'],
                            'activePageCssClass' => 'active',
                        ],
                        'tableOptions'=>['class'=>'table table-striped'],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'jenis_bayaran',
                        'tahap_pelajar',
                        [
                            'attribute'=>'jumlah',
                            'format'=>'raw',
                            'value'=>function($value){
                                return number_format($value->jumlah,2);
                            }
                        ],
                        //'created_at',
                        //'updated_at',
                        //'updated_by',

                        [
                            'header' => 'Tindakan',
                            'class' => 'yii\grid\ActionColumn',
                            'template'=>'{edit}',
                                'buttons' => [
                                    'edit' => function ($url, $model) {
                                        return Html::a('Edit', ['update','id'=>$model->id],['class' => 'btn btn-outline-primary','title' => 'Edit']);
                                    },
                                ],
                        ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
