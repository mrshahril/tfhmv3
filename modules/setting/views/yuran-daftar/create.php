<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LookupYurandaftar */

$this->title = 'Tambah Yuran Daftar';
$script = <<< JS
$(document).ready(function(){
    if($('#tetapan').hasClass('yuran-daftar/create')){
        $('.main-menu > .scroll > ul > li#setting').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }
});
JS;
$this->registerJs($script);
?>
<span id="tetapan" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="lookup-yurandaftar-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
