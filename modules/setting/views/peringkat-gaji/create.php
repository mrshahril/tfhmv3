<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LookupPeringkatGaji */

$this->title = 'Create Lookup Peringkat Gaji';
$this->params['breadcrumbs'][] = ['label' => 'Lookup Peringkat Gajis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-peringkat-gaji-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
