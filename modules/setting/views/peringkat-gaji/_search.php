<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\hr\staff\models\MaklumatKakitanganSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'options'=>[
        'data-pjax'=>1,
    ],
]); ?>
<div class="card mb-4">
    <div class="card-body">
        <div class="input-group">
            <input type="text" id="LookupPeringkatGajiSearch-global" class="form-control" name="LookupPeringkatGajiSearch[global]" placeholder="Carian..." aria-describedby="basic-addon2" autocomplete="off">

            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit">Carian</button>
                <?= Html::a('Reset', ['index'], ['class' => 'btn btn-outline-secondary']) ?>

            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
