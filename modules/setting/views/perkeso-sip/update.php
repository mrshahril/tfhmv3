<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PerkesoSip */

$this->title = 'Update Perkeso Sip: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Perkeso Sips', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="perkeso-sip-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
