<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PerkesoSip */

$this->title = 'Create Perkeso Sip';
$this->params['breadcrumbs'][] = ['label' => 'Perkeso Sips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perkeso-sip-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
