<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LookupKwsp11 */

$this->title = 'Update Lookup Kwsp11: ' . $model->id_kwsp;
$this->params['breadcrumbs'][] = ['label' => 'Lookup Kwsp11s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kwsp, 'url' => ['view', 'id' => $model->id_kwsp]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lookup-kwsp11-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
