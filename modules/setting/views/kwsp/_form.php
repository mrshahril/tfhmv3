<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LookupKwsp11 */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Tambah Senarai KWSP';

$script = <<< JS
$(document).ready(function(){
    if($('#tetapan').hasClass('kwsp/create')){
        $('.main-menu > .scroll > ul > li#setting').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }
});
JS;
$this->registerJs($script);
?>
<span id="tetapan" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['index'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="lookup-kwsp11-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dari')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hingga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oleh_majikan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oleh_pekerja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jumlah_caruman')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
