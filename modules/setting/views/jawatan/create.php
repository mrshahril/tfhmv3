<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LookupJawatan */

$this->title = 'Create Lookup Jawatan';
$this->params['breadcrumbs'][] = ['label' => 'Lookup Jawatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-jawatan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
