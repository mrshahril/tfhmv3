<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\payroll\models\PaySlipSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pay Slips';
$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
$(document).ready(function(){
    if($('#payroll').hasClass('pay-slip/index')){
        alert('ad');
        $('.main-menu > .scroll > ul > li#salary').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    function init_click_handlers(){
        $('.modaltest').click(function(){
            $('#exampleModal').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));

        });
    }
    
    init_click_handlers(); //first run
    deleteswal();
    $("#some_pjax_id").on("pjax:success", function() {
      init_click_handlers(); //reactivate links in grid after pjax update
      deleteswal();
    });

    function deleteswal(){
        $('a.delete').on('click', function(e){
             e.preventDefault();
            var link = $(this).attr('value');
            console.log(link);
          swal({   
            title: "Anda pasti mahu hapus kakitangan ini dari sistem ?",
            // text: "Perhatian ! Proses ini tidak boleh diulang semua !",   
            type: "warning",   
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: 'Ya, saya pasti.',
            confirmButtonClass: 'btn-info',
            cancelButtonText: 'Batal',
            cancelButtonClass: 'btn-danger',

          }, 
           function(){   
                // $("#myform").submit();
                $.ajax({
                    type: 'POST',
                    url: link,
                    success: function(data) {
                    
                   }
                });
            });
        })
    }

});
JS;
$this->registerJs($script);

?>
<span id="payroll" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="pay-slip-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pay Slip', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'pay_slip_id',
            'staff_id',
            'gaji_asas',
            'elaun_rumah',
            'elaun_asas',
            //'ctg',
            //'kksk',
            //'tabung_guru',
            //'tabung_haji',
            //'cuti_ehsan',
            //'cuti_sakit',
            //'pelarasan',
            //'potongan',
            //'kwsp',
            //'socso',
            //'gaji_tahan',
            //'sewa_rumah',
            //'loan',
            //'tarikhmasa',
            //'memo_ctg',
            //'hibah',
            //'bonus',
            //'lain',
            //'lain_tambahan',
            //'sip',
            //'kwsp_majikan',
            //'socso_majikan',
            //'sip_majikan',
            //'created_at',
            //'update_at',
            //'enter_by',
            //'update_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
