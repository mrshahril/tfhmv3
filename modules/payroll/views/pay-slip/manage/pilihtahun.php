<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Rekod Slip Gaji';
$script = <<< JS
$(document).ready(function(){
    if($('#payroll').hasClass('pay-slip/pilihtahun')){
        $('.main-menu > .scroll > ul > li#salary').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

});
JS;
$this->registerJs($script);
?>
<span id="payroll" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['managesalary'],['class'=>'btn btn-outline-primary btn-lg mr-1']) ?>
            </div>
        </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php $form = ActiveForm::begin([
            	'action' => ['rekodslip_gaji'],
    			'method' => 'get',
            ]); ?>
            <div class="box-body ml-4">
            	<div class="form-group row">
                	<label class="col-sm-2 col-form-label">Rekod Untuk Tahun</label>
                    <div class="col-sm-10">
                    	<select name="tahun" class="form-control" required="">
	                    <?php
	                        $already_selected_value = date('Y');
	                        $earliest_year = 2012;
	                        foreach (range(date('Y'), $earliest_year) as $x) {
	                            print '<option value="'.$x.'"'.($x === $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
	                        }
	                    ?>
		                </select>
                        <input type="hidden" name="idstaf" value="<?php echo $id ?>">
                    </div>
            	</div>
            </div>
            <div class="box-footer">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success mb-1">Seterusnya</button>
                    <?= Html::a('Kembali',['managesalary'],['class'=>'btn btn-danger']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

    	</div>
    </div>
</div>