<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;

$this->title = 'Kemaskini Payroll - '.$model->nama;


$script = <<< JS
$(document).ready(function(){
    if($('#payroll').hasClass('pay-slip/edit')){
        $('.main-menu > .scroll > ul > li#salary').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }
});
JS;
$this->registerJs($script);
?>
<span id="payroll" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['managesalary'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
            	<?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>
                <form>
                	<div class="form-row">
                         <div class="form-group col-md-6">
                         	<label>Peratus Kwsp</label>
                         	<?= $form->field($model, 'peratus_kwsp')->dropDownList([
                         			'8'=>'8',
                         			'11'=>'11',
                         		], 
                                [
                                    'prompt'=>'Sila Pilih',
                                ])->label(false) ?>
                         </div>
                         <div class="form-group col-md-6">
                         	<label>No Kwsp</label>
                         	<?= $form->field($model, 'no_kwsp')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                	</div>
                	<div class="form-row">
                         <div class="form-group col-md-6">
                         	<label>Gaji Asas</label>
                         	<?= $form->field($model, 'gaji_asas')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                         <div class="form-group col-md-6">
                         	<label>Gaji Tahan</label>
                         	<?= $form->field($model, 'gaji_tahan')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                	</div>
                	<div class="form-row">
                         <div class="form-group col-md-6">
                         	<label>No. Akaun Bank</label>
                         	<?= $form->field($model, 'acc_bimb')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                         <div class="form-group col-md-6">
                         	<label>No. Akaun Tabung Haji</label>
                         	<?= $form->field($model, 'acc_tabung_haji')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                	</div>
                	<div class="form-row">
                         <div class="form-group col-md-4">
                         	<label>Elaun Asas</label>
                         	<?= $form->field($model, 'elaun_asas')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                         <div class="form-group col-md-4">
                         	<label>Elaun Rumah</label>
                         	<?= $form->field($model, 'elaun_rumah')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                         <div class="form-group col-md-4">
                         	<label>Sewa Rumah</label>
                         	<?= $form->field($model, 'sewa_rumah')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                	</div>
                	<div class="form-row">
                         <div class="form-group col-md-6">
                         	<label>Tabung Haji</label>
                         	<?= $form->field($model, 'tabung_gaji')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                         <div class="form-group col-md-6">
                         	<label>Tabung Guru</label>
                         	<?= $form->field($model, 'tabung_guru')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                	</div>
                	<div class="form-row">
                         <div class="form-group col-md-6">
                         	<label>KKSK</label>
                         	<?= $form->field($model, 'kksk')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                         <div class="form-group col-md-6">
                         	<label>Loan</label>
                         	<?= $form->field($model, 'loan')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                	</div>
                </form>
                <div class="box-footer">
	                <div class="col-md-12">
	                    <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>

	                    <?= Html::submitButton('Simpan', ['class' => 'btn btn-success mb-1']) ?>

	                    <?= Html::a('Batal',['managesalary'],['class'=>'btn btn-danger mb-1']) ?>
	                </div>
	            </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>