<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;

$this->title = 'Jana Gaji Bulan '.date('F Y');

$script = <<< JS
$(document).ready(function(){
    if($('#payroll').hasClass('pay-slip/janaslip')){
        $('.main-menu > .scroll > ul > li#salary').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }
});
JS;
$this->registerJs($script);
?>
<span id="payroll" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['jana'],['class'=>'btn btn-outline-primary btn-lg']) ?>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
            	<?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>
                <form>
                	<div class="form-row">
                         <div class="form-group col-md-4">
                         	<label>Gaji Asas</label>
                         	<?= $form->field($model, 'gaji_asas')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                         <div class="form-group col-md-4">
                         	<label>Elaun Asas</label>
                         	<?= $form->field($model, 'elaun_asas')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                         <div class="form-group col-md-4">
                         	<label>Elaun Rumah</label>
                         	<?= $form->field($model, 'elaun_rumah')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                     </div>
                	<div class="form-row">
                         <div class="form-group col-md-6">
                         	<label>Gaji Kasar</label>
                         	<?php  
                         		$gajikasar = $model->gaji_asas + $model->elaun_asas + $model->elaun_rumah;
                         	?>
                         	<input type="text" disabled="disabled" class="form-control" value="<?php echo number_format($gajikasar,2) ?>">
                         </div>
                         <div class="form-group col-md-6">
                         	<label>Gaji Tahan</label>
                         	<?= $form->field($model, 'gaji_tahan')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                         </div>
                	</div>
                	<div class="form-row">
                		<div class="form-group col-md-6">
                			<label>Hibah (+RM)</label>
                			<?= $form->field($model2, 'hibah')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                		</div>
                		<div class="form-group col-md-6">
                			<label>Bonus (+RM)</label>
                			<?= $form->field($model2, 'bonus')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                		</div>
                	</div>
                	<div class="form-row">
                		<div class="form-group col-md-6">
                			<label>KKSK (-RM)</label>
                			<?= $form->field($model, 'kksk')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                		</div>
                		<div class="form-group col-md-6">
                			<label>Loan (-RM)</label>
                			<?= $form->field($model, 'loan')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                		</div>
                	</div>
                	<div class="form-row">
                		<div class="form-group col-md-6">
                			<label>Tabung Haji (-RM)</label>
                			<?= $form->field($model, 'tabung_gaji')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                		</div>
                		<div class="form-group col-md-6">
                			<label>Tabung Guru (-RM)</label>
                			<?= $form->field($model, 'tabung_guru')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                		</div>
                	</div>
                	<div class="form-row">
                		<div class="form-group col-md-6">
                			<label>Sewa Rumah (-RM)</label>
                			<?= $form->field($model, 'sewa_rumah')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                		</div>
                		<div class="form-group col-md-6">
                			<label>KWSP (%)</label>
                			<?= $form->field($model2, 'kwsp')->dropDownList([
                         			'8'=>'8',
                         			'11'=>'11',
                         		], 
                                [
                                    'prompt'=>'Sila Pilih',
                                     'options' => ['11' => ['selected'=>true]],

                                ])->label(false) ?>
                		</div>
                		
                	</div>
                	<div class="form-row">
                		<div class="form-group col-md-6">
                			<label>CTG (Hari)</label>
                			<?= $form->field($model2, 'ctg')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                		</div>
                		<div class="form-group col-md-6">
                			<label>Tarikh CTG</label>
                			<?= $form->field($model2, 'memo_ctg')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                		</div>
                	</div>
                	<div class="form-row">
                		<div class="form-group col-md-6">
                			<label>Lain-lain Tambahan (+RM)</label>
                			<?= $form->field($model2, 'lain_tambahan')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                		</div>
                		<div class="form-group col-md-6">
                			<label>Lain-lain Pemotongan (-RM)</label>
                			<?= $form->field($model2, 'lain')->textInput(['maxlength' => true,'class'=>'form-control','autocomplete'=>'off'])->label(false) ?>
                		</div>
                	</div>
                </form>
                <div class="box-footer">
	                <div class="col-md-12">
	                    <?= $form->errorSummary($model,['class'=>'alert alert-danger','header'=>'']); ?>

	                    <?= Html::submitButton('Simpan', ['class' => 'btn btn-success mb-1']) ?>
                		<?= Html::a('Batal',['jana'],['class'=>'btn btn-danger mb-1']) ?>
	                </div>
	            </div>
            </div>
            <?php ActiveForm::end(); ?>

    	</div>
    </div>
</div>