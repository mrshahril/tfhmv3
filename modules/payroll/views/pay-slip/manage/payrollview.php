<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\hr\staff\models\MaklumatCuti */

$this->title = 'Maklumat Terperinci';
$script = <<< JS
$(document).ready(function(){
    if($('#payroll').hasClass('pay-slip/payrollview')){
        $('.main-menu > .scroll > ul > li#salary').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }

    deleteswal();
    $("#some_pjax_id").on("pjax:success", function() {
      init_click_handlers(); //reactivate links in grid after pjax update
      deleteswal();
    });

    function deleteswal(){
        $('a.delete').on('click', function(e){
             e.preventDefault();
            var link = $(this).attr('value');
            console.log(link);
          swal({   
            title: "Anda pasti mahu hapus maklumat cuti ini dari sistem ?",
            // text: "Perhatian ! Proses ini tidak boleh diulang semua !",   
            type: "warning",   
            allowOutsideClick: false,
            showConfirmButton: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText: 'Ya, saya pasti.',
            confirmButtonClass: 'btn-info',
            cancelButtonText: 'Batal',
            cancelButtonClass: 'btn-danger',

          }, 
           function(){   
                // $("#myform").submit();
                $.ajax({
                    type: 'POST',
                    url: link,
                    success: function(data) {
                    
                   }
                });
            });
        })
    }
});
JS;
$this->registerJs($script);
?>
<span id="payroll" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="float-md-right text-zero">
                <?= Html::a('Kembali',['managesalary'],['class'=>'btn btn-outline-primary btn-lg mr-1']) ?>
                <?= Html::a('Kemaskini',['pay-slip/edit','id'=>base64_encode($model->id_staf)],['class'=>'btn btn-outline-primary btn-lg mr-1']) ?>
                </div>
            </div>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="box box-danger">
            <div class="box-body table-responsive">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'peratus_kwsp',
                        'gaji_asas',
                        'elaun_asas',
                        'elaun_rumah',
                        'tabung_gaji',
                        'tabung_guru',
                        'sewa_rumah',
                        'kksk',
                        'loan',
                        'gaji_tahan',
                        'acc_tabung_haji',
                        'no_kwsp',
                        'acc_bimb',

                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>