<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\hr\staff\models\MaklumatKakitanganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengurusan Gaji Kakitangan';
$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
$(document).ready(function(){
    if($('#payroll').hasClass('pay-slip/managesalary')){

        $('.main-menu > .scroll > ul > li#salary').addClass('active');
        $('.main-menu > .scroll > ul > li#utama').removeClass('active');
    }


});
JS;
$this->registerJs($script);

?>
<span id="payroll" class="<?php echo Yii::$app->controller->id."/".Yii::$app->controller->action->id;?>"></span>
<div class="row">
    <div class="col-12">
        <div class="mb-1">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="separator mb-5"></div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <?= $this->render('_search', ['model' => $searchModel]);  ?>
        
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
            <div class="alert alert-success" role="alert" id="messageerror2">
                <?php echo Yii::$app->session->getFlash('savedone'); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-body table-responsive">
                <?php Pjax::begin(['id'=>'some_pjax_id']); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "{summary}\n{items}\n<nav class='mt-4 mb-3'>{pager}</nav>",
                    'pager' => [
                        'firstPageLabel' => 'Mula',
                        'lastPageLabel' => 'Akhir',
                        'prevPageLabel' => 'Sebelumnya',
                        'nextPageLabel' => 'Seterusnya',

                        'maxButtonCount' => 5,

                         'options' => [
                            'tag' => 'ul',
                            'class' => 'pagination justify-content-center mb-0',
                        ],
                        'linkContainerOptions'=>['class'=>'page-item'],
                        'linkOptions' => ['class' => 'page-link'],
                        'activePageCssClass' => 'active',
                    ],
                    'tableOptions'=>['class'=>'table table-striped'],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'Foto',
                            'format' => 'html',    
                            'value' => function ($data) {
                                return Html::img(Yii::getAlias('@web').'/uploads/'. $data['foto'],
                                    ['width' => '70px']);
                            },
                        ],

                        'nama',
                        'no_kp',
                        'jawatan.jawatan',
                        //'nama_tahfiz.pusat_pengajian',
                        //'statuskerja.status_pekerjaan',
                        'no_pekerja',
                        'no_kwsp',
                        [
                            'attribute'=>'status_pekerjaan',
                            'format'=>'raw',
                            'value'=>function($data){
                                if ($data->status_pekerjaan == 1) {
                                    $btn = 'btn btn-xs btn-success';
                                }
                                elseif ($data->status_pekerjaan == 2) {
                                    $btn = 'btn btn-xs btn-danger';
                                }

                                return Html::a($data->statuskerja->status_pekerjaan,FALSE,['class'=>$btn,'style'=>'color:#fff;']);
                            }
                        ],
                        [
                            'header' => 'Tindakan',
                            'class' => 'yii\grid\ActionColumn',
                            'template'=>'{all}',
                            'buttons' => [
                                'all' => function ($url, $model, $key) {
                                    return ButtonDropdown::widget([
                                        'encodeLabel' => false, // if you're going to use html on the button label
                                        'label' => 'Pilih Tindakan',
                                        'dropdown' => [
                                            'encodeLabels' => false, // if you're going to use html on the items' labels
                                            'items' => [
                                                [
                                                    'label' => \Yii::t('yii', 'Rekod Slip Gaji'),
                                                    'url' => ['pay-slip/rekod', 'id' =>base64_encode($key)],
                                                    'linkOptions' => ['class'=>'dropdown-item'],
                                                    'visible' => true,  // if you want to hide an item based on a condition, use this
                                                ],
                                                [
                                                    'label' => \Yii::t('yii', 'Lihat Maklumat'),
                                                    'url' => ['pay-slip/payrollview', 'id' =>base64_encode($key)],
                                                    'linkOptions' => ['class'=>'dropdown-item'],
                                                    'visible' => true,  // if you want to hide an item based on a condition, use this
                                                ],
                                                [
                                                    'label' => \Yii::t('yii', 'Kemaskini Maklumat'),
                                                    'url' => ['pay-slip/edit', 'id' => base64_encode($key)],
                                                    'linkOptions' => ['class'=>'dropdown-item'],
                                                    'visible' => true,  // if you want to hide an item based on a condition, use this
                                                ],
                                            ],
                                            
                                        ],
                                        'options' => [
                                            'class' => 'btn btn-outline-primary btn-sm',   // btn-success, btn-info, et cetera
                                        ],
                                        'split' => false,    // if you want a split button
                                    ]);
                                },
                            ],
                        ],
                        // ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
            
        </div>
    </div>
</div>
