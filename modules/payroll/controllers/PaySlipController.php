<?php

namespace app\modules\payroll\controllers;

use Yii;

use app\models\LookupKwsp8;
use app\models\LookupKwsp11;
use app\models\Perkeso;
use app\models\PerkesoSip;

use app\modules\payroll\models\PaySlip;
use app\modules\hr\staff\models\MaklumatKakitangan;
use app\modules\hr\staff\models\MaklumatKakitanganSearch;
use app\modules\payroll\models\PaySlipSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

/**
 * PaySlipController implements the CRUD actions for PaySlip model.
 */
class PaySlipController extends Controller
{
    /**
     * {@inheritdoc}
     */

    private $kwsp11;
    private $kwsp8;
    private $perkeso;
    private $sip;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PaySlip models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaySlipSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaySlip model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PaySlip model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PaySlip();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pay_slip_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PaySlip model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel(base64_decode($id));
        $model2 = $model->namestaf;
        
        if ($model->load(Yii::$app->request->post()) && $model2->load(Yii::$app->request->post()) ) {
            $model2->gaji_asas = $model->gaji_asas;
            $model2->elaun_rumah = $model->elaun_rumah;
            $model2->elaun_asas = $model->elaun_asas;
            $model2->sewa_rumah = $model->sewa_rumah;
            $model2->gaji_tahan = $model->gaji_tahan;
            $model2->tabung_gaji = $model->tabung_haji;
            $model2->tabung_guru = $model->tabung_guru;
            $model2->kksk = $model->kksk;
            $model2->loan = $model->loan;
            $model2->updated_at = date("Y-m-d h:i:s A");
            $model2->updated_by = Yii::$app->user->identity->id;

            $gaji_kasar = $model->gaji_asas + $model->elaun_asas + $model->elaun_rumah;
            $epfvalue = $gaji_kasar + $model->bonus;

            $model->kwsp = $model2->peratus_kwsp;

            if ($model->kwsp == 8) {
                $kwsp = $this->getKwsp8();
            }
            else{
                $kwsp = $this->getKwsp11();
            }

            //determine kwsp
            foreach ($kwsp as $value2) {
                if(($epfvalue) >= $value2['dari'] && ($epfvalue) <= $value2['hingga']){
                    $oleh_pekerja = $value2['oleh_pekerja'];
                    $oleh_majikan = $value2['oleh_majikan'];
                }
            }

            $perkeso = $this->getPerkeso();
            //determine socso
            foreach ($perkeso as $value3)
            {
                if ($gaji_kasar >= $value3['dari'] && $gaji_kasar <= $value3['hingga']) {
                    $syer_pekerja = $value3['syer_pekerja'];
                    $syer_majikan = $value3['syer_majikan'];
                }
            }

            //determine perkeso SIP
            $sip = $this->getSip();

            foreach ($sip as $value4) {
                if ($gaji_kasar >= $value4['dari'] && $gaji_kasar <= $value4['hingga']) {
                    $sip_pekerja = $value4['syer_pekerja'];
                    $sip_majikan = $value4['syer_majikan'];
                }
            }

            $model->kwsp = $oleh_pekerja;
            $model->kwsp_majikan = $oleh_majikan;
            $model->socso = $syer_pekerja;
            $model->socso_majikan = $syer_majikan;
            $model->sip = $sip_pekerja;
            $model->sip_majikan = $sip_majikan;
            $model->update_at = date('Y-m-d h:i:s A');
            $model->update_by = Yii::$app->user->identity->id;

            if ($model->save()) {
                $model2->save();
                Yii::$app->session->setFlash('savedone','Slip Gaji telah berjaya dikemaskini');
                return $this->redirect(['rekodslip_gaji', 'tahun' =>date('Y',strtotime($model->tarikhmasa)),'idstaf'=>base64_encode($model->staff_id)]);
            }
        }

        return $this->render('manage/editslip', [
            'model' => $model,
            'model2' => $model2,
        ]);
    }

    public function actionCetakslip($id)
    {
        $model = PaySlip::findOne(base64_decode($id));

        $content = $this->renderPartial('manage/cetakslip',[
            'model'=>$model,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'content' => $content,
            'filename' => 'slip_gaji_'.$model->namestaf->nama.'_'.date('Y-m-d',strtotime($model->tarikhmasa)).'.pdf',
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
            'cssInline' => '.main{font-size:11px;font-family:courier}.head{font-size:15px;font-family:courier}',
            'options' => ['title' => 'Slip Gaji | '.$model->namestaf->nama],
            'methods' => [
                'SetHeader'=>['Tarikh Cetakan '.date("d/m/Y")],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
        return $pdf->render();

    }

    public function actionJana()
    {
        $model = PaySlip::find()->select('staff_id')->where(['DATE_FORMAT(tarikhmasa,"%Y-%m")'=>date('Y-m')]);
        $searchModel = new MaklumatKakitanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['NOT IN','id_staf',$model]);
        $dataProvider->query->andWhere(['status_pekerjaan'=>1]);

        return $this->render('manage/jana',[
            'searchModel'=>$searchModel,
            'dataProvider'=>$dataProvider,
       ]);
    }

    public function actionJanaslip($id)
    {
        $model2 = new PaySlip();
        $model = MaklumatKakitangan::findOne(base64_decode($id));

        if ($model2->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post())) {
            $model2->staff_id = base64_decode($id);
            $model2->created_at = date('Y-m-d h:i:s A');
            $model2->enter_by = Yii::$app->user->identity->id;
            $model2->gaji_asas = $model->gaji_asas;
            $model2->elaun_rumah = $model->elaun_rumah;
            $model2->elaun_asas = $model->elaun_asas;
            $model2->sewa_rumah = $model->sewa_rumah;
            $model2->gaji_tahan = $model->gaji_tahan;
            $model2->tabung_haji = $model->tabung_gaji;
            $model2->tabung_guru = $model->tabung_guru;
            $model2->kksk = $model->kksk;
            $model2->loan = $model->loan;
            $model2->tarikhmasa = date('Y-m-t h:i:s A');

            if ($model2->kwsp == 8) {
                $kwsp = $this->getKwsp8();
            }
            else{
                $kwsp = $this->getKwsp11();
            }  

            $gaji_kasar = $model->gaji_asas + $model->elaun_asas + $model->elaun_rumah;

            //determine kwsp
            foreach ($kwsp as $value2) {
                if(($gaji_kasar + $model2->bonus) >= $value2['dari'] && ($gaji_kasar + $model2->bonus) <= $value2['hingga'])
                {
                    
                    $oleh_pekerja = $value2['oleh_pekerja'];
                    $oleh_majikan = $value2['oleh_majikan'];
                }
            }

            $perkeso = $this->getPerkeso();
            //determine socso
            foreach ($perkeso as $value3)
            {
                if ($gaji_kasar >= $value3['dari'] && $gaji_kasar <= $value3['hingga']) {
                    $syer_pekerja = $value3['syer_pekerja'];
                    $syer_majikan = $value3['syer_majikan'];
                }
            }

            //determine perkeso SIP
            $sip = $this->getSip();

            foreach ($sip as $value4) {
                if ($gaji_kasar >= $value4['dari'] && $gaji_kasar <= $value4['hingga']) {
                    $sip_pekerja = $value4['syer_pekerja'];
                    $sip_majikan = $value4['syer_majikan'];
                }
            }

            $model2->kwsp = $oleh_pekerja;
            $model2->kwsp_majikan = $oleh_majikan;
            $model2->socso = $syer_pekerja;
            $model2->socso_majikan = $syer_majikan;
            $model2->sip = $sip_pekerja;
            $model2->sip_majikan = $sip_majikan;

            if ($model2->save()) {
                $model->save();
                Yii::$app->session->setFlash('savedone','Slip Gaji telah berjaya dijana');
                return $this->redirect(['jana']);
            }
        }
        else{
            return $this->render('manage/janaslip',[
                'model'=>$model,
                'model2'=>$model2,
            ]);
        }
    }
    /**
     * Deletes an existing PaySlip model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PaySlip model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaySlip the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PaySlip::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function getKwsp11()
    {
        $this->kwsp11 = LookupKwsp11::find()
                      ->all();

        return $this->kwsp11;
    }

    public function getKwsp8()
    {
        $this->kwsp8 = LookupKwsp8::find()
                      ->all();

        return $this->kwsp8;
    }

    public function getPerkeso()
    {
        $this->perkeso = Perkeso::find()
                        ->all();

        return $this->perkeso;
    }

    public function getSip()
    {
        $this->sip = PerkesoSip::find()
                    ->all();

        return $this->sip;
    }
    // MANAGE SALARY

    public function actionManagesalary()
    {
        $searchModel = new MaklumatKakitanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('manage/managesalary', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPayrollview($id)
    {
        $model = MaklumatKakitangan::findOne(base64_decode($id));

        return $this->render('manage/payrollview',[
            'model'=>$model,
        ]);
    }

    public function actionEdit($id)
    {
        $model = MaklumatKakitangan::findOne(base64_decode($id));

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = date('Y-m-d h:i:s A');
            $model->updated_by = Yii::$app->user->identity->id;

            if ($model->save()) {
                Yii::$app->session->setFlash('savedone','Maklumat gaji kakitangan telah berjaya dikemaskini');
                return $this->redirect(['managesalary']);
            }
        }
        else{
            return $this->render('manage/edit',[
                'model'=>$model,
            ]);
        }
    }

    public function actionRekod($id)
    {
        return $this->render('manage/pilihtahun',[
            'id'=>$id,
        ]);
    }

    public function actionRekodslip_gaji($tahun,$idstaf)
    {   
        $staf = MaklumatKakitangan::findOne(base64_decode($idstaf));

        $searchModel = new PaySlipSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['DATE_FORMAT(tarikhmasa, "%Y")'=>$tahun]);
        $dataProvider->query->andWhere(['staff_id'=>base64_decode($idstaf)]);

        return $this->render('manage/rekodslip_gaji', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'staf'=> $staf,
        ]);
    }
}
