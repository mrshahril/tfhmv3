<?php

namespace app\modules\payroll\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\payroll\models\PaySlip;

/**
 * PaySlipSearch represents the model behind the search form of `app\modules\payroll\models\PaySlip`.
 */
class PaySlipSearch extends PaySlip
{
    /**
     * {@inheritdoc}
     */

    public $globalslip;

    public function rules()
    {
        return [
            [['pay_slip_id', 'staff_id', 'enter_by', 'update_by'], 'integer'],
            [['gaji_asas', 'elaun_rumah', 'elaun_asas', 'ctg', 'kksk', 'tabung_guru', 'tabung_haji', 'cuti_ehsan', 'cuti_sakit', 'pelarasan', 'potongan', 'kwsp', 'socso', 'gaji_tahan', 'sewa_rumah', 'loan', 'hibah', 'bonus', 'lain', 'lain_tambahan', 'sip', 'kwsp_majikan', 'socso_majikan', 'sip_majikan'], 'number'],
            [['tarikhmasa', 'memo_ctg', 'created_at', 'update_at','globalslip'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaySlip::find();
        $query->joinWith('namestaf');
        // add conditions that should always apply here
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'sort'=> ['defaultOrder' => ['maklumat_kakitangan.nama' => SORT_ASC]],
            // 'pagination' => [
            //     'pageSize' => 2,
            // ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->orFilterWhere(['like', 'pay_slip.memo_ctg', $this->globalslip])
            ->orFilterWhere(['like', 'pay_slip.created_at', $this->globalslip])
            ->orFilterWhere(['like', 'maklumat_kakitangan.nama', $this->globalslip])
            ->orFilterWhere(['like', 'pay_slip.update_at', $this->globalslip]);

        return $dataProvider;
    }
}
