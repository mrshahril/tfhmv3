<?php

namespace app\modules\payroll\models;

use Yii;
use app\modules\hr\staff\models\MaklumatKakitangan;
/**
 * This is the model class for table "pay_slip".
 *
 * @property int $pay_slip_id
 * @property int $staff_id
 * @property double $gaji_asas
 * @property double $elaun_rumah
 * @property double $elaun_asas
 * @property double $ctg
 * @property double $kksk
 * @property double $tabung_guru
 * @property double $tabung_haji
 * @property double $cuti_ehsan
 * @property double $cuti_sakit
 * @property double $pelarasan
 * @property double $potongan
 * @property double $kwsp
 * @property double $socso
 * @property double $gaji_tahan
 * @property double $sewa_rumah
 * @property double $loan
 * @property string $tarikhmasa
 * @property string $memo_ctg
 * @property double $hibah
 * @property double $bonus
 * @property double $lain
 * @property double $lain_tambahan
 * @property double $sip
 * @property double $kwsp_majikan
 * @property double $socso_majikan
 * @property double $sip_majikan
 * @property string $created_at
 * @property string $update_at
 * @property int $enter_by
 * @property int $update_by
 */
class PaySlip extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pay_slip';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['staff_id', 'enter_by', 'update_by'], 'integer'],
            [['gaji_asas', 'elaun_rumah', 'elaun_asas', 'ctg', 'kksk', 'tabung_guru', 'tabung_haji', 'cuti_ehsan', 'cuti_sakit', 'pelarasan', 'potongan', 'kwsp', 'socso', 'gaji_tahan', 'sewa_rumah', 'loan', 'hibah', 'bonus', 'lain', 'lain_tambahan', 'sip', 'kwsp_majikan', 'socso_majikan', 'sip_majikan'], 'number'],
            [['tarikhmasa', 'created_at', 'update_at'], 'string', 'max' => 50],
            [['memo_ctg'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pay_slip_id' => 'Pay Slip ID',
            'staff_id' => 'Staff ID',
            'gaji_asas' => 'Gaji Asas',
            'elaun_rumah' => 'Elaun Rumah',
            'elaun_asas' => 'Elaun Asas',
            'ctg' => 'Ctg',
            'kksk' => 'Kksk',
            'tabung_guru' => 'Tabung Guru',
            'tabung_haji' => 'Tabung Haji',
            'cuti_ehsan' => 'Cuti Ehsan',
            'cuti_sakit' => 'Cuti Sakit',
            'pelarasan' => 'Pelarasan',
            'potongan' => 'Potongan',
            'kwsp' => 'Kwsp',
            'socso' => 'Socso',
            'gaji_tahan' => 'Gaji Tahan',
            'sewa_rumah' => 'Sewa Rumah',
            'loan' => 'Loan',
            'tarikhmasa' => 'Tarikhmasa',
            'memo_ctg' => 'Memo Ctg',
            'hibah' => 'Hibah',
            'bonus' => 'Bonus',
            'lain' => 'Lain',
            'lain_tambahan' => 'Lain Tambahan',
            'sip' => 'Sip',
            'kwsp_majikan' => 'Kwsp Majikan',
            'socso_majikan' => 'Socso Majikan',
            'sip_majikan' => 'Sip Majikan',
            'created_at' => 'Created At',
            'update_at' => 'Update At',
            'enter_by' => 'Enter By',
            'update_by' => 'Update By',
        ];
    }

    public function getNamestaf()
    {
        return $this->hasOne(MaklumatKakitangan::className(),['id_staf' =>'staff_id']);
    }

    public function getGajikasar()
    {
        $gajikasar = $this->gaji_asas + $this->elaun_asas + $this->elaun_rumah;
        return $gajikasar;
    }

    public function getCtgaji()
    {
        $ctgaji = ($this->gajikasar / date('t',strtotime($this->tarikhmasa))) * $this->ctg;
        return $ctgaji;
    }

    public function getCtggaji()
    {
        $ctgaji = (($this->gajikasar - $this->gaji_tahan) / date('t',strtotime($this->tarikhmasa))) * $this->ctg;
        return $ctgaji;
    }

    public function getLainjumlah()
    {
        $lain_jumlah = $this->lain_tambahan - $this->lain;
        return $lain_jumlah;
    }

    public function getPelaras()
    {
        $tpelarasan =  ($this->hibah + $this->bonus + $this->lain_tambahan) - ($this->tabung_haji + $this->tabung_guru + $this->sewa_rumah + $this->kksk + $this->gaji_tahan + $this->lain + $this->loan);
        return $tpelarasan;
    }

    public function getGajibersih()
    {
        $gaji_bersih = ($this->gajikasar + $this->pelaras - $this->kwsp - $this->socso - $this->sip -((($this->gajikasar + $this->gaji_tahan) / date('t',strtotime($this->tarikhmasa))) * $this->ctg));
        return $gaji_bersih;
    }
}
