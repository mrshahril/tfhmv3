<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\LookupPusatPengajian;
use app\models\LookupJawatan;

$pengajian = ArrayHelper::map(LookupPusatPengajian::find()->asArray()->all(), 'id', 'pusat_pengajian');
$jawatan = ArrayHelper::map(LookupJawatan::find()->orderBy(['jawatan'=>SORT_ASC])->asArray()->all(),'id','jawatan');


$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
$(document).ready(function(){
    $('#m_login_signup').click(function() {
      $('html, body').animate({
        scrollTop: $(".m-login__wrapper-2").offset().top
      }, 1000)
    });
});
JS;
$this->registerJs($script);
?>
<style type="text/css">
.help-block{
  color:red;
}
</style>
<div class="m-login__wrapper-1 m-portlet-full-height">
    <div class="m-login__wrapper-1-1">
        <div class="m-login__contanier">
            <div class="m-login__content">
                <div class="m-login__logo">
                    <a href="#">
                        <img src="<?= Yii::$app->request->baseUrl; ?>/theme/login/img/tafhim.png">
                    </a>
                </div>
                <div class="m-login__title">
                    <h3>
                        Log masuk staff ?
                    </h3>
                </div>
                <div class="m-login__desc">
                    Sila daftar dahulu dan pihak pentadbir akan menghubungi anda selepas anda selesai mengisi borang pendaftaran pengguna sistem tafhim.
                </div>
                <div class="m-login__form-action">
                    <button type="button" id="m_login_signup" class="btn btn-outline-focus m-btn--pill">
                        Dapatkan Akaun
                    </button>
                </div>
            </div>
        </div>
        <div class="m-login__border">
            <div></div>
        </div>
    </div>
</div>
<div class="m-login__wrapper-2 m-portlet-full-height">
    <div class="m-login__contanier">
        <div class="m-login__signin">
            <div class="m-login__head">
                <h3 class="m-login__title">
                    Log Masuk ke Akaun Anda
                </h3>
                <div class="row">
                    <div class="col-12">
                        <?php if(Yii::$app->session->hasFlash('savedone')) { ?>
                            <div class="alert alert-success" role="alert" id="messageerror2">
                                <?php echo Yii::$app->session->getFlash('savedone'); ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php $form = ActiveForm::begin(); ?>
            <form class="m-login__form m-form">
                <div class="form-group m-form__group">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true,'class'=>'form-control m-input','autocomplete'=>'off','placeholder'=>'Email'])->label(false) ?>
                </div>
                <div class="form-group m-form__group">
                    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true,'class'=>'form-control m-input m-login__form-input--last','placeholder'=>'Password'])->label(false) ?>
                </div>
                <div class="row m-login__form-sub">
                    <div class="col m--align-left">
                        
                    </div>
                    <div class="col m--align-right">
                        <a href="javascript:;" id="m_login_forget_password" class="m-link">
                            Forget Password ?
                        </a>
                    </div>
                </div>
                <div class="m-login__form-action">
                   <!--  <button type="submit" id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                        Sign In
                    </button> -->
                    <?= Html::submitButton('Log Masuk', ['class' => 'btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air', 'name' => 'login-button']) ?>
                </div>
            </form>
            <?php ActiveForm::end(); ?>

        </div>
        <div class="m-login__signup">
            <div class="m-login__head">
                <h3 class="m-login__title">
                    Daftar Masuk
                </h3>
                <div class="m-login__desc">
                    Masukkan butiran anda untuk membuat akaun anda.
                </div>
            </div>
            <?php $form = ActiveForm::begin([
                'action' => ['signup'],
                'method' => 'post',
            ]); ?>
            <form class="m-login__form m-form">
                <div class="form-group m-form__group">
                    <?= $form->field($model2, 'username')->textInput(['maxlength' => true,'class'=>'form-control m-input','placeholder'=>'Nama Pertama (Cth: Arif)','required'=>''])->label(false) ?>
                </div>
                <div class="form-group m-form__group">
                    <?= $form->field($model2, 'nama_akaun')->textInput(['maxlength' => true,'class'=>'form-control m-input','placeholder'=>'Nama Penuh','required'=>''])->label(false) ?>
                </div>
                <div class="form-group m-form__group">
                    <?= $form->field($model2, 'tahfiz')->dropDownList($pengajian, 
                        ['prompt' => '--Pilih Cawangan/Tahfiz--','class' => 'form-control m-input','required'=>'']
                    )->label(false) ?>
                </div>
                <div class="form-group m-form__group">
                    <?= $form->field($model2, 'jawatan_sekarang')->dropDownList($jawatan, 
                        ['prompt' => '--Pilih Jawatan Anda--','class' => 'form-control m-input','required'=>'']
                    )->label(false) ?>
                </div>
                <div class="form-group m-form__group">
                    <?= $form->field($model2, 'email')->textInput(['maxlength' => true,'class'=>'form-control m-input','placeholder'=>'Email','required'=>''])->label(false) ?>
                </div>
                <div class="form-group m-form__group">
                    <?= $form->field($model2, 'password_hash')->passwordInput(['maxlength' => true,'class'=>'form-control m-input','placeholder'=>'Password','required'=>''])->label(false) ?>
                </div>
                <div class="m-login__form-action">

                    <?= Html::submitButton('Daftar', ['class' => 'btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air', 'name' => 'login-button']) ?>

                    <button id="m_login_signup_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">
                        Cancel
                    </button>
                </div>
            </form>
            <?php ActiveForm::end(); ?>

        </div>
        <div class="m-login__forget-password">
            <div class="m-login__head">
                <h3 class="m-login__title">
                    Forgotten Password ?
                </h3>
                <div class="m-login__desc">
                    Enter your email to reset your password:
                </div>
            </div>
            <form class="m-login__form m-form" action="">
                <div class="form-group m-form__group">
                    <input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
                </div>
                <div class="m-login__form-action">
                    <button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                        Request
                    </button>
                    <button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom ">
                        Cancel
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
