<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\widgets\Menu;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\Modal;

AppAsset::register($this);

$script = <<< JS
$(document).ready(function(){
    setTimeout(function(){ 
            $('#messageerror2').hide();
    }, 5000);

});
JS;
$this->registerJs($script);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<style type="text/css">
    .alert-success{
        color: #020202;
        background-color: #e2f7e7;
        border-color: #c3e6cb;
    }
</style>
<body id="app-container" class="menu-default menu-sub-hidden">
<?php $this->beginBody() ?>
    <nav class="navbar fixed-top">
        <div class="d-flex align-items-center navbar-left">
            <a href="#" class="menu-button d-none d-md-block">
                <svg class="main" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 17">
                    <rect x="0.48" y="0.5" width="7" height="1" />
                    <rect x="0.48" y="7.5" width="7" height="1" />
                    <rect x="0.48" y="15.5" width="7" height="1" />
                </svg>
                <svg class="sub" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17">
                    <rect x="1.56" y="0.5" width="16" height="1" />
                    <rect x="1.56" y="7.5" width="16" height="1" />
                    <rect x="1.56" y="15.5" width="16" height="1" />
                </svg>
            </a>

            <a href="#" class="menu-button-mobile d-xs-block d-sm-block d-md-none">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
                    <rect x="0.5" y="0.5" width="25" height="1" />
                    <rect x="0.5" y="7.5" width="25" height="1" />
                    <rect x="0.5" y="15.5" width="25" height="1" />
                </svg>
            </a>
        </div>


        <!-- <a class="navbar-logo" href="Dashboard.Default.html">
            <span class="logo d-none d-xs-block"></span>
            <span class="logo-mobile d-block d-xs-none"></span>
        </a> -->

        <div class="navbar-right">
            <div class="user d-inline-block">
                <button class="btn btn-empty p-0" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <span class="name"><?= Yii::$app->user->identity->nama_akaun ?></span>
                    <span>
                        <img alt="Profile Picture" src="<?php echo Yii::$app->request->baseUrl; ?>/theme/tfhm/img/profile-pic-l.jpg" />
                    </span>
                </button>

                <div class="dropdown-menu dropdown-menu-right mt-3">
                    <a class="dropdown-item" href="#">Account</a>
                    <?= Html::a('Sign Out',['/site/logout'],['data-method'=>'post','class'=>'dropdown-item']) ?>
                </div>
            </div>
        </div>
    </nav>
    <!-- Start sidebar -->
    <div class="sidebar">
        <div class="main-menu">
            <div class="scroll">  
                <?php
                    echo Menu::widget([
                        'items' => [
                            [
                                'label' => '<i class="iconsmind-Home"></i><span>Utama</span>', 
                                'url' => ['/site/index'],
                                'options'=>['id'=>'utama'],
                            ],
                            [
                                'label' => '<i class="iconsmind-Arrow-Mix"></i><span>Tukar Tahfiz</span>', 
                                'url' => false,
                                'template' => '<a href="#change">{label}</a>',
                                'options'=>['id'=>'change'],
                            ],
                            [
                                'label' => '<i class="iconsmind-Administrator"></i><span>Sumber Manusia</span>', 
                                'url' => false,
                                'template' => '<a href="#hr">{label}</a>',
                                'options'=>['id'=>'hr'],

                            ],
                            [
                                'label' => '<i class="iconsmind-Bank"></i><span>Kewangan</span>', 
                                'url' => false,
                                'template' => '<a href="#kewangan">{label}</a>',
                                'options'=>['id'=>'kewangan'],

                            ],
                            [
                                'label' => '<i class="iconsmind-Calculator"></i><span>Payroll</span>', 
                                'url' => false,
                                'template' => '<a href="#salary">{label}</a>',
                                'options'=>['id'=>'salary'],

                            ],
                            [
                                'label' => '<i class="iconsmind-Folder-WithDocument"></i><span>Laporan</span>', 
                                'url' => false,
                                'template' => '<a href="#report">{label}</a>',
                                'options'=>['id'=>'report'],

                            ],
                            [
                                'label' => '<i class="iconsmind-Data-Settings"></i><span>Tetapan</span>', 
                                'url' => false,
                                'template' => '<a href="#setting">{label}</a>',
                                'options'=>['id'=>'setting'],

                            ],
                        ],
                        //'activateParents'=>true,activeCssClass,firstItemCssClass
                        // 'firstItemCssClass'=>'active',
                        'firstItemCssClass'=>'active',
                        'encodeLabels' => false,
                        'options' => ['class' => 'list-unstyled'],
                    ]); 
                ?>
            </div>
        </div>
        <div class="sub-menu">
            <div class="scroll">
                <?php
                    echo Menu::widget([
                        'items' => [
                            [
                                'label' => '<b>Pengurusan Kakitangan</b>', 
                                'url' => false,
                                // 'visible'=>User::checkMenu('menu15'),
                            ],
                            [
                                'label' => '<i class="simple-icon-user-following"></i> Maklumat Kakitangan', 
                                'url' => ['/hr/staff/maklumat-kakitangan/index'],
                            ],
                            [
                                'label' => '<i class="simple-icon-user-unfollow"></i> Maklumat Berhenti', 
                                'url' => ['/hr/staff/maklumat-kakitangan/resign'],
                            ],
                            [
                                'label' => '<i class="simple-icon-docs"></i> Surat Tawaran', 
                                'url' => ['/hr/staff/maklumat-kakitangan/surat'],
                            ],
                            [
                                'label' => '<i class="simple-icon-calendar"></i> Maklumat Cuti', 
                                'url' => ['/hr/staff/maklumat-cuti/index'],
                            ],
                            [
                                'label' => '<b>Pengurusan Pelajar</b>', 
                                'url' => false,
                                // 'visible'=>User::checkMenu('menu15'),
                            ],
                            [
                                'label' => '<i class="simple-icon-people"></i>Senarai Keseluruhan Pelajar', 
                                'url' => ['/hr/pelajar/maklumat-pelajar-penjaga/all'],
                            ],
                            [
                                'label' => '<i class="simple-icon-book-open"></i>Maklumat Pelajar', 
                                'url' => ['/hr/pelajar/maklumat-pelajar-penjaga/index'],
                            ],
                            [
                                'label' => '<i class="simple-icon-graduation"></i>Maklumat Alumni Pelajar', 
                                'url' => ['/hr/pelajar/maklumat-pelajar-penjaga/alumni'],
                            ],
                            [
                                'label' => '<i class="simple-icon-hourglass"></i>Pelajar Pending', 
                                'url' => ['/hr/pelajar/maklumat-pelajar-penjaga/pending'],
                            ],
                            [
                                'label' => '<i class="simple-icon-drawer"></i>Kelas Pelajar', 
                                'url' => ['/hr/pelajar/kelas-pelajar/index'],
                            ],
                            [
                                'label' => '<i class="simple-icon-wallet"></i>Zakat Pelajar', 
                                'url' => ['/hr/pelajar/zakat/index'],
                            ],
                            [
                                'label' => '<i class="simple-icon-envelope"></i>Subsidi Pelajar',
                                'url' => ['/hr/pelajar/subsidi-pelajar/index'],
                            ],
                        ],
                        //'items' => $items,
                        // 'activeCssClass'=>'active',
                        'activateParents'=>true,

                        'encodeLabels' => false,
                        'options' => ['class' => 'list-unstyled', 'data-link'=> 'hr']
                    ]);
                ?>
                <?php
                    echo Menu::widget([
                        'items' => [
                            [
                                'label' => '<b>Pengurusan Gaji</b>', 
                                'url' => false,
                                // 'visible'=>User::checkMenu('menu15'),
                            ],
                            [
                                'label' => '<i class="simple-icon-people"></i>Kakitangan',
                                'url' => ['/payroll/pay-slip/managesalary'],
                            ],
                            [
                                'label' => '<i class="simple-icon-calculator"></i>Jana Gaji',
                                'url' => ['/payroll/pay-slip/jana'],
                            ],
                        ],
                        //'items' => $items,
                        // 'activeCssClass'=>'active',
                        'activateParents'=>true,

                        'encodeLabels' => false,
                        'options' => ['class' => 'list-unstyled', 'data-link'=> 'salary']
                    ]);
                ?>
                <?php
                    echo Menu::widget([
                        'items' => [
                            [
                                'label' => '<b>Payroll</b>', 
                                'url' => false,
                                // 'visible'=>User::checkMenu('menu15'),
                            ],
                            [
                                'label' => '<i class="simple-icon-wallet"></i>Gaji Bersih',
                                'url' => ['/report/payroll/pay-slip/index','type'=>base64_encode('gajibersih')],
                            ],
                            [
                                'label' => '<i class="simple-icon-calculator"></i>EPF',
                                'url' => ['/report/payroll/pay-slip/index','type'=>base64_encode('epf')],
                            ],
                            [
                                'label' => '<i class="simple-icon-calculator"></i>SOCSO',
                                'url' => ['/report/payroll/pay-slip/index','type'=>base64_encode('socso')],
                            ],
                            [
                                'label' => '<i class="simple-icon-calculator"></i>SIP',
                                'url' => ['/report/payroll/pay-slip/index','type'=>base64_encode('sip')],
                            ],
                            [
                                'label' => '<i class="simple-icon-calculator"></i>Tabung Kebajikan',
                                'url' => ['/report/payroll/pay-slip/index','type'=>base64_encode('tbgkbj')],
                            ],
                            [
                                'label' => '<i class="iconsmind-Bank"></i>Tabung Guru',
                                'url' => ['/report/payroll/pay-slip/index','type'=>base64_encode('tbgguru')],
                            ],
                            [
                                'label' => '<i class="simple-icon-calculator"></i>CTG',
                                'url' => ['/report/payroll/pay-slip/index','type'=>base64_encode('ctg')],
                            ],
                            [
                                'label' => '<i class="iconsmind-Green-House"></i>Sewa Rumah',
                                'url' => ['/report/payroll/pay-slip/index','type'=>base64_encode('sewa')],
                            ],
                            [
                                'label' => '<i class="simple-icon-calculator"></i>KKSK',
                                'url' => ['/report/payroll/pay-slip/index','type'=>base64_encode('kksk')],
                            ],
                            [
                                'label' => '<i class="iconsmind-Bank"></i>Tabung Haji',
                                'url' => ['/report/payroll/pay-slip/index','type'=>base64_encode('tbghaji')],
                            ],
                            [
                                'label' => '<i class="iconsmind-Bank"></i>Bank',
                                'url' => ['/report/payroll/pay-slip/index','type'=>base64_encode('bank')],
                            ],
                        ],
                        //'items' => $items,
                        // 'activeCssClass'=>'active',
                        'activateParents'=>true,

                        'encodeLabels' => false,
                        'options' => ['class' => 'list-unstyled', 'data-link'=> 'report']
                    ]);
                ?>
                <?php
                    echo Menu::widget([
                        'items' => [
                            [
                                'label' => '<b>Pusat Tahfiz</b>', 
                                'url' => false,
                                // 'visible'=>User::checkMenu('menu15'),
                            ],
                            [
                                'label' => '<i class="simple-icon-home"></i>RAUDHAH',
                                'url' => ['/site/change','type'=>base64_encode(1)],
                            ],
                            [
                                'label' => '<i class="simple-icon-home"></i>AN-NUR',
                                'url' => ['/site/change','type'=>base64_encode(2)],
                            ],
                            [
                                'label' => '<i class="simple-icon-home"></i>DAMAI',
                                'url' => ['/site/change','type'=>base64_encode(3)],
                            ],
                            [
                                'label' => '<i class="simple-icon-home"></i>AL-EHSAN',
                                'url' => ['/site/change','type'=>base64_encode(4)],
                            ],
                            [
                                'label' => '<i class="simple-icon-home"></i>BANGI-3',
                                'url' => ['/site/change','type'=>base64_encode(5)],
                            ],
                            [
                                'label' => '<i class="simple-icon-home"></i>AS-SOBAH',
                                'url' => ['/site/change','type'=>base64_encode(6)],
                            ],
                            [
                                'label' => '<i class="simple-icon-home"></i>AL-MADANI',
                                'url' => ['/site/change','type'=>base64_encode(7)],
                            ],
                            [
                                'label' => '<i class="simple-icon-home"></i>AL-KAUSAR',
                                'url' => ['/site/change','type'=>base64_encode(8)],
                            ],
                            [
                                'label' => '<i class="simple-icon-home"></i>AL-IKHLAS',
                                'url' => ['/site/change','type'=>base64_encode(9)],
                            ],
                            [
                                'label' => '<i class="simple-icon-home"></i>AL-AMIRIN',
                                'url' => ['/site/change','type'=>base64_encode(10)],
                            ],
                            [
                                'label' => '<i class="simple-icon-home"></i>AL-IKHWAN',
                                'url' => ['/site/change','type'=>base64_encode(11)],
                            ],
                            [
                                'label' => '<i class="simple-icon-home"></i>AL-ABRAR',
                                'url' => ['/site/change','type'=>base64_encode(12)],
                            ],
                            [
                                'label' => '<i class="simple-icon-home"></i>AL-HASANAH',
                                'url' => ['/site/change','type'=>base64_encode(13)],
                            ],
                        ],
                        //'items' => $items,
                        // 'activeCssClass'=>'active',
                        'activateParents'=>true,

                        'encodeLabels' => false,
                        'options' => ['class' => 'list-unstyled', 'data-link'=> 'change']
                    ]);
                ?>
                <?php
                    echo Menu::widget([
                        'items' => [
                            [
                                'label' => '<b>Yuran</b>', 
                                'url' => false,
                                // 'visible'=>User::checkMenu('menu15'),
                            ],
                            [
                                'label' => '<i class="simple-icon-calculator"></i>Yuran Pendaftaran',
                                'url' => ['/kewangan/yuran-pendaftaran/index'],
                            ],
                        ],
                        //'items' => $items,
                        // 'activeCssClass'=>'active',
                        'activateParents'=>true,

                        'encodeLabels' => false,
                        'options' => ['class' => 'list-unstyled', 'data-link'=> 'kewangan']
                    ]);
                ?>
                <?php
                    echo Menu::widget([
                        'items' => [
                            [
                                'label' => '<b>Tetapan</b>', 
                                'url' => false,
                                // 'visible'=>User::checkMenu('menu15'),
                            ],
                            [
                                'label' => '<i class="simple-icon-docs"></i>Pendapatan',
                                'url' => ['/setting/pendapatan/index'],
                            ],
                            [
                                'label' => '<i class="simple-icon-docs"></i>Jawatan',
                                'url' => ['/setting/jawatan/index'],
                            ],
                            [
                                'label' => '<i class="simple-icon-docs"></i>Kwsp',
                                'url' => ['/setting/kwsp/index'],
                            ],
                            [
                                'label' => '<i class="simple-icon-docs"></i>Peringkat Gaji',
                                'url' => ['/setting/peringkat-gaji/index'],
                            ],
                            [
                                'label' => '<i class="simple-icon-docs"></i>Perkeso',
                                'url' => ['/setting/perkeso/index'],
                            ],
                            [
                                'label' => '<i class="simple-icon-docs"></i>Perkeso SIP',
                                'url' => ['/setting/perkeso-sip/index'],
                            ],
                            [
                                'label' => '<i class="simple-icon-docs"></i>Pusat Pengajian',
                                'url' => ['/setting/pusat-pengajian/index'],
                            ],
                            [
                                'label' => '<i class="simple-icon-docs"></i>Yuran Daftar',
                                'url' => ['/setting/yuran-daftar/index'],
                            ],
                        ],
                        //'items' => $items,
                        // 'activeCssClass'=>'active',
                        'activateParents'=>true,

                        'encodeLabels' => false,
                        'options' => ['class' => 'list-unstyled', 'data-link'=> 'setting']
                    ]);
                ?>
            </div>
        </div>
        <!-- end submenu -->
    </div>
    <!-- end sidebar-->

    <main>
        <div class="container-fluid">
            <?= Alert::widget() ?>
            <div class="col-12">
                <?php if(Yii::$app->session->hasFlash('tukartahfiz')) { ?>
                    <div class="alert alert-success" role="alert" id="messageerror2">
                        <?php echo Yii::$app->session->getFlash('tukartahfiz'); ?>
                    </div>
                <?php } ?>
            </div>
            <div class="col-lg-12">
                <div class="alert alert-success" role="alert">
                   Anda kini berada di Pusat Pengajian <?= Yii::$app->user->identity->branch->pusat_pengajian ?>
                </div>
            </div>
            
            <?= $content ?>
        </div>
        
    </main>

<?php $this->endBody() ?>
</body>
</html>
<?php 
Modal::begin([
    'header' =>'<h5 class="modal-title" id="exampleModalLabel">Tafhim</h5>',
    'id' => 'exampleModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => true, 'keyboard' => TRUE],
    'options' => [
        'tabindex' => false, // important for Select2 to work properly
        'aria-hidden'=>'true'
    ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>
<?php $this->endPage() ?>
