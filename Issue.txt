Issue
========================
1. Pendaftaran Pelajar
   - Situasi : Ahmad mendaftar di HQ . staff login ke sistem dan berada di dalam tahfiz raudhah (default). Ahmad mahu berada di tahfiz Damai dan staff meletakkan nama Ahmad di dalam tahfiz Damai. Secara default, sekiranya user berada di dalam sistem Raudhah hanya pelajar di tahfiz Raudhah sahaja akan dipaparkan. Selain itu tidak . Dan apabila melakukan pencarian di dalam sistem seperti nama Ahmad semasa hari pendaftaran . Sistem tidak dapat memaparkan maklumat Ahmad kerana maklumat Ahmad berada di dalam sistem Damai.