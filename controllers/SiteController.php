<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\User;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // 'only' => ['signup','login'],
                'rules' => [
                    [
                        'actions' => ['login','error','signup'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout','index','change'],   
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */

    public function actionChange($type)
    {
        $model = User::findOne(Yii::$app->user->identity->id);
        $model->tahfiz = base64_decode($type);

        if ($model->save()) {
            Yii::$app->session->setFlash('tukartahfiz','Akaun anda telah berjaya ditukar ke tahfiz '.$model->branch->pusat_pengajian);
            return $this->redirect(['index']);
        }
    }
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $this->layout = 'login';

        $model = new LoginForm();
        $model2 = new SignupForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
            'model2' => $model2,
        ]);
    }

    public function actionSignup()
    {
        $model2 = new SignupForm();

        if ($model2->load(Yii::$app->request->post())) {
            // var_dump($model2);
            // exit();
            if ($user = $model2->signup()) {
                // if (Yii::$app->getUser()->login($user)) {
                //     return $this->goHome();
                // }
                Yii::$app->session->setFlash('savedone','Maklumat telah berjaya disimpan. Anda akan dihubungi oleh pihak pentadbir setelah akaun anda disahkan. Terima Kasih.');
                
                return $this->redirect(['site/login']);
            }
        }


        // return $this->render('signup', [
        //     'model2' => $model2,
        // ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    // public function actionError()
    // {
    //     $exception = Yii::$app->errorHandler->exception;
    //     if ($exception !== null) {
    //         return $this->render('error', ['exception' => $exception]);
    //     }
    // }
}
