<?php
namespace app\models;

use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $nama_akaun;
    public $username;
    public $email;
    public $password_hash;
    public $tahfiz;
    public $jawatan_sekarang;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Nama pertama ini telah wujud'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['nama_akaun', 'trim'],
            ['nama_akaun', 'required'],
            ['nama_akaun', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Nama pengguna ini telah wujud'],
            ['nama_akaun', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Email pengguna ini telah wujud'],

            ['password_hash', 'required'],
            ['password_hash', 'string', 'min' => 6],

            [['tahfiz','jawatan_sekarang'], 'required'],
            [['tahfiz','jawatan_sekarang'], 'integer'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->email = $this->email;
        $user->nama_akaun = $this->nama_akaun;
        $user->username = $this->username;
        $user->tahfiz = $this->tahfiz;
        $user->jawatan_sekarang = $this->jawatan_sekarang;
        $user->setPassword($this->password_hash);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
}
