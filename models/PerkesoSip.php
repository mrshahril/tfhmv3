<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "perkeso_sip".
 *
 * @property integer $id
 * @property double $dari
 * @property double $hingga
 * @property double $syer_majikan
 * @property double $syer_pekerja
 * @property double $jumlah_caruman
 */
class PerkesoSip extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'perkeso_sip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dari', 'hingga', 'syer_majikan', 'syer_pekerja', 'jumlah_caruman'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dari' => 'Dari',
            'hingga' => 'Hingga',
            'syer_majikan' => 'Syer Majikan',
            'syer_pekerja' => 'Syer Pekerja',
            'jumlah_caruman' => 'Jumlah Caruman',
        ];
    }
}
