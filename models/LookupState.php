<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_state".
 *
 * @property integer $state_id
 * @property string $state
 */
class LookupState extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_state';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state'], 'string', 'max' => 225],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'state_id' => 'State ID',
            'state' => 'Negeri',
        ];
    }
}
