<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_jawatan".
 *
 * @property integer $id
 * @property string $jawatan
 */
class LookupJawatan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_jawatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jawatan'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jawatan' => 'Jawatan',
        ];
    }
}
