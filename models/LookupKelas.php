<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_kelas".
 *
 * @property int $id
 * @property string $kelas
 * @property int $id_pusat_pengajian
 * @property int $tahap
 */
class LookupKelas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lookup_kelas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pusat_pengajian', 'tahap'], 'integer'],
            [['kelas'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kelas' => 'Kelas',
            'id_pusat_pengajian' => 'Id Pusat Pengajian',
            'tahap' => 'Tahap',
        ];
    }
}
