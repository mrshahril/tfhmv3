<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_district".
 *
 * @property integer $district_id
 * @property string $district
 * @property string $state_id
 */
class LookupDistrict extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_district';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['district', 'state_id'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'district_id' => 'District ID',
            'district' => 'Daerah',
            'state_id' => 'State ID',
        ];
    }
}
