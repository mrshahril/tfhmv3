<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_yurandaftar".
 *
 * @property int $id
 * @property string $jenis_bayaran
 * @property int $tahap_pelajar
 * @property double $jumlah
 * @property int $enter_by
 * @property string $created_at
 * @property string $updated_at
 * @property int $updated_by
 */
class LookupYurandaftar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lookup_yurandaftar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tahap_pelajar', 'enter_by', 'updated_by'], 'integer'],
            [['jumlah'], 'number'],
            [['jenis_bayaran'], 'string', 'max' => 255],
            [['created_at', 'updated_at'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_bayaran' => 'Jenis Bayaran',
            'tahap_pelajar' => 'Tahap Pelajar',
            'jumlah' => 'Jumlah',
            'enter_by' => 'Enter By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
