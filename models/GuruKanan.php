<?php

namespace app\models;

use Yii;

use app\modules\hr\staff\models\MaklumatKakitangan;
/**
 * This is the model class for table "guru_kanan".
 *
 * @property integer $id
 * @property integer $id_staff
 * @property integer $id_pusat_pengajian
 */
class GuruKanan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guru_kanan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_staff', 'id_pusat_pengajian'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_staff' => 'Id Staff',
            'id_pusat_pengajian' => 'Id Pusat Pengajian',
        ];
    }

    public function getGurukanan()
    {
        return $this->hasOne(MaklumatKakitangan::className(),['id_staf'=>'id_staff']);
    }
}
