<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'theme/tfhm/font/iconsmind/style.css',
        'theme/tfhm/font/simple-line-icons/css/simple-line-icons.css',
        'theme/tfhm/css/vendor/bootstrap.min.css',
        'theme/tfhm/css/vendor/dataTables.bootstrap4.min.css',
        'theme/tfhm/css/vendor/datatables.responsive.bootstrap4.min.css',
        'theme/tfhm/css/vendor/bootstrap-float-label.min.css',
        // 'theme/tfhm/css/vendor/select2.min.css',
        'theme/tfhm/css/vendor/perfect-scrollbar.css',
        'theme/tfhm/css/vendor/owl.carousel.min.css',
        'theme/tfhm/css/vendor/bootstrap-stars.css',
        'theme/tfhm/css/vendor/nouislider.min.css',
        'theme/tfhm/css/vendor/select2.min.css',
        'theme/tfhm/css/vendor/select2-bootstrap.min.css',
        'theme/tfhm/css/vendor/bootstrap-datepicker3.min.css',
        'theme/tfhm/css/main.css',
        'theme/tfhm/css/sweetalert.css',
        'theme/tfhm/css/dore.light.purple.min.css',
        "theme/tfhm/css/vendor/cropper.min.css",

        //'css/dore.dark.red.css',
        // 'theme/tfhm/css/site.css',
        // 'theme/tfhm/css/bootstrap-timepicker.min.css',

    ];
    public $js = [
        // 'js/webfont.js',
        // 'theme/demo2/assets/vendors/base/vendors.bundle.js',
        // 'theme/demo2/assets/demo/demo2/base/scripts.bundle.js',

        // 'theme/tfhm/js/vendor/jquery-3.3.1.min.js',

        'theme/tfhm/js/vendor/bootstrap.bundle.min.js',
        'theme/tfhm/js/vendor/Chart.bundle.min.js',
        'theme/tfhm/js/vendor/chartjs-plugin-datalabels.js',
        'theme/tfhm/js/vendor/moment.min.js',
        'theme/tfhm/js/vendor/datatables.min.js',
        'theme/tfhm/js/vendor/perfect-scrollbar.min.js',
        'theme/tfhm/js/vendor/owl.carousel.min.js',
        'theme/tfhm/js/vendor/progressbar.min.js',
        'theme/tfhm/js/vendor/jquery.barrating.min.js',
        'theme/tfhm/js/vendor/select2.full.js',
        'theme/tfhm/js/vendor/nouislider.min.js',
        'theme/tfhm/js/vendor/bootstrap-datepicker.js',
        'theme/tfhm/js/vendor/Sortable.js',
        'theme/tfhm/js/dore.script.js',
        'theme/tfhm/js/sweetalert.min.js',
        'theme/tfhm/js/ui-sweetalert.min.js',
        'theme/tfhm/js/scripts.single.theme.js',
        'theme/tfhm/js/vendor/bootstrap-notify.min.js',
        'theme/tfhm/js/vendor/cropper.min.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
